<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

?>

<div <?php post_class( 'single-blog-post bg-solid-post hide-pr quote_post' ); ?>>
    <div class="post-data">
        <h5 class="title">
            <a href="<?php the_permalink() ?>">
                <?php rogan_get_html_tag( 'blockquote', get_the_content()) ?>
            </a>
        </h5>
        <div class="author"><?php the_author(); ?></div>
    </div> <!-- /.post-data -->
</div> <!-- /.single-blog-post -->
