<!-- Modal -->
<div class="modal fade" id="modalbox_features_5" tabindex="-1" role="dialog" aria-labelledby="eCommerce Database" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">eCommerce Database</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add eCommerce Database</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="ecommerce_database" id="ecommerce_database" class="form-control" multiple>
                            <option id="SQLite">SQLite</option>
                            <option id="Realm Database">Realm Database</option>
                            <option id="Couchbase">Couchbase</option>
                            <option id="LevelDB">LevelDB</option>
                            <option id="PaperDb">PaperDb</option>
                            <option id="Azure Cosmos DB">Azure Cosmos DB</option>
                            <option id="MongoDB">MongoDB</option>
                            <option id="Microsoft SQL Server">Microsoft SQL Server</option>
                            <option id="MariaDB">MariaDB</option>
                            <option id="MySQL">MySQL</option>
                            <option id="Oracle NoSQL Database">Oracle NoSQL Database</option>
                            <option id="PostgreSQL">PostgreSQL</option>
                            <option id="Oracle Database">Oracle Database</option>
                            <option id="Neo4j">Neo4j</option>
                            <option id="Actian X">Actian X</option>
                            <option id="Adabas">Adabas</option>
                            <option id="Aerospike">Aerospike</option>
                            <option id="AllegroGraph">AllegroGraph</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>