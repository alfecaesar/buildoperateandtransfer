<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

$opt = get_option( 'rogan_opt' );
$blog_excerpt = !empty($opt['blog_excerpt']) ? $opt['blog_excerpt'] : 40;
$is_post_date = isset($opt['is_post_date']) ? $opt['is_post_date'] : '1';
?>
<div <?php post_class( 'single-blog-post' ) ?>>
    <?php
    if(is_sticky()) {
        echo '<p class="sticky-label">'.esc_html__( 'Featured', 'rogan' ).'</p>';
    }
    ?>
    <?php
    if (has_post_thumbnail()) : ?>
        <div class="img-holder"> <?php the_post_thumbnail(); ?> </div>
    <?php
    endif;
    ?>
    <div class="post-data">
        <?php if($is_post_date == '1' ) : ?>
            <a href="<?php rogan_day_link() ?>" class="date"> <?php the_time(get_option( 'date_format' )) ?> </a>
        <?php endif; ?>
        <h5 class="blog-title-two title">
            <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
        </h5>
        <p> <?php echo strip_shortcodes(rogan_excerpt( 'rogan_opt', 'blog_excerpt', false)); ?> </p>
        <a href="<?php the_permalink(); ?>" class="read-more"><i class="flaticon-next-1"></i></a>
    </div>
</div> <!-- /.single-blog-post -->