<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_product_slider extends Widget_Base {

    public function get_name() {
        return 'rogan_product_slider';
    }

    public function get_title() {
        return esc_html__( 'Product Slider', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-post-slider';
    }

    public function get_script_depends() {
        return [ 'iziModal' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls()
    {

        // Filters
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__('Filters', 'chaoz-core'),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__('Show products', 'chaoz-core'),
                'description' => esc_html__('How much featured products do you want to show in this section?', 'chaoz-core'),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 6
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__('Button Label', 'chaoz-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Shop Now'
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__('Order', 'chaoz-core'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

        // Share Options
        $this->start_controls_section(
            'share_opt', [
                'label' => esc_html__('Share Options', 'chaoz-core'),
            ]
        );

        $this->add_control(
            'is_share', [
                'label' => esc_html__('Share', 'chaoz-core'),
                'type' => Controls_Manager::SWITCHER,
                'default' => true,
            ]
        );

        $this->add_control(
            'share_label', [
                'label' => esc_html__('Share Label', 'chaoz-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Share+'
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $is_featured = isset($settings['is_featured_products_only']) ? $settings['is_featured_products_only'] : '';
        if($is_featured == 'yes') {
            $tax_query[] = array(
                'taxonomy' => 'product_visibility',
                'field' => 'name',
                'terms' => 'featured',
                'operator' => 'IN', // or 'NOT IN' to exclude feature products
            );
        }
        else {
            $tax_query = '';
        }
        $products = new WP_Query(array(
            'post_type' => 'product',
            'post_status' => 'publish',
            'ignore_sticky_posts' => 1,
            'posts_per_page' => !empty($settings['show_count']) ? $settings['show_count'] : -1,
            'order' => !empty($settings['order']) ? $settings['order'] : 'DESC',
            'tax_query' => $tax_query
        ));
        ?>
        <div class="eCommerce-hero-section">
            <div class="main-carousel-wrapper">
                <div id="eCommerce-carousel" class="carousel slide" data-ride="carousel" data-interval="8000">
                    <div class="carousel-inner">
                        <?php
                        $i = 1;
                        while( $products->have_posts() ) : $products->the_post();
                            ?>
                            <!-- Layer <?php echo $i; ?> -->
                            <div class="carousel-item <?php echo ($i == 1) ? 'active' : ''; ?>">
                                <div class="inner-container">
                                    <div class="inner-item-wrapper">
                                        <h2 class="main-title" data-animation="animated fadeInLeft"> <?php the_title() ?> </h2>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title_attribute() ?>" class="product-img" data-animation="animated fadeInRight">
                                        <div class="price-tag">
                                            <?php woocommerce_template_loop_price(); ?>
                                        </div> <!-- /.price-tag -->
                                        <ul class="button-group">
                                            <?php if ( !empty($settings['btn_label']) ) : ?>
                                            <li>
                                                <a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ) ?>" class="shop-now" data-animation="animated fadeInLeft">
                                                    <?php echo $settings['btn_label'] ?>
                                                </a>
                                            </li>
                                            <?php endif; ?>
                                            <li><button class="details-info-button" data-animation="animated fadeInRight" data-izimodal-open="#product-<?php echo get_the_ID(); ?>" data-izimodal-transitionout="fadeIn">+</button></li>
                                        </ul>
                                    </div> <!-- /.inner-item-wrapper -->
                                </div> <!-- /.inner-container -->
                                <div class="brand-text" data-animation="animated bounceInUp">Nike</div>
                            </div>
                            <?php
                        ++$i;
                        endwhile; 
                        wp_reset_postdata();
                        ?>
                    </div> <!-- /.carousel-inner -->

                    <ol class="carousel-indicators">
                        <?php
                        unset($i);
                        $i = 0;
                        while( $products->have_posts() ) : $products->the_post(); ?>
                            <li data-target="#eCommerce-carousel" data-slide-to="<?php echo $i; ?>" class="<?php echo ($i==0) ? 'active' : ''; ?>"></li>
                        <?php ++$i; endwhile; wp_reset_postdata(); ?>
                    </ol>
                </div>
            </div> <!-- /.main-carousel-wrapper -->
            <?php
            if ( $settings['is_share'] == 'yes' ) : ?>
                <ul class="social-share">
                    <?php if ( !empty($settings['share_label']) ) :  ?>
                        <li> <?php echo esc_html($settings['share_label']) ?> </li>
                    <?php endif; ?>
                    <li><a href="//www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <li><a href="//twitter.com/intent/tweet?text=<?php the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="//facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                </ul>
            <?php endif; ?>
        </div>

        <?php
        unset($i);
        $i = 1;
        while( $products->have_posts() ) : $products->the_post();
            ?>
            <div id="product-<?php echo get_the_ID(); ?>" class="iziModal product-details-modal" data-izimodal-group="alerts">
                <div class="main-bg-wrapper">
                    <button data-izimodal-close="" data-izimodal-transitionout="fadeOutDown" class="close-button">
                        <img src="<?php echo plugins_url('images/icon/icon43.svg', __FILE__) ?>" alt="close icon">
                    </button>
                    <button data-izimodal-next="" class="next"><i class="flaticon-next"></i></button>
                    <button data-izimodal-prev="" class="prev"><i class="flaticon-back"></i></button>
                    <img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title_attribute() ?>" class="product-img">
                    <h2 class="title"> <?php the_title() ?> </h2>
                    <?php the_excerpt(); ?>
                    <?php woocommerce_template_loop_add_to_cart() ?>
                </div> <!-- /.main-bg-wrapper -->
            </div> <!-- /.product-details-modal -->
            <?php
        ++$i;
        endwhile; wp_reset_postdata();

    }
}