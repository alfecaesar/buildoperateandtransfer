<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

get_header();

$wrapper = '<div class="blog-details pt-140 pb-140">
                    <div class="container">';

if ( class_exists( 'WooCommerce' ) ) {
    if ( is_cart() ) {
        $wrapper = '<div class="cart-section pt-90 pb-100">
				    <div class="main-container">';
    } elseif( is_checkout() ) {
        $wrapper = '<div class="checkout-section pt-90 pb-100">
				    <div class="container">';
    }
    else {
        $wrapper = '<div class="blog-details pt-140 pb-140">
                    <div class="container">';
    }
}

$wrapper_end = '</div> </div>';

while ( have_posts() ) : the_post();
    echo wp_kses_post($wrapper);
        the_content();
        wp_link_pages( array (
            'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'rogan' ) . '</span>',
            'after'       => '</div>',
            'link_before' => '<span>',
            'link_after'  => '</span>',
            'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rogan' ) . ' </span>%',
            'separator'   => '<span class="screen-reader-text">, </span>',
        ));

        // If comments are open or we have at least one comment, load up the comment template.
        if ( comments_open() || get_comments_number() ) :
            comments_template();
        endif;
    echo wp_kses_post($wrapper_end);
endwhile; // End of the loop.

get_footer();
