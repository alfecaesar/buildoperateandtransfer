<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_service_creative extends Widget_Base {

    public function get_name() {
        return 'rogan_service_creative';
    }

    public function get_title() {
        return esc_html__( 'Service Posts', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_keywords() {
        return [ 'service creative', 'service minimal', 'service modern', 'service standard' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }


    protected function _register_controls() {

        /****************************** Background **************************/
        $this->start_controls_section(
            'sec-style', [
                'label' => esc_html__( 'Section Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__( 'Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'default' => 'creative',
                'options' => [
                    'creative'  => esc_html__( 'Creative', 'rogan-core' ),
                    'minimal'   => esc_html__( 'Minimal', 'rogan-core' ),
                    'modern'    => esc_html__( 'Modern', 'rogan-core' ),
                    'standard'  => esc_html__( 'Standard', 'rogan-core' ),
                ]
            ]
        );

        $this->add_control(
            'important_note',
            [
                'label' => esc_html__( 'Important Note', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::RAW_HTML,
                'raw' => esc_html__( 'Place this widget in a full width container of Elementor in the Elementor Full Width page template. ', 'rogan-core' ),
                'content_classes' => 'your-class',
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Top Section ------------------------
        $this->start_controls_section(
            'top_sec_opt', [
                'label' => esc_html__( 'Top Section', 'rogan-core' ),
                'condition' => [
                    'style' => 'creative'
                ]
            ]
        );

        $this->add_control(
            'top_sec', [
                'label' => esc_html__( 'Top Section', 'rogan-core' ),
                'description' => esc_html__( 'Show / Hide the Top Section', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'is_row_reverse_top_sec', [
                'label' => esc_html__( 'Row Reverse', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
                'condition' => [
                    'top_sec' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'image_01', [
                'label' => esc_html__( 'Image 01', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'condition' => [
                    'top_sec' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'image_02', [
                'label' => esc_html__( 'Image 02', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'condition' => [
                    'top_sec' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'content', [
                'label' => esc_html__( 'Content', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
                'condition' => [
                    'top_sec' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'margin_bottom',
            [
                'label' => esc_html__( 'Margin Bottom', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px' ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .our-history.mb-200' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'top_sec' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter Options', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'cat', [
                'label' => esc_html__( 'Category', 'rogan-core' ),
                'description' => esc_html__( 'Input here the category slugs to display services by category. Use comma for multiple category slugs', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 3
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'orderby', [
                'label' => esc_html__( 'Order By', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'none' => esc_html__('None', 'rogan-core'),
                    'title' => esc_html__('Title (Order by post title)', 'rogan-core'),
                    'date' => esc_html__('Date (Order by post date)', 'rogan-core'),
                    'rand' => esc_html__('Random (Order the posts randomly)', 'rogan-core'),
                ],
                'default' => 'none'
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        $cats = explode(',', $settings['cat']);
        if(!empty($settings['cat'])) {
            $service = new WP_Query(array(
                'post_type' => 'service',
                'posts_per_page' => $settings['show_count'],
                'order' => $settings['order'],
                'orderby' => $settings['orderby'],
                'tax_query' => array(
                    array(
                        'taxonomy' => 'service_cat',
                        'field'    => 'slug',
                        'terms'    => $cats,
                    ),
                ),
            ));
        } else {
            $service = new WP_Query(array(
                'post_type' => 'service',
                'posts_per_page' => $settings['show_count'],
                'order' => $settings['order'],
            ));
        }

        $is_row_reverse_top_sec = ( $settings['is_row_reverse_top_sec'] == 'yes' ) ? 'flex-row-reverse' : '';

        if( $settings['style'] == 'creative' ) {
            include 'part-service-creative.php';
        }

        if( $settings['style'] == 'minimal' ) {
            include 'part-service-minimal.php';
        }

        if( $settings['style'] == 'modern' ) {
            include 'part-service-modern.php';
        }

        if( $settings['style'] == 'standard' ) {
            include 'part-service-standard.php';
        }
    }
}