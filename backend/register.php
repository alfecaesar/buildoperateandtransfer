<?php

include 'dbconfig.php';

$user_name = (!empty($_POST['user_name'])) ? $_POST['user_name'] : '';
$user_email = (!empty($_POST['user_email'])) ? $_POST['user_email'] : '';
$user_password = (!empty($_POST['user_password'])) ? $_POST['user_password'] : '';
$access_type = (!empty($_POST['access_type'])) ? $_POST['access_type'] : '';


if($user_name === '' or $user_email === '' or $user_password === '' ){
    header("Location: http://afarinsights.com/backend/?register_field_error=true");
}
else{
    $count = $database->count("user", [
        "user_email" => $user_email
    ]);
    if($count === 0){

        $database->insert("user", [
            "user_name" => $user_name,
            "user_email" => $user_email,
            "user_password" => $user_password,
            "access_type" => $access_type
        ]);
    
        header("Location: http://afarinsights.com/backend/?user_saved=true");
    }
    else{
        header("Location: http://afarinsights.com/backend/?register_error=true");
    }
}



?>