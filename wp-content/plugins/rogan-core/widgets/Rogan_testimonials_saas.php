<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_saas extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials';
    }

    public function get_title() {
        return esc_html__( 'Testimonials Saas', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        //***********************************  Testimonials Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Testimonials Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'What’s Our Client Think <br> About Us.',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //*********************************  Shape Image ********************************//
        $this->start_controls_section(
            'shape',
            [
                'label' => esc_html__( 'Title Icon Box', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'shape_img1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape5.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape_img2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/icon/icon27.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();

        //********************************* Display Author Image **********************************//
        $this->start_controls_section(
            'author_img_sec',
            [
                'label' => esc_html__( 'Author Images', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'alt',
            [
                'label' => esc_html__( 'Alt Text', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'author_images',
            [
                'label' => esc_html__( 'Author Images', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ alt }}}',
            ]
        );

        $this->end_controls_section();


        //*********************************  Testimonials Repeater ********************************//
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'author_name', [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'desc', [
                'label' => esc_html__( 'Description', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'designation', [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ author_name }}}',
            ]
        );

        $this->end_controls_section();


        // Gradient Color
        $this->start_controls_section(
            'background_style',
            [
                'label' => esc_html__( 'Background', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Background Color 02', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .sass-testimonial-section' => 'background: linear-gradient( 90deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $testimonials = $settings['testimonials_sec'];
        $author_images = $settings['author_images'];

        ?>
        <div class="sass-testimonial-section">

            <?php
            if (!empty($author_images)) {
                foreach ($author_images as $author_image) {
                    $image = isset($author_image['author_img']['id']) ? $author_image['author_img']['id'] : '';
                    $image = wp_get_attachment_image_src($image, 'full');
                    ?>
                    <?php if (!empty($image[0])) : ?>
                        <img src="<?php echo $image[0] ?>" alt="" class="people elementor-repeater-item-<?php echo $author_image['_id'] ?>">
                    <?php endif; ?>
                    <?php
                }
            }
            ?>
            <div class="container">
                <div class="theme-title-one text-center hide-pr">
                    <div class="icon-box hide-pr">
                        <?php if (!empty($settings['shape_img1']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['shape_img1']['url']) ?>" alt="" class="bg-shape">
                        <?php endif; ?>
                        <?php if (!empty($settings['shape_img2']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['shape_img2']['url']) ?>" alt="" class="icon">
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="main-title"> <?php echo wp_kses_post($settings['title_text']) ?> </h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->

                <div class="inner-container">
                    <div class="main-content">
                        <div class="agn-testimonial-slider">
                            <?php
                            if (!empty($testimonials)) {
                                foreach ($testimonials as $testimonial) {
                                    ?>
                                    <div class="item elementor-repeater-item-<?php echo $testimonial['_id'] ?>">
                                        <?php if (!empty($testimonial['desc'])) : ?>
                                            <?php echo wp_kses_post(wpautop($testimonial['desc'])) ?>
                                        <?php endif; ?>
                                        <?php if (!empty($testimonial['author_name'])) : ?>
                                            <h6 class="name"><?php echo esc_html($testimonial['author_name']) ?></h6>
                                        <?php endif; ?>
                                        <?php if (!empty($testimonial['designation'])) : ?>
                                            <span class="designation"><?php echo esc_html($testimonial['designation']) ?></span>
                                        <?php endif; ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div> <!-- /.inner-container -->
            </div> <!-- /.container -->
        </div>

        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready(function (){
                    var agnTslider = $ (".agn-testimonial-slider");
                    if(agnTslider.length) {
                        agnTslider.owlCarousel({
                            loop:true,
                            nav:true,
                            navText: ["<i class='flaticon-back'></i>" , "<i class='flaticon-next'></i>"],
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            smartSpeed:1200,
                            autoplayHoverPause:true,
                            lazyLoad:true,
                            items:1
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }
}