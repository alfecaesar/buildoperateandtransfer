<?php
get_header();
$opt = get_option( 'rogan_opt' );
$portfolio_layout = !empty($opt['portfolio_layout']) ? $opt['portfolio_layout'] : 'agency';
if(function_exists( 'get_field' )) {
    $layout = (get_field( 'layout' ) != 'default' ) ? get_field( 'layout' ) : $portfolio_layout;
} else {
    $layout = 'agency';
}
    while(have_posts()) : the_post();

        if( $layout == 'standard' ) {
            get_template_part( 'template-parts/project-layouts/project', 'standard' );
        }

        elseif( $layout == 'agency' ) {
            get_template_part( 'template-parts/project-layouts/project', 'agency' );
        }

        elseif( $layout == 'business' ) {
            get_template_part( 'template-parts/project-layouts/project', 'business' );
        }

        elseif( $layout == 'creative' ) {
            get_template_part( 'template-parts/project-layouts/project', 'creative' );
        }

        else {
            get_template_part( 'template-parts/project-layouts/project', 'standard' );
        }

    endwhile;

    $is_related_portfolio = !empty( $opt['is_related_portfolio'] ) ? $opt['is_related_portfolio'] : '';

    if( $is_related_portfolio == '1' ) {
        get_template_part( 'template-parts/project-layouts/project', 'related' );
    }

get_footer();