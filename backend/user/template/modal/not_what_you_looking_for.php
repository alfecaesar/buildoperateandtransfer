<!-- Modal -->
<div class="modal fade" id="modalbox_notwhatyoulookingfor" tabindex="-1" role="dialog" aria-labelledby="Not what you are looking for?" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Not what you are looking for?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-text">
                        <p>If you like, you can suggest a new specialty for us. In the meantime, we will automatically store your project under an existing specialty based on the information you’ve provided. </p>
                    </div>
                    <div class="form-group-label">
                        <label for="bot_name_project">Suggest a new specialty</label>
                    </div>
                    <div class="form-group-tag">
                        <input type="text" name="bot_name_project" id="bot_name_project" class="form-control">
                    </div>
                    <div class="form-group-text">
                        <p><a class="modalbox_specialty_btn" href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_specialty">I will pick an existing specialty myself</a></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="#" class="btn btn-yellow"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>