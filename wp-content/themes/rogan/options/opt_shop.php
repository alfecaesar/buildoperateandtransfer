<?php
// Shop page
Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Shop Settings', 'rogan' ),
    'id'               => 'shop_opt',
    'icon'             => 'dashicons dashicons-cart',
));


// Shop page
Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Shop Header', 'rogan' ),
    'id'               => 'shop_header_opt',
    'icon'             => '',
    'subsection'       => true,
    'fields'           => array(

        array(
            'id'      => 'eheader_for_shop',
            'type'    => 'switch',
            'title'   => esc_html__( 'eCommerce Header', 'rogan' ),
            'subtitle'=> esc_html__( 'eCommerce Header for the Shop Pages', 'rogan' ),
            'on'      => esc_html__( 'Yes', 'rogan' ),
            'off'     => esc_html__( 'No', 'rogan' ),
            'default' => '',
        ),

        array (
            'title'     => esc_html__( 'Title-Bar Banner', 'rogan' ),
            'subtitle'  => esc_html__( 'Title-Bar visibility on the Shop Pages', 'rogan' ),
            'id'        => 'is_shop_banner',
            'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '',
        ),

        array(
            'title'     => esc_html__( 'Logo', 'rogan' ),
            'subtitle'  => esc_html__( 'eCommerce header logo image', 'rogan' ),
            'id'        => 'menu6_logo',
            'type'      => 'media',
            'compiler'  => true,
            'default'   => array(
                'url'   => esc_url(ROGAN_DIR_IMG.'/logo/logo5.svg' )
            ),
        ),

        array(
            'title'     => esc_html__( 'Menu Open Icon', 'rogan' ),
            'subtitle'  => esc_html__( 'eCommerce header menu open icon', 'rogan' ),
            'id'        => 'menu6_open_icon',
            'type'      => 'media',
            'compiler'  => true,
            'default'   => array(
                'url'   => esc_url(ROGAN_DIR_IMG.'/logo/menu.svg' )
            ),
        ),

        array(
            'title'     => esc_html__( 'Menu Close Icon', 'rogan' ),
            'subtitle'  => esc_html__( 'eCommerce header menu close Icon', 'rogan' ),
            'id'        => 'menu6_close_icon',
            'type'      => 'media',
            'compiler'  => true,
            'default'   => array(
                'url'   => esc_url(ROGAN_DIR_IMG.'/icon/icon43.svg' )
            ),
        ),

        array(
            'id'      => 'header6_content',
            'type'    => 'textarea',
            'title'   => esc_html__( 'Header Content', 'rogan' ),
        ),

        array(
            'id'      => 'header6_search',
            'type'    => 'switch',
            'title'   => esc_html__( 'Search Form', 'rogan' ),
            'on'      => esc_html__( 'Show', 'rogan' ),
            'off'     => esc_html__( 'Hide', 'rogan' ),
            'default' => true,
        ),

        array (
            'title'     => esc_html__( 'Account Dropdown', 'rogan' ),
            'subtitle'  => esc_html__( 'The account dropdown will show on the right side of the eCommerce header.', 'rogan' ),
            'id'        => 'ac_dropdown_start',
            'type'      => 'section',
            'indent'    => true,
        ),

        array (
            'id'      => 'ac_menu_title',
            'type'    => 'text',
            'title'   => esc_html__( 'Menu Title', 'rogan' ),
            'default' => esc_html__( 'Account', 'rogan' ),
        ),

        array (
            'title'     => esc_html__( 'Menu Icon', 'rogan' ),
            'subtitle'  => esc_html__( 'eCommerce header menu close Icon', 'rogan' ),
            'id'        => 'ac_menu_icon',
            'type'      => 'media',
            'compiler'  => true,
            'default'   => array (
                'url'   => esc_url(ROGAN_DIR_IMG.'/icon/icon38.svg' )
            ),
            'required'      => array( 'ac_menu_title', '!=', '' )
        ),

        array (
            'id'            => 'ac_items',
            'type'          => 'slides',
            'title'         => esc_html__( 'Menu Dropdown Items', 'rogan' ),
            'show'          => array(
                'title'       => true,
                'description' => false,
                'url'         => true,
            ),
            'content_title' => esc_html__( 'Dropdown Item', 'rogan' ),
            'required'      => array( 'ac_menu_title', '!=', '' )
        ),

        array (
            'id'     => 'ac_dropdown_end',
            'type'   => 'section',
            'indent' => false,
        ),

    ),
));


// Shop page
Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Shop Page', 'rogan' ),
    'id'               => 'shop_page_opt',
    'icon'             => '',
    'subsection'       => true,
    'fields'           => array(
        array(
            'title'     => esc_html__( 'Page title', 'rogan' ),
            'subtitle'  => esc_html__( 'Give here the shop page title', 'rogan' ),
            'desc'      => esc_html__( 'This text will show on the shop page banner', 'rogan' ),
            'id'        => 'shop_title',
            'type'      => 'text',
            'default'   => esc_html__( 'Shop', 'rogan' ),
        ),
        array(
            'title'     => esc_html__( 'Shop Page Subtitle', 'rogan' ),
            'id'        => 'shop_subtitle',
            'type'      => 'textarea',
        ),
        array(
            'title'     => esc_html__( 'Title bar background', 'rogan' ),
            'subtitle'  => esc_html__( 'Upload image file as Shop page title bar background', 'rogan' ),
            'id'        => 'shop_header_bg',
            'type'      => 'media',
        ),
    ),
));


// Product Single Options
Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Product Single', 'rogan' ),
    'id'               => 'product_single_opt',
    'subsection'       => true,
    'icon'             => '',
    'fields'           => array(
        array(
            'title'     => esc_html__( 'Related Products Title', 'rogan' ),
            'id'        => 'related_products_title',
            'type'      => 'text',
            'default'   => esc_html__( 'Related products', 'rogan' ),
        ),
        array(
            'title'     => esc_html__( 'Related Products Subtitle', 'rogan' ),
            'id'        => 'related_products_subtitle',
            'type'      => 'textarea',
        ),
    )
));