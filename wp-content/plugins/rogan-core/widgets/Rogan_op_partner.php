<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_op_partner extends Widget_Base {

    public function get_name() {
        return 'rogan_op_partner';
    }

    public function get_title() {
        return esc_html__( 'Client Logos', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-logo';
    }

    public function get_keywords() {
        return ['client', 'partner', 'logo'];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ------------------------------ slider Logo ------------------------------
        $this->start_controls_section(
            'partner_section',
            [
                'label' => esc_html__( 'Partner', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'alt', [
                'label' => esc_html__( 'Logo alt Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Company Name'
            ]
        );

        $repeater->add_control(
            'partner_logo', [
                'label' => esc_html__( 'Logo', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'partner_url', [
                'label' => esc_html__( 'Logo URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'column', [
                'label' => esc_html__( 'Column', 'rogan-core' ),
                'description' => esc_html__( 'Total 12 column in a row. If you select 4 column, the rest of the column will take 8 column space', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'default' => '3',
                'options' => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '6' => '6',
                    '8' => '8',
                ]
            ]
        );

        $this->add_control(
            'all_partners', [
                'label' => esc_html__( 'Logos', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ alt }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .op-partner-section-one' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();

        ?>
        <div class="op-partner-section-one">
            <div class="container">
                <div class="row">
                <?php 
                $x = 'a';
                if ( !empty($settings['all_partners'] )) {
                    foreach ($settings['all_partners'] as $partner) {
                        $partner_logo = isset($partner['partner_logo']['id']) ? $partner['partner_logo']['id'] : '';
                        $partner_logo = wp_get_attachment_image_src($partner_logo, 'full');
                        $target = $partner['partner_url']['is_external'] ? 'target="_blank"' : '';
                        ?>
                        <?php if (!empty($partner_logo[0])) : ?>
                        <div class="col-lg-<?php echo $partner['column'] ?> col-md-4 col-6">
                            <div class="img-box bx-<?php echo $x; ?>">
                                <a href="<?php echo esc_url($partner['partner_url']['url']) ?>" <?php echo $target ?>>
                                    <img src="<?php echo $partner_logo[0] ?>" alt="<?php echo esc_attr($partner['alt']) ?>">
                                </a>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php
                        $x++;
                    }
                }
                ?>
                </div>
            </div>
        </div> <!-- /.op-partner-section-one -->
        <?php
    }
}