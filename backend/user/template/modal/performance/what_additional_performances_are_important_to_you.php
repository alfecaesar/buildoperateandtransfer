<!-- Modal -->
<div class="modal fade" id="modalbox_performance_8" tabindex="-1" role="dialog" aria-labelledby="What additional performances are important to you" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">What additional performances are important to you</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">What additional performances are important to you</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="additional_performances_important" id="additional_performances_important" class="form-control" multiple>
                            <option id="Africa">Africa</option>
                            <option id="Americas">Americas</option>
                            <option id="Antarctica">Antarctica</option>
                            <option id="Asia">Asia</option>
                            <option id="Europe">Europe</option>
                            <option id="Oceania">Oceania</option>
                            <option id="Amazon">Amazon</option>
                            <option id="Bing">Bing</option>
                            <option id="Facebook ">Facebook </option>
                            <option id="Google">Google</option>
                            <option id="Instagram">Instagram</option>
                            <option id="Yahoo">Yahoo</option>
                            <option id="Linkedin">Linkedin</option>
                            <option id="Pinterest">Pinterest</option>
                            <option id="Snapchat">Snapchat</option>
                            <option id="Twitter">Twitter</option>
                            <option id="YouTube">YouTube</option>
                            <option id="Website">Website</option>
                            <option id="Blog">Blog</option>
                            <option id="eBay">eBay</option>
                            <option id="Reddit">Reddit</option>
                            <option id="TikTok">TikTok</option>
                            <option id="Tumblr">Tumblr</option>
                            <option id="Twitch">Twitch</option>
                            <option id="WeChat">WeChat</option>
                            <option id="WhatsApp">WhatsApp</option>
                            <option id="Advertising on Social Media">Advertising on Social Media</option>
                            <option id="Affiliata Marketing">Affiliata Marketing</option>
                            <option id="Influencer Marketing">Influencer Marketing</option>
                            <option id="Social Media Management">Social Media Management</option>
                            <option id="Content Marketing">Content Marketing</option>
                            <option id="Email Marketing">Email Marketing</option>
                            <option id="Paid Media">Paid Media</option>
                            <option id="PPC Marketing ">PPC Marketing </option>
                            <option id="Search Engine Marketing (SEM)">Search Engine Marketing (SEM)</option>
                            <option id="Marketing Automation">Marketing Automation</option>
                            <option id="Fulfilment Automation">Fulfilment Automation</option>
                            <option id="Inventory Management">Inventory Management</option>
                            <option id="Eliminating Physical Stores">Eliminating Physical Stores</option>
                            <option id="Intutive Navigation">Intutive Navigation</option>
                            <option id="Recommended Searches">Recommended Searches</option>
                            <option id="Similar products">Similar products</option>
                            <option id="Qoo10">Qoo10</option>
                            <option id="Lazada">Lazada</option>
                            <option id="Shoppee">Shoppee</option>
                            <option id="Carousell">Carousell</option>
                            <option id="Ezbuy">Ezbuy</option>
                            <option id="Ebay">Ebay</option>
                            <option id="Zalora">Zalora</option>
                            <option id="Weekly/Monthly Discounts">Weekly/Monthly Discounts</option>
                            <option id="Prelaunch offers">Prelaunch offers</option>
                            <option id="Holiday and seasonal deals">Holiday and seasonal deals</option>
                            <option id="Abandones cart offers">Abandones cart offers</option>
                            <option id="Email/newsletter subscription offer">Email/newsletter subscription offer</option>
                            <option id="Incentives for likes, follows and shares on social media">Incentives for likes, follows and shares on social media</option>
                            <option id="Referral promos">Referral promos</option>
                            <option id="First-time shopper offer">First-time shopper offer</option>
                            <option id="Minimum purchase discount">Minimum purchase discount</option>
                            <option id="Exclusive social offers">Exclusive social offers</option>
                            <option id="Customer Loyalty Offers">Customer Loyalty Offers</option>
                            <option id="Exit Intent offers">Exit Intent offers</option>
                            <option id="Retargeted promotions">Retargeted promotions</option>
                            <option id="Influencer offers">Influencer offers</option>
                            <option id="Customer loyalty programs and member incentives">Customer loyalty programs and member incentives</option>
                            <option id="Offers for purchasing online">Offers for purchasing online</option>
                            <option id="Coupons for purchasing in-person">Coupons for purchasing in-person</option>
                            <option id="Event attendance offers">Event attendance offers</option>
                            <option id="Bulk or Group buys">Bulk or Group buys</option>
                            <option id="Customer milestone discounts">Customer milestone discounts</option>
                            <option id="Revenue">Revenue</option>
                            <option id="Likes, Follows and Shares for product">Likes, Follows and Shares for product</option>
                            <option id="Positive comments ">Positive comments </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>