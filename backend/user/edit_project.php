<?php include '../user/template/header.php';


include '../dbconfig.php';


?>

<?php include '../user/template/topbar.php';

?>


<div class="container" id="createproject-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
                <nav id="side-nav">
                    <ul>
                        <li><a href="javascript:void(0)" id="tab_side_a_1" onclick="tabGo(1)" class="active"><span><i class="fas fa-arrow-alt-circle-right"></i> Title</span></a></li>
                        <li><a href="javascript:void(0)" id="tab_side_a_2" onclick="tabGo(2)"><span><i class="fas fa-arrow-alt-circle-right"></i> Description</span></a></li>
                        <li><a href="javascript:void(0)" id="tab_side_a_3" onclick="tabGo(3)"><span><i class="fas fa-arrow-alt-circle-right"></i> Performance</span></a></li>
                        <li><a href="javascript:void(0)" id="tab_side_a_4" onclick="tabGo(4)"><span><i class="fas fa-arrow-alt-circle-right"></i> Features</span></a></li>
                        <li><a href="javascript:void(0)" id="tab_side_a_5" onclick="tabGo(5)"><span><i class="fas fa-arrow-alt-circle-right"></i> Budget & Timeframe</span></a></li>
                        <li><a href="javascript:void(0)" id="tab_side_a_6" onclick="tabGo(6)"><span><i class="fas fa-arrow-alt-circle-right"></i> Review</span></a></li>
                    </ul>
                </nav>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">

        <?php
                $project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

                        $datas = $database->select("project", [
                            "project_name",
                            "category",
                            "category_specialty",
                            "suggest_specialty",
                            "description",
                            "ideal_minimum_viable_product",
                            "ideal_minimum_viable_product_others",
                            "additional_project_files",
                            "target_markets",
                            "increased_social_media_awareness",
                            "lower_acquisition_costs_by",
                            "locate_product_faster",
                            "compare_shopping_on",
                            "enable_deals_bargains_coupons_and_group_buys",
                            "create_market_for_products_to_achieve",
                            "additional_performances_important",
                            "performance_not_what_you_looking_for",
                            "ecommerce_development_deliverables",
                            "ecommerce_platforms",
                            "ecommerce_development_skills",
                            "ecommerce_payment_systems",
                            "ecommerce_database",
                            "business_size_experience",
                            "additional_features_are_important_to_you",
                            "features_not_what_you_looking_for",
                            "specific_budget",
                            "specific_timeframe_to_complete_min",
                            "specific_timeframe_to_complete_max",
                            "how_long_prepared_to_work_with_min",
                            "how_long_prepared_to_work_with_max",
                            "how_would_you_like_to_work_with",
                            "when_would_you_like_to_pay",
                            "contractors_email",
                            "message_to_contractors",
                            "allow_contractors_to_bid_other_projects",
                            "user_name",
                            "user_email"
                        ], [
                            "user_email" => $_SESSION["user_email"],
                            "project_id" => $project_id
                        ]);


                        foreach($datas as $data)
                        {
        ?>

        <form method="get" name="create_project_form" action="save_edit_project.php">
            <input type="hidden" name="project_id" id="project_id" value="<?php echo $project_id; ?>" />

            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab" id="tab_content_1">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Get Started</h2>
                            <p>Step 1 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" onclick="tabGo(0)" class="btn btn-grey"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" onclick="tabGo(2)" class="btn btn-yellow"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="project_name">Enter the name of BOT Project</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="project_name" id="project_name" class="form-control" value="<?php echo $data['project_name']; ?>">
                        </div>
                        <div class="form-group-text">
                            <p>Here are some good examples:</p>
                            <ul>
                                <li>E-commerce for fashion store</li>
                                <li>Database to gather financial advisors</li>
                                <li>SAAS for corporate services company</li>
                            </ul>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label>Project Category</label>
                        </div>
                        <div class="form-group-text">
                            <p>Let’s categorize your project, which will help us personalize your project details and provide you a more accurate proposal</p>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="category_1"> Web, Mobile & Software Dev
                                <input type="radio" id="category_1" name="category" value="Web, Mobile & Software Dev">
                                <span class="checkmark"></span>
                            </label>
                            <div class="popup-btn-c"><a class="popup-btn" onclick="prepop_speciality(1,'specialty_1_val')" href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_specialty">Choose a Specialty</a> <span class="specialty_val" id="specialty_1_val"></span></span></div>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="category_2"> Data Science & Analytics
                                <input type="radio" id="category_2" name="category" value="Data Science & Analytics">
                                <span class="checkmark"></span>
                            </label>
                            <div class="popup-btn-c"><a class="popup-btn" onclick="prepop_speciality(2,'specialty_2_val')" href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_specialty">Choose a Specialty</a> <span class="specialty_val" id="specialty_2_val"></span></span></div>
                        </div>
                    </div>

                    

                    <input type="hidden" name="category_specialty" id="category_specialty">
                </div>
            </div>
            <!-- END OF STEP 1 -->

            <!-- START OF STEP 2 -->
            <div class="content_col_box" id="tab_content_2" style="display:none;">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Description</h2>
                            <p>Step 2 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" class="btn btn-grey" onclick="tabGo(1)"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" class="btn btn-yellow" onclick="tabGo(3)"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="description">A good description includes:</label>
                        </div>
                        <div class="form-group-text">
                            <ul>
                                <li>What the minimum viable product deliverable is</li>
                                <li>Performance target to achieve for the minimum viable product</li>
                                <li>Whether you are looking for a freelancer or a team</li>
                                <li>Anything unique about project or team</li>
                            </ul>
                        </div>
                        <div class="form-group-tag">
                            <textarea name="description" id="description" class="form-control"><?php echo $data['description']; ?></textarea>
                            <span>minimum 50 characters</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label>What do you consider an ideal minimum viable product?</label>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="ideal_minimum_viable_product_1"> A landing page that succeeds to sign up people to a waiting list
                                <input type="radio" id="ideal_minimum_viable_product_1" name="ideal_minimum_viable_product" value="A landing page that succeeds to sign up people to a waiting list">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="ideal_minimum_viable_product_2"> A video that succeeds to sign up people on a waiting list
                                <input type="radio" id="ideal_minimum_viable_product_2" name="ideal_minimum_viable_product" value="A video that succeeds to sign up people on a waiting list">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="ideal_minimum_viable_product_3"> A form that collects positive feedbacks
                                <input type="radio" id="ideal_minimum_viable_product_3" name="ideal_minimum_viable_product" value="A form that collects positive feedbacks">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="ideal_minimum_viable_product_4"> A machine that test a hypothesis
                                <input type="radio" id="ideal_minimum_viable_product_4" name="ideal_minimum_viable_product" value="A machine that test a hypothesis">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="ideal_minimum_viable_product_5"> Others
                                <input type="radio" id="ideal_minimum_viable_product_5" name="ideal_minimum_viable_product" value="Others">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="ideal_minimum_viable_product_others" id="ideal_minimum_viable_product_others" class="form-control" value="<?php echo $data['ideal_minimum_viable_product_others']; ?>">
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="additional_project_files">Additional project files (optional)</label>
                        </div>
                        <div class="form-group-tag">
                            <div class="upload-box">
                                <input type="file" multiple name="additional_project_files" id="additional_project_files" value="<?php echo $data['additional_project_files']; ?>">
                                <p>drag or <a href="#">upload</a> files</p>
                            </div>
                            <span>You may attach up to 5 files under 100 MB each</span>
                        </div>
                    </div>

                </div>
            </div>
            <!-- END OF STEP 2 -->


            <!-- START OF STEP 3 -->
            <div class="content_col_box" id="tab_content_3" style="display:none;">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Performance</h2>
                            <p>Step 3 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" class="btn btn-grey" onclick="tabGo(2)"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" class="btn btn-yellow" onclick="tabGo(4)"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group no-border mar-bot-25">
                        <div class="form-group-label">
                            <label for="bot_name_project">What performance targets are most important to you in <em><?php echo $data['project_name']; ?></em></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Target markets</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_1">+ Add</a>
                            <?php include '../user/template/modal/performance/target_markets.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Increased Social Media Awareness</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_2">+ Add</a>
                            <?php include '../user/template/modal/performance/increased_social_media_awareness.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Lower Acquisition Costs by </label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_3">+ Add</a>
                            <?php include '../user/template/modal/performance/lower_acquisition_costs_by.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Locate Product Faster</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_4">+ Add</a>
                            <?php include '../user/template/modal/performance/locate_product_faster.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Compare Shopping on</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_5">+ Add</a>
                            <?php include '../user/template/modal/performance/compare_shopping_on.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Enable Deals, Bargains, Coupons and Group buys </label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_6">+ Add</a>
                            <?php include '../user/template/modal/performance/enable_deals_bargains_coupons_and_group_buys.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Create market for specific products to achieve</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_7">+ Add</a>
                            <?php include '../user/template/modal/performance/create_market_for_specific_products_to_achieve.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">What additional performances are important to you?</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_performance_8">+ Add</a>
                            <?php include '../user/template/modal/performance/what_additional_performances_are_important_to_you.php';?>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="performance_not_what_you_looking_for">Not what you are looking for?</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="performance_not_what_you_looking_for" id="performance_not_what_you_looking_for" class="form-control" value="<?php echo $data['performance_not_what_you_looking_for']; ?>">
                        </div>
                    </div>


                </div>
            </div>
            <!-- END OF STEP 3 -->



            <!-- START OF STEP 4 -->
            <div class="content_col_box" id="tab_content_4" style="display:none;">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Features</h2>
                            <p>Step 4 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" class="btn btn-grey" onclick="tabGo(3)"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" class="btn btn-yellow" onclick="tabGo(5)"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group no-border mar-bot-25">
                        <div class="form-group-label">
                            <label for="bot_name_project">What features most important to you in <em><?php echo $data['project_name']; ?></em></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">eCommerce development deliverables</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_1">+ Add</a>
                            <?php include '../user/template/modal/features/ecommerce_development_deliverables.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">eCommerce Platforms</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_2">+ Add</a>
                            <?php include '../user/template/modal/features/ecommerce_platforms.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">eCommerce Development Skills </label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_3">+ Add</a>
                            <?php include '../user/template/modal/features/ecommerce_development_skills.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">eCommerce Payment Systems</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_4">+ Add</a>
                            <?php include '../user/template/modal/features/ecommerce_payment_systems.php';?>
                        </div>
                    </div>
 
                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">eCommerce Database</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_5">+ Add</a>
                            <?php include '../user/template/modal/features/ecommerce_database.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">Business Size Experience </label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_6">+ Add</a>
                            <?php include '../user/template/modal/features/business_size_experience.php';?>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="bot_name_project">What additional features are important to you?</label>
                            <a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_features_7">+ Add</a>
                            <?php include '../user/template/modal/features/what_additional_features_are_important_to_you.php';?>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="features_not_what_you_looking_for">Not what you are looking for?</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="features_not_what_you_looking_for" id="features_not_what_you_looking_for" class="form-control" value="<?php echo $data['features_not_what_you_looking_for']; ?>">
                        </div>
                    </div>


                </div>
            </div>
            <!-- END OF STEP 4 -->


            <!-- START OF STEP 5 -->
            <div class="content_col_box" id="tab_content_5" style="display:none;">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Budget & Timeframe</h2>
                            <p>Step 5 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" class="btn btn-grey" onclick="tabGo(4)"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" class="btn btn-yellow" onclick="tabGo(6)"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="specific_budget">Do you have a specific budget for the minimum viable product? </label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="specific_budget" id="specific_budget" class="form-control" value="<?php echo $data['specific_budget']; ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="specific_timeframe_to_complete_min">Do you have a specific timeframe to complete the minimum viable product?</label>
                        </div>
                        <div class="form-group-tag">
                            <div class="row">
                                <div class="col-6 text-left date input-group">
                                    <input type="text" name="specific_timeframe_to_complete_min" id="specific_timeframe_to_complete_min" class="form-control" value="<?php echo $data['specific_timeframe_to_complete_min']; ?>"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="col-6 text-right date input-group">
                                    <input type="text" name="specific_timeframe_to_complete_max" id="specific_timeframe_to_complete_max" class="form-control" value="<?php echo $data['specific_timeframe_to_complete_max']; ?>"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="how_long_prepared_to_work_with_min">How long are you prepared to work with the freelancer or team?</label>
                        </div>
                        <div class="form-group-tag">
                            <div class="row">
                                <div class="col-6 text-left date input-group">
                                    <input type="text" name="how_long_prepared_to_work_with_min" id="how_long_prepared_to_work_with_min" class="form-control" value="<?php echo $data['how_long_prepared_to_work_with_min']; ?>"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                                </div>
                                <div class="col-6 text-right date input-group">
                                    <input type="text" name="how_long_prepared_to_work_with_max" id="how_long_prepared_to_work_with_max" class="form-control" value="<?php echo $data['how_long_prepared_to_work_with_max']; ?>"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="how_would_you_like_to_work_with">How would you like to work with the freelancer or team?</label>
                        </div>
                        <div class="form-group-tag">
                            <div class="row btn_val_c how_would_like_to_work">
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'how_would_you_like_to_work_with')" class="inp-bg-btn">Build, Operate and Transfer</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'how_would_you_like_to_work_with')" class="inp-bg-btn">Build, Own, Operate and Transfer</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'how_would_you_like_to_work_with')" class="inp-bg-btn">Turnkey</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'how_would_you_like_to_work_with')" class="inp-bg-btn">Operations and Management</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="when_would_you_like_to_pay">When would you like to pay the freelancer or team?</label>
                        </div>
                        <div class="form-group-tag">
                            <div class="row btn_val_c when_would_pay">
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'when_would_you_like_to_pay')" class="inp-bg-btn">Pay upon Minimum Viable Product Delivery</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'when_would_you_like_to_pay')" class="inp-bg-btn">Pay per month</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'when_would_you_like_to_pay')" class="inp-bg-btn">Pay per hour</button>
                                </div>
                                <div class="col-3 ">
                                    <button type="button" onclick="clbtnVal(this,'when_would_you_like_to_pay')" class="inp-bg-btn">Pay upon Full Product Delivery</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="how_would_you_like_to_work_with" id="how_would_you_like_to_work_with" value="<?php echo $data['how_would_you_like_to_work_with']; ?>">
                    <input type="hidden" name="when_would_you_like_to_pay" id="when_would_you_like_to_pay" value="<?php echo $data['when_would_you_like_to_pay']; ?>">

                </div>
            </div>
            <!-- END OF STEP 5 -->




            <!-- START OF STEP 6 -->
            <div class="content_col_box" id="tab_content_6" style="display:none;">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Review</h2>
                            <p>Step 6 of 6</p>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" class="btn btn-grey" onclick="tabGo(5)"><span><i class="fas fa-angle-double-left"></i> Back</a>
                            <a href="javascript:void(0)" class="btn btn-yellow" onclick="createProjectSubmit(document.create_project_form)"><span>Next <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Title</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Enter the name of BOT Project</p>
                            <p id="cp_sum_1"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Project Category</p>
                            <p id="cp_sum_2"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Specialty</p>
                            <p id="cp_sum_3"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Description</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">A good description includes</p>
                            <p id="cp_sum_4"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What do you consider an ideal minimum viable product?</p>
                            <p id="cp_sum_5"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Additional project files</p>
                            <p id="cp_sum_6"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Performance</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Target markets</p>
                            <p id="cp_sum_7"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Increased Social Media Awareness</p>
                            <p id="cp_sum_8"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Lower Acquisition Costs by </p>
                            <p id="cp_sum_9"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Locate Product Faster </p>
                            <p id="cp_sum_10"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Compare Shopping on </p>
                            <p id="cp_sum_11"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Enable Deals, Bargains, Coupons and Group buys </p>
                            <p id="cp_sum_12"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Create market for specific products to achieve </p>
                            <p id="cp_sum_13"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What additional performances are important to you? </p>
                            <p id="cp_sum_14"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Not what you are looking for? </p>
                            <p id="cp_sum_15"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Features</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce development deliverables</p>
                            <p id="cp_sum_16"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Platforms</p>
                            <p id="cp_sum_17"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Development Skills </p>
                            <p id="cp_sum_18"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Payment Systems </p>
                            <p id="cp_sum_19"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Database </p>
                            <p id="cp_sum_20"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Business Size Experience </p>
                            <p id="cp_sum_21"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What additional features are important to you? </p>
                            <p id="cp_sum_22"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Not what you are looking for? </p>
                            <p id="cp_sum_23"></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Budget & Timeframe</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Do you have a specific budget for the minimum viable product?</p>
                            <p id="cp_sum_24"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Do you have a specific timeframe to complete the minimum viable product?</p>
                            <p id="cp_sum_25"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">How long are you prepared to work with the freelancer or team?</p>
                            <p id="cp_sum_26"></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">How would you like to work with the freelancer or team?</p>
                            <p id="cp_sum_27"></p>
                        </div>

                        <div class="form-group-text">
                            <p class="text-bold">When would you like to pay the freelancer or team?</p>
                            <p id="cp_sum_28"></p>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <h4>Contractors</h4>
                            <p>Recommend a contractor you know to put in a proposal.</p>
                        </div>
                    </div>
                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="contractors_email">Email Address</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" placeholder="Separate emails using commas" name="contractors_email" id="contractors_email" class="form-control">
                        </div>
                    </div>
                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="message_to_contractors">Message</label>
                        </div>
                        <div class="form-group-tag">
                            <textarea name="message_to_contractors" id="message_to_contractors" placeholder="Write a message to the contractor" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group no-border">
                        <div class="form-group-tag">
                            <label class="form-field-radio" for="allow_contractors_to_bid_other_projects"> Also allow these contractors to bid for other projects on Stackbuild 
                                <input type="radio" id="allow_contractors_to_bid_other_projects" name="allow_contractors_to_bid_other_projects" value="Yes">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                    </div>

                    <input type="hidden" name="user_name" id="user_name" value="<?php echo $_SESSION["user_name"]; ?>">
                    <input type="hidden" name="user_email" id="user_email" value="<?php echo $_SESSION["user_email"]; ?>">


                </div>
            </div>
            <!-- END OF STEP 6 -->

        </form>

        <?php
            }
        ?>


        </div>
    </div>
</div>

<?php include '../user/template/modal/choose_specialty.php';?>
<?php include '../user/template/modal/not_what_you_looking_for.php';?>

<script type="text/javascript">
    var category = '<?php echo $data['category']; ?>';
    var category_specialty = '<?php echo $data['category_specialty']; ?>';
    var ideal_minimum_viable_product = '<?php echo $data['ideal_minimum_viable_product']; ?>';
    var target_markets = '<?php echo $data['target_markets']; ?>';
    var increased_social_media_awareness = '<?php echo $data['increased_social_media_awareness']; ?>';
    var lower_acquisition_costs_by = '<?php echo $data['lower_acquisition_costs_by']; ?>';
    var locate_product_faster = '<?php echo $data['locate_product_faster']; ?>';
    var compare_shopping_on = '<?php echo $data['compare_shopping_on']; ?>';
    var enable_deals_bargains_coupons_and_group_buys = '<?php echo $data['enable_deals_bargains_coupons_and_group_buys']; ?>';
    var create_market_for_products_to_achieve = '<?php echo $data['create_market_for_products_to_achieve']; ?>';
    var additional_performances_important = '<?php echo $data['additional_performances_important']; ?>';

    var ecommerce_development_deliverables = '<?php echo $data['ecommerce_development_deliverables']; ?>';
    var ecommerce_platforms = '<?php echo $data['ecommerce_platforms']; ?>';
    var ecommerce_development_skills = '<?php echo $data['ecommerce_development_skills']; ?>';
    var ecommerce_payment_systems = '<?php echo $data['ecommerce_payment_systems']; ?>';
    var ecommerce_database = '<?php echo $data['ecommerce_database']; ?>';
    var business_size_experience = '<?php echo $data['business_size_experience']; ?>';
    var additional_features_are_important_to_you = '<?php echo $data['additional_features_are_important_to_you']; ?>';


    window.onload = function(e){ 

        
    
        // category
        prePopCheckRadio('category',category);

        // category specialty
        if(category === 'Web, Mobile & Software Dev'){
            $('#specialty_1_val').text(category_specialty)
        }
        else if(category === 'Data Science & Analytics'){
            $('#specialty_2_val').text(category_specialty)
        }

        prePopCheckRadio('ideal_minimum_viable_product',ideal_minimum_viable_product);
        prePopSelect2('target_markets',target_markets);
        prePopSelect2('increased_social_media_awareness',increased_social_media_awareness);
        prePopSelect2('lower_acquisition_costs_by',lower_acquisition_costs_by);
        prePopSelect2('locate_product_faster',locate_product_faster);
        prePopSelect2('compare_shopping_on',compare_shopping_on);
        prePopSelect2('enable_deals_bargains_coupons_and_group_buys',enable_deals_bargains_coupons_and_group_buys);
        prePopSelect2('create_market_for_products_to_achieve',create_market_for_products_to_achieve);
        prePopSelect2('additional_performances_important',additional_performances_important);

        prePopSelect2('ecommerce_development_deliverables',ecommerce_development_deliverables);
        prePopSelect2('ecommerce_platforms',ecommerce_platforms);
        prePopSelect2('ecommerce_development_skills',ecommerce_development_skills);
        prePopSelect2('ecommerce_payment_systems',ecommerce_payment_systems);
        prePopSelect2('ecommerce_database',ecommerce_database);
        prePopSelect2('business_size_experience',business_size_experience);
        prePopSelect2('additional_features_are_important_to_you',additional_features_are_important_to_you);

        prePopButtonText($('.how_would_like_to_work'),'<?php echo $data['how_would_you_like_to_work_with']; ?>');
        prePopButtonText($('.when_would_pay'),'<?php echo $data['when_would_you_like_to_pay']; ?>');

    }
</script>

<?php include '../user/template/footer.php';?>
