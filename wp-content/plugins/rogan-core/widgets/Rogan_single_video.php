<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_single_video extends Widget_Base {

    public function get_name() {
        return 'rogan_single_video';
    }

    public function get_title() {
        return esc_html__( 'Featured Video', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-youtube';
    }

    public function get_script_depends() {
        return [ 'fancybox' ];
    }

    public function get_style_depends() {
        return [ 'fancybox' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {


        // -------------------- Featured Video ------------------------------
        $this->start_controls_section(
            'video_sec', [
                'label' => esc_html__( 'Featured Video', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'video_url', [
                'label' => esc_html__( 'Video URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true
            ]
        );

        $this->add_control(
            'video_image', [
                'label' => esc_html__( 'Video Poster Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url'   => plugins_url('images/icon/icon47.svg', __FILE__),
                ]
            ]
        );

        $this->add_control(
            'bg_image', [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url'   => plugins_url('images/home/12.jpg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Background ------------------------------//
        $this->start_controls_section(
            'style_bg_overlay', [
                'label' => esc_html__( 'Style Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_bg', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .video-action-banner-one .overlay' => 'background: {{VALUE}};',
                ],
            ]
        );


        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        ?>
        <div class="video-action-banner-one mt-95" style="background: url(<?php echo esc_url($settings['bg_image']['url']) ?>);
                background-size: 120%;
                background-repeat: no-repeat;">
            <div class="overlay">
                <?php if (!empty($settings['video_image']['url'])) : ?>
                    <a data-fancybox href="<?php echo esc_url($settings['video_url']['url']) ?>" class="video-button fancybox">
                        <img src="<?php echo esc_url($settings['video_image']['url']) ?>" alt="">
                    </a>
                <?php endif; ?>
            </div> <!-- /.overlay -->
        </div> <!-- /.video-action-banner-one -->
        <?php
    }
}