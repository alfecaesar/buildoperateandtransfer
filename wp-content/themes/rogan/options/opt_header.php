<?php
// Header Section
Redux::setSection( 'rogan_opt', array (
    'title'            => esc_html__( 'Header Settings', 'rogan' ),
    'id'               => 'header_sec',
    'customizer_width' => '400px',
    'icon'             => 'dashicons dashicons-arrow-up-alt2',
) );


// Header Section
Redux::setSection( 'rogan_opt', array (
    'title'            => esc_html__( 'Header Content', 'rogan' ),
    'id'               => 'header_content_sec',
    'customizer_width' => '400px',
    'icon'             => '',
    'subsection'       => true,
    'fields'           => array (

        array (
            'title'     => esc_html__( 'Header Style', 'rogan' ),
            'id'        => 'header_style',
            'type'      => 'image_select',
            'default'   => '1',
            'options'   => array (
                '1' => array (
                    'alt' => esc_html__( 'Header Agency', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/1.png' ),
                ),
                '2' => array (
                    'alt' => esc_html__( 'Header SEO', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/2.png' ),
                ),
                '3' => array (
                    'alt' => esc_html__( 'Header Portfolio', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/3.png' ),
                ),
                '4' => array (
                    'alt' => esc_html__( 'Header Saas', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/4.png' ),
                ),
                '5' => array (
                    'alt' => esc_html__( 'Header Mobile App', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/5.png' ),
                ),
                '6' => array (
                    'alt' => esc_html__( 'eCommerce Header', 'rogan' ),
                    'img' => esc_url(ROGAN_DIR_IMG.'/headers/6.png' ),
                ),
            )
        ),

        array (
            'id'        => 'header6_warning',
            'type'      => 'info',
            'style'     => 'warning',
            'title'      => esc_html__( 'Warning', 'rogan' ),
            'desc'      => wp_kses_post(__( 'You can customize the eCommerce Header from <b> Theme Settings > Shop Settings > Shop Header </b>.', 'rogan' )),
            'required'  => array ( 'header_style', '=', '6' ),
        ),

        array (
            'id'        => 'header6_minicart',
            'type'      => 'switch',
            'title'     => esc_html__( 'Mini Cart', 'rogan' ),
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => true,
            'required'  => array ( 'header_style', '=', '6' ),
        ),

        array (
            'id'      => 'header_content',
            'type'    => 'textarea',
            'title'   => esc_html__( 'Header Content', 'rogan' ),
            'subtitle'=> esc_html__( 'You can input here mobile number or an important short text.', 'rogan' ),
        ),

        array(
            'id'            => 'header_content_typo',
            'type'          => 'typography',
            'title'         => esc_html__( 'Typography', 'rogan' ),
            'subtitle'      => esc_html__( 'Header Content typography options', 'rogan' ),
            'output'        => '.theme-menu-one .header-right-widget .call-us',
            'required'  => array ( 'header_content', '!=', '' )
        ),

        array (
            'title'     => esc_html__( 'Link Color', 'rogan' ),
            'id'        => 'header_content_link_color',
            'output'    => '.theme-menu-one .header-right-widget .call-us a',
            'type'      => 'color',
            'required'  => array ( 'header_content', '!=', '' )
        ),

        array (
            'id'      => 'is_header_sticky',
            'type'    => 'switch',
            'title'   => esc_html__( 'Header Sticky', 'rogan' ),
            'on'      => esc_html__( 'Enable', 'rogan' ),
            'off'     => esc_html__( 'Disable', 'rogan' ),
            'default' => true,
        ),

    )
) );


// Logo
Redux::setSection( 'rogan_opt', array (
    'title'            => esc_html__( 'Logo', 'rogan' ),
    'id'               => 'logo_opt',
    'subsection'       => true,
    'icon'             => '',
    'fields'           => array (

        array (
            'title'     => esc_html__( 'Main logo', 'rogan' ),
            'subtitle'  => esc_html__( 'Upload here a image file for your logo', 'rogan' ),
            'id'        => 'main_logo',
            'type'      => 'media',
            'compiler'  => true,
            'default'   => array (
                'url'   => esc_url(ROGAN_DIR_IMG.'/logo.svg' )
            )
        ),

        array (
            'title'     => esc_html__( 'Sticky logo', 'rogan' ),
            'subtitle'  => esc_html__( 'Upload here a image file for your logo', 'rogan' ),
            'id'        => 'sticky_logo',
            'type'      => 'media',
            'compiler'  => true,
        ),

        array (
            'title'     => esc_html__( 'Logo dimensions', 'rogan' ),
            'subtitle'  => esc_html__( 'Set a custom height width for your upload logo.', 'rogan' ),
            'id'        => 'logo_dimensions',
            'type'      => 'dimensions',
            'units'     => array ( 'em','px','%' ),
            'output'    => '.theme-menu-one.d-align-item .logo, .theme-menu-one.d-align-item.fixed .logo, .theme-menu-two .logo, .theme-menu-two.fixed .logo'
        ),

        array (
            'title'     => esc_html__( 'Padding', 'rogan' ),
            'subtitle'  => esc_html__( 'Padding around the logo. Input the padding as clockwise (Top Right Bottom Left)', 'rogan' ),
            'id'        => 'logo_padding',
            'type'      => 'spacing',
            'output'    => array ( '.logo' ),
            'mode'      => 'padding',
            'units'     => array ( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
            'units_extended' => 'true',
        ),
    )
) );


// Menu action button
Redux::setSection( 'rogan_opt', array (
    'title'            => esc_html__( 'Action Button', 'rogan' ),
    'id'               => 'menu_action_btn_opt',
    'subsection'       => true,
    'icon'             => '',
    'fields'           => array (
        array (
            'title'     => esc_html__( 'Button Visibility', 'rogan' ),
            'id'        => 'is_menu_btn',
            'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
        ),

        array (
            'title'     => esc_html__( 'Button label', 'rogan' ),
            'subtitle'  => esc_html__( 'Leave the button label field empty to hide the menu action button.', 'rogan' ),
            'id'        => 'menu_btn_label',
            'type'      => 'text',
            'default'   => esc_html__( 'Get Started', 'rogan' ),
            'required'  => array ( 'is_menu_btn', '=', '1' )
        ),

        array (
            'title'     => esc_html__( 'Button URL', 'rogan' ),
            'id'        => 'menu_btn_url',
            'type'      => 'text',
            'default'   => '#',
            'required'  => array ( 'is_menu_btn', '=', '1' )
        ),

        array (
            'title'     => esc_html__( 'Button Icon', 'rogan' ),
            'subtitle'  => esc_html__( 'We used here flaticon class. You can also place here Font-awesome icon class', 'rogan' ),
            'id'        => 'menu_btn_icon',
            'type'      => 'text',
            'default'   => 'flaticon-next',
            'required'  => array (
                array ( 'header_style', '=', '1' ),
                array ( 'is_menu_btn', '=', '1' ),
            )
        ),

        array (
            'title'     => esc_html__( 'Font Size', 'rogan' ),
            'id'        => 'menu_btn_size',
            'type'      => 'spinner',
            'default'   => '14',
            'min'       => '12',
            'step'      => '1',
            'max'       => '50',
            'required'  => array ( 'is_menu_btn', '=', '1' )
        ),


        /**
         * Button colors
         * Style will apply on the Non sticky mode and sticky mode of the header
         */
        array (
            'title'     => esc_html__( 'Button Colors', 'rogan' ),
            'subtitle'  => esc_html__( 'Button style attributes on normal (non sticky) mode.', 'rogan' ),
            'id'        => 'button_colors',
            'type'      => 'section',
            'indent'    => true
        ),

        array (
            'title'     => esc_html__( 'Font color', 'rogan' ),
            'id'        => 'menu_btn_font_color',
            'type'      => 'color',
            'output'    => array ( '.header_area .navbar .btn_get, .navbar-collapse .menu_action_btn' ),
        ),
        
        array (
            'title'     => esc_html__( 'Border Color', 'rogan' ),
            'id'        => 'menu_btn_border_color',
            'type'      => 'color',
            'mode'      => 'border-color',
            'output'    => array ( '.header_area .navbar .btn_get, .navbar-collapse .menu_action_btn' ),
        ),
        
        array (
            'title'     => esc_html__( 'Background Color', 'rogan' ),
            'id'        => 'menu_btn_bg_color',
            'type'      => 'color',
            'mode'      => 'background',
            'output'    => array ( '.header_area .navbar .btn_get, .navbar-collapse .menu_action_btn' ),
        ),

        // Button color on hover stats
        array (
            'title'     => esc_html__( 'Hover Font Color', 'rogan' ),
            'subtitle'  => esc_html__( 'Font color on hover stats.', 'rogan' ),
            'id'        => 'menu_btn_hover_font_color',
            'type'      => 'color',
            'output'    => array ( '.header_area .navbar .btn_get:hover, .navbar-collapse .menu_action_btn:hover' ),
        ),
        array (
            'title'     => esc_html__( 'Hover Border Color', 'rogan' ),
            'id'        => 'menu_btn_hover_border_color',
            'type'      => 'color',
            'mode'      => 'border-color',
            'output'    => array ( '.header_area .navbar .btn_get:hover, .navbar-collapse .menu_action_btn:hover' ),
        ),
        array (
            'title'     => esc_html__( 'Hover background color', 'rogan' ),
            'subtitle'  => esc_html__( 'Background color on hover stats.', 'rogan' ),
            'id'        => 'menu_btn_hover_bg_color',
            'type'      => 'color',
            'output'    => array (
                'background' => '.header_area .navbar .btn_get:hover, .navbar-collapse .menu_action_btn:hover',
                'border-color' => '.navbar_fixed .header_area .navbar .btn_get:hover, .navbar-collapse .menu_action_btn:hover'
            ),
        ),
        array (
            'id'     => 'button_colors-end',
            'type'   => 'section',
            'indent' => false,
        ),

        /*
         * Button colors on sticky mode
         */
        array (
            'title'     => esc_html__( 'Sticky Button Style', 'rogan' ),
            'subtitle'  => esc_html__( 'Button colors on sticky mode.', 'rogan' ),
            'id'        => 'button_colors_sticky',
            'type'      => 'section',
            'indent'    => true
        ),
        array (
            'title'     => esc_html__( 'Border color', 'rogan' ),
            'id'        => 'menu_btn_border_color_sticky',
            'type'      => 'color',
            'mode'      => 'border-color',
            'output'    => array ( '.navbar_fixed.header_area .navbar .btn_get, .theme-main-menu.fixed .navbar-collapse .menu_action_btn' ),
        ),
        array (
            'title'     => esc_html__( 'Font color', 'rogan' ),
            'id'        => 'menu_btn_font_color_sticky',
            'type'      => 'color',
            'output'    => array ( '.navbar_fixed.header_area .navbar .btn_get, .theme-main-menu.fixed .navbar-collapse .menu_action_btn' ),
        ),
        array (
            'title'     => esc_html__( 'Background color', 'rogan' ),
            'id'        => 'menu_btn_bg_color_sticky',
            'type'      => 'color',
            'mode'      => 'background',
            'output'    => array ( '.navbar_fixed.header_area .navbar .btn_get, .theme-main-menu.fixed .navbar-collapse .menu_action_btn' ),
        ),

        // Button color on hover stats
        array (
            'title'     => esc_html__( 'Hover font color', 'rogan' ),
            'subtitle'  => esc_html__( 'Font color on hover stats.', 'rogan' ),
            'id'        => 'menu_btn_hover_font_color_sticky',
            'type'      => 'color',
            'output'    => array ( '.navbar_fixed .header_area .navbar .btn_get:hover, .theme-main-menu.fixed .navbar-collapse .menu_action_btn:hover' ),
        ),
        array (
            'title'     => esc_html__( 'Hover background color', 'rogan' ),
            'subtitle'  => esc_html__( 'Background color on hover stats.', 'rogan' ),
            'id'        => 'menu_btn_hover_bg_color_sticky',
            'type'      => 'color',
            'output'    => array (
                'background' => '.navbar_fixed .header_area .navbar .btn_get:hover, .theme-main-menu.fixed .navbar-collapse .menu_action_btn:hover',
            ),
        ),

        array (
            'id'     => 'button_colors-sticky-end',
            'type'   => 'section',
            'indent' => false,
        ),

        array (
            'title'     => esc_html__( 'Button padding', 'rogan' ),
            'subtitle'  => esc_html__( 'Padding around the menu action button.', 'rogan' ),
            'id'        => 'menu_btn_padding',
            'type'      => 'spacing',
            'output'    => array ( '.btn_get, .navbar-collapse .menu_action_btn' ),
            'mode'      => 'padding',
            'units'     => array ( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
            'units_extended' => 'true',
            'required'  => array ( 'is_menu_btn', '=', '1' )
        ),
    )
));


// Title-bar
Redux::setSection( 'rogan_opt', array (
    'title'            => esc_html__( 'Title-bar', 'rogan' ),
    'id'               => 'title_bar_opt',
    'subsection'       => true,
    'icon'             => '',
    'fields'           => array (
        
        array (
            'title'     => esc_html__( 'Background Image', 'rogan' ),
            'subtitle'  => esc_html__( 'Upload here the default background shape image', 'rogan' ),
            'id'        => 'banner_bg',
            'type'      => 'media',
            'compiler'  => true,
        ),
        
        array (
            'title'     => esc_html__( 'Background Color', 'rogan' ),
            'id'        => 'banner_bg_color',
            'type'      => 'color',
            'mode'          => 'background',
            'output'    => array ( '.solid-inner-banner' )
        ),
        
        array (
            'title'     => esc_html__( 'Title Color', 'rogan' ),
            'id'        => 'banner_title_color',
            'type'      => 'color',
            'output'    => array ( '.solid-inner-banner .page-title' )
        ),
        
        array (
            'id'        => 'titlebar_title_typo',
            'type'      => 'typography',
            'title'     => esc_html__( 'Title Typography', 'rogan' ),
            'output'    => '.solid-inner-banner .page-title'
        ),
        
        array (
            'title'     => esc_html__( 'Title-bar padding', 'rogan' ),
            'subtitle'  => esc_html__( 'The default padding around the Title-bar is 265px 0px 0px 0px. Apply the padding less than the default to make it smaller.', 'rogan' ),
            'id'        => 'title_bar_padding',
            'type'      => 'spacing',
            'output'    => array ( '.solid-inner-banner' ),
            'mode'      => 'padding',
            'units'     => array ( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
            'units_extended' => 'true',
        ),
        
        array (
            'id'       => 'titlebar_align',
            'type'     => 'button_set',
            'title'    => esc_html__( 'Alignment', 'rogan' ),
            //Must provide key => value pairs for options
            'options' => array (
                'left' => esc_html__( 'Left', 'rogan' ),
                'center' => esc_html__( 'Center', 'rogan' ),
                'right' => esc_html__( 'Right', 'rogan' )
            ),
            'default' => 'center'
        ),
        
    )
));