<div class="our-service service-minimal">
    <div class="container">
        <div class="demo-container-1100">
            <div class="row">
                <?php
                $i = 0;
                while ($service->have_posts()) : $service->the_post();
                    ++$i;
                    ?>
                    <div class="col-lg-6">
                        <div class="service-block aos-init aos-animate" data-aos="fade-up">
                            <div class="icon-box">
                                <?php the_post_thumbnail( 'full' ) ?>
                            </div>
                            <h2 class="service-title">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_title() ?>
                                </a>
                            </h2>
                            <?php the_excerpt() ?>
                            <a href="<?php the_permalink() ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                        </div> <!-- /.service-block -->
                    </div>
                    <?php
                endwhile;
                wp_reset_postdata();
                ?>
            </div> <!-- /.row -->
        </div>
    </div>
</div>