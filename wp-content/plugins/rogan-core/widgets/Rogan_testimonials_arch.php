<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_arch extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials_arch';
    }

    public function get_title() {
        return esc_html__( 'Testimonials Arch', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        //********************************* Testimonials List **********************************//
        $this->start_controls_section(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'author_name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'location',
            [
                'label' => esc_html__( 'Location', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'contents',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'testimonials_sliders',
            [
                'label' => esc_html__( 'Sliders Section', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ author_name }}}',
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Upper Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Main Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Upper Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Main Title Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //********************************* Background Shape Image ********************************//
        $this->start_controls_section(
            'bg_shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $testimonials = $settings['testimonials_sliders'];
        ?>
        <div class="arch-testimonial" style="background: url(<?php echo esc_url( $settings['bg_img']['url'] ) ?>);
            background-size: cover;">
            <div class="container">
                <div class="arch-client-slider">
                    <?php
                    if ( !empty( $testimonials )) {
                        foreach ( $testimonials as $testimonial ) {
                            ?>
                            <div class="item">
                                <div class="single-block">
                                    <?php if ( !empty( $testimonial['contents'] ) ) : ?>
                                        <p class="font-lato"><?php echo esc_html( $testimonial['contents'] ) ?></p>
                                    <?php endif; ?>
                                    <?php if ( !empty( $testimonial['author_name'] ) ) : ?>
                                        <h6 class="name"><?php echo esc_html( $testimonial['author_name'] ) ?></h6>
                                    <?php endif; ?>
                                    <?php if ( !empty( $testimonial['location'] ) ) : ?>
                                        <span class="font-lato"><?php echo esc_html( $testimonial['location'] ) ?></span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div> <!-- /.container -->
        </div>

        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready(function (){

                    var arclider = $ (".arch-client-slider");
                    if(arclider.length) {
                        arclider.owlCarousel({
                            loop:true,
                            nav:false,
                            dots:true,
                            autoplay:true,
                            margin:30,
                            autoplayTimeout:4500,
                            autoplaySpeed:1000,
                            lazyLoad:true,
                            singleItem:true,
                            responsive:{
                                0:{
                                    items:1
                                },
                                768:{
                                    items:2
                                },
                                1100:{
                                    items:3
                                }
                            }
                        });
                    }

                });
            }( jQuery ));
        </script>
        <?php
    }
}