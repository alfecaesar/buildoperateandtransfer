var $saveModalValueTo = '';

function matchStart(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
      return data;
    }
  
    // Skip if there is no 'children' property
    if (typeof data.children === 'undefined') {
      return null;
    }
  
    // `data.children` contains the actual options that we are matching against
    var filteredChildren = [];
    $.each(data.children, function (idx, child) {
      if (child.text.toUpperCase().indexOf(params.term.toUpperCase()) == 0) {
        filteredChildren.push(child);
      }
    });
  
    // If we matched any of the timezone group's children, then set the matched children on the group
    // and return the group object
    if (filteredChildren.length) {
      var modifiedData = $.extend({}, data, true);
      modifiedData.children = filteredChildren;
  
      // You can return modified objects from here
      // This includes matching the `children` how you want in nested data sets
      return modifiedData;
    }
  
    // Return `null` if the term should not be displayed
    return null;
}

function tabGo(i){
    if(i > 0){
      $('.content_col_box.active_tab').fadeOut(500).removeClass('active_tab');
      $('#tab_content_'+i).addClass('active_tab').fadeIn(500);

      $('#side-nav li a.active').removeClass('active');
      $('#tab_side_a_'+i).addClass('active');

      // prepare Create Project form summary
      if(i===6){
        prepCreateProjSum();
      }
    }
}

function deleteProjectURL(id){
  $('#modalbox_deleteProject .btn-confirm-y').attr('href','http://afarinsights.com/backend/user/delete_project.php?project_id='+id);
}

function prepop_speciality(i,savedModal){
    $saveModalValueTo = $('#'+savedModal);
    var optList = "";
    if(i === 1){
      getJson('http://afarinsights.com/backend/json/specialty_web_mobile_and_software_dev.json');
    }
    else if(i === 2){
      getJson('http://afarinsights.com/backend/json/specialty_data_science_and_analytics.json');
    }
    
}

function getJson(url){

  $.getJSON( url, function( data ) {
    var items = [];
    $.each( data.value, function( key, val ) {
      items.push( '<option value="' + val + '">' + val + '</option>' );
    });
   
    //console.log(items.join(''))
    $('#select_specialty').empty();
    $('#select_specialty').html(items.join(''));

    if(document.location.pathname === '/backend/user/edit_project.php'){
      prePopSelect2('select_specialty',category_specialty)
    }

  });
}

function saveSpecialty($sd){
  //console.log($sd.val().join(', '))
  $saveModalValueTo.html($sd.val().join(', '));
  $('#category_specialty').val($sd.val().join(', '));
}

function clbtnVal(el,inpName){
  $(el).closest('.btn_val_c').find('button.active').removeClass('active');
  $(el).addClass('active');
  $('#'+inpName).val($(el).text());
}

function createProjectSubmit(frm){
  frm.submit();
}

function prePopCheckRadio(name,val){
  $('input[name='+name+']').each(function(){
      if($(this).val() === val){
          $(this).prop('checked',true);
      }
  });
}

function prePopSelect2(name,val){
  var arr_val = val.split(', ');
  $('select[name='+name+']').val(arr_val).trigger('change');
}

function prePopButtonText($elem,val){
  $elem.find('button').each(function(){
    if($(this).text() === val){
      $(this).addClass('active');
    }
  });
}

$(document).ready(function(){

    // Modal #modalbox_specialty_btn -> #modalbox_notwhatyoulookingfor
    $(document).on('click', '.modalbox_notwhatyoulookingfor_btn', function(){
        $('#modalbox_specialty').modal('toggle');
        return false;
    });
    
    // Modal #modalbox_notwhatyoulookingfor -> #modalbox_specialty_btn
    $(document).on('click', '.modalbox_specialty_btn', function(){
        $('#modalbox_notwhatyoulookingfor').modal('toggle');
        return false;
    });


    $("#select_specialty, .performance_select select").select2({
        matcher: matchStart
    });

    $( "#select_specialty, .performance_select select" ).on( "select2:open", function() {
      if ( $( this ).parents( "[class*='has-']" ).length ) {
        var classNames = $( this ).parents( "[class*='has-']" )[ 0 ].className.split( /\s+/ );

        for ( var i = 0; i < classNames.length; ++i ) {
          if ( classNames[ i ].match( "has-" ) ) {
            $( "body > .select2-container" ).addClass( classNames[ i ] );
          }
        } 
      }
    });

    $('.input-group.date').datepicker({
      format: 'dd-M-yyyy'
    });

});