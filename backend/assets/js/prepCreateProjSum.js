function prepCreateProjSum(){
    $('#cp_sum_1').text($('input[name=project_name]').val());
    $('#cp_sum_2').text($('input[name=category]').val());
    $('#cp_sum_3').text($('select[name=select_specialty]').val());

    if(document.location.pathname === '/backend/user/edit_project.php'){
        if(!$('select[name=select_specialty]').val()[0]){
            $('#cp_sum_3').text(category_specialty);
        }        
    }

    $('#cp_sum_4').text($('textarea[name=description]').val());
    $('#cp_sum_5').text($('input[name=ideal_minimum_viable_product]').val()+ ' '+$('input[name=ideal_minimum_viable_product_others]').val());
    $('#cp_sum_6').text($('input[name=additional_project_files]').val());

    $('#cp_sum_7').text($('select[name=target_markets]').val());
    $('#cp_sum_8').text($('select[name=increased_social_media_awareness]').val());
    $('#cp_sum_9').text($('select[name=lower_acquisition_costs_by]').val());
    $('#cp_sum_10').text($('select[name=locate_product_faster]').val());
    $('#cp_sum_11').text($('select[name=compare_shopping_on]').val());
    $('#cp_sum_12').text($('select[name=enable_deals_bargains_coupons_and_group_buys]').val());
    $('#cp_sum_13').text($('select[name=create_market_for_products_to_achieve]').val());
    $('#cp_sum_14').text($('select[name=additional_performances_important]').val());
    $('#cp_sum_15').text($('input[name=performance_not_what_you_looking_for]').val());

    $('#cp_sum_16').text($('select[name=ecommerce_development_deliverables]').val());
    $('#cp_sum_17').text($('select[name=ecommerce_platforms]').val());
    $('#cp_sum_18').text($('select[name=ecommerce_development_skills]').val());
    $('#cp_sum_19').text($('select[name=ecommerce_payment_systems]').val());
    $('#cp_sum_20').text($('select[name=ecommerce_database]').val());
    $('#cp_sum_21').text($('select[name=business_size_experience]').val());
    $('#cp_sum_22').text($('select[name=additional_features_are_important_to_you]').val());
    $('#cp_sum_23').text($('input[name=features_not_what_you_looking_for]').val());

    $('#cp_sum_24').text($('input[name=specific_budget]').val());
    $('#cp_sum_25').text($('input[name=specific_timeframe_to_complete_min]').val()+' - '+$('input[name=specific_timeframe_to_complete_max]').val());
    $('#cp_sum_26').text($('input[name=how_long_prepared_to_work_with_min]').val()+' - '+$('input[name=how_long_prepared_to_work_with_max]').val());
    $('#cp_sum_27').text($('input[name=how_would_you_like_to_work_with]').val());
    $('#cp_sum_28').text($('input[name=when_would_you_like_to_pay]').val());
}