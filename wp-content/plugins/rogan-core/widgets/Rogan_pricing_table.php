<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_pricing_table extends Widget_Base {

    public function get_name() {
        return 'rogan-pricing-table';
    }

    public function get_title() {
        return esc_html__( 'Pricing Table', 'rogan-hero' );
    }

    public function get_icon() {
        return ' eicon-price-table';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {


        //*****************************  Title Section  *********************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'No Hidden Charges! Choose <br> your Plan.',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //************************************** Title Icon Box ************************************//
        $this->start_controls_section(
            'title_icon_box', [
                'label' => esc_html__( 'Title Icon Box', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'shape_img', [
                'label' => esc_html__( 'Background Shape Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape4.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'icon_img', [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/icon/icon26.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        // *********************************** Section Pricing Table Color ******************************//
        $this->start_controls_section(
            'section style', [
                'label' => esc_html__( 'Pricing Table Style', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'table_title_color',
            [
                'label' => esc_html__( 'Main Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sass-our-pricing .single-pr-table .pr-header .title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'feature_title_color',
            [
                'label' => esc_html__( 'Feature Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sass-our-pricing .single-pr-table .pr-body .feature' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'table_title_typography',
                'label' => esc_html__( 'Main Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .sass-our-pricing .single-pr-table .pr-header .title',
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'feature_title_typography',
                'label' => esc_html__( 'Feature Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .sass-our-pricing .single-pr-table .pr-body .feature',
            ]
        );

        $this->end_controls_section();

        //************************************* Background Shape ***************************************//
        $this->start_controls_section(
            'shape_image', [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'shape1', [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => plugins_url('images/shape/shape-18.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => plugins_url('images/shape/shape-24.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape3', [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => plugins_url('images/shape/shape-25.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape4', [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => plugins_url('images/shape/shape-26.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'shape5', [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => plugins_url('images/shape/shape-27.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        // ********************************* Pricing Table tabs ***********************************//
        $this->start_controls_section(
            'pricing_table_sec',
            [
                'label' => esc_html__( 'Pricing Table', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'table_title', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'price', [
                'label' => esc_html__( 'Price', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'price_color',
            [
                'label' => esc_html__( 'Price Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}} .single-pr-table.regular .pr-header .price' => 'color: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'package', [
                'label' => esc_html__( 'Package', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'feature_title', [
                'label' => esc_html__( 'Feature Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'feature_contents', [
                'label' => esc_html__( 'Feature Contents', 'rogan-core' ),
                'type' =>Controls_Manager::WYSIWYG,
            ]
        );

        $repeater->add_control(
            'btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Upgrade',
            ]
        );

        $repeater->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );

        $repeater->add_control(
            'trial_btn_label',
            [
                'label' => esc_html__( 'Trial Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Get your 30 day free trial',
            ]
        );

        $repeater->add_control(
            'trial_btn_url',
            [
                'label' => esc_html__( 'Trial Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );


        $repeater->end_controls_tab();

        $this->add_control(
            'pricing_tables',
            [
                'label' => esc_html__( 'Pricing Tables', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{table_title}}}'
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .sass-our-pricing' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        $tables = $settings['pricing_tables'];
        ?>
        <div class="sass-our-pricing">
            <?php if (!empty($settings['shape1']['url']))  : ?>
                <div class="section-shape-one"><img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>"></div>
            <?php endif; ?>
            <?php if (!empty($settings['shape2']['url']))  : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="section-shape-two" data-aos="fade-right" data-aos-duration="3000">
            <?php endif; ?>
            <?php if (!empty($settings['shape3']['url']))  : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="section-shape-three">
            <?php endif; ?>
                <div class="section-shape-four"></div>
            <?php if (!empty($settings['shape4']['url']))  : ?>
                <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="section-shape-five">
            <?php endif; ?>
            <?php if (!empty($settings['shape5']['url']))  : ?>
                <img src="<?php echo esc_url($settings['shape5']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="section-shape-six" data-aos="fade-left" data-aos-duration="3000">
            <?php endif; ?>

            <div class="container">
                <div class="theme-title-one text-center hide-pr">
                    <div class="icon-box hide-pr">
                        <?php if (!empty($settings['shape_img']['url']))  : ?>
                            <img src="<?php echo esc_url($settings['shape_img']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="bg-shape">
                        <?php endif; ?>
                        <?php if (!empty($settings['icon_img']['url']))  : ?>
                            <img src="<?php echo esc_url($settings['icon_img']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="icon">
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($settings['title_text'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post($settings['title_text']) ?></h2>
                    <?php endif; ?>
                </div>
                <div class="row no-gutters">
                    <?php
                    if (!empty($tables)) {
                        $i = 0;
                        foreach ($tables as $table) {
                            ?>
                            <div class="col-lg-4 elementor-repeater-item-<?php echo $table['_id'] ?>" data-aos="fade-up"
                                <?php if($i != 0) : ?> data-aos-delay="<?php echo $i; ?>" <?php endif; ?>">
                                <div class="single-pr-table regular">
                                    <div class="pr-header">
                                        <?php if (!empty($table['price'])) : ?>
                                            <div class="price"> <?php echo esc_html($table['price']) ?> </div>
                                        <?php endif; ?>
                                        <?php if (!empty($table['table_title'])) : ?>
                                            <h4 class="title"><?php echo esc_html($table['table_title']) ?></h4>
                                        <?php endif; ?>
                                        <?php if (!empty($table['package'])) : ?>
                                            <div class="package"><?php echo esc_html($table['package']) ?></div>
                                        <?php endif; ?>
                                    </div> <!-- /.pr-header -->
                                    <div class="pr-body">

                                        <?php if (!empty($table['feature_title'])) : ?>
                                            <h3 class="feature"><?php echo esc_html($table['feature_title']) ?></h3>
                                        <?php endif; ?>

                                        <?php if (!empty($table['feature_contents'])) : ?>
                                            <?php echo wp_kses_post($table['feature_contents']) ?>
                                        <?php endif; ?>

                                    </div> <!-- /.pr-body -->
                                    <div class="pr-footer">
                                        <?php if (!empty($table['btn_label'])) : ?>
                                            <a href="<?php echo esc_url($table['btn_url']['url']) ?>"
                                                <?php rogan_is_external($table['btn_url']); rogan_is_nofollow($table['btn_url']) ?> class="upgrade-button">
                                                <?php echo esc_html($table['btn_label']) ?>
                                            </a>
                                        <?php endif; ?>
                                        <?php if (!empty($table['trial_btn_label'])) : ?>
                                            <a href="<?php echo esc_url($table['trial_btn_url']['url']) ?>"
                                                <?php rogan_is_external($table['trial_btn_url']); rogan_is_nofollow($table['trial_btn_url']) ?> class="trial-button">
                                                <?php echo esc_html($table['trial_btn_label']) ?>
                                            </a>
                                        <?php endif; ?>
                                    </div> <!-- /.pr-footer -->
                                </div> <!-- /.single-pr-table -->
                            </div>
                            <?php
                            $i = $i + 200;
                        }
                    }
                    ?>
            </div>
        </div>
        <?php
    }
}