<?php
add_shortcode('roganmm_container', function($atts, $content) {
    ob_start();
    ?>

    <div class="clearfix">
        <div class="row">
            <?php echo do_shortcode($content) ?>
        </div>
    </div>

    <?php
    $html = ob_get_clean();
    return $html;
});