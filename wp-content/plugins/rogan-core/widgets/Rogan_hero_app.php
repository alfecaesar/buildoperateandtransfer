<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_app extends Widget_Base {

    public function get_name() {
        return 'rogan_hero_app';
    }

    public function get_title() {
        return esc_html__( 'Hero Mobile App', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Title Section  ------------------------------------//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Ultimate web app <br>for your customer <br> support.',
            ]
        );

        $this->end_controls_section();

        // ----------------------------------------  Hero content ------------------------------
        $this->start_controls_section(
            'subtitle_sec',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
            ]
        );
        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();

        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'fimg1_sec',
            [
                'label' => esc_html__( 'First Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimg1', [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen7.png', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position1',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-one' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position1',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-one' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        //*****************************  Second Featured image *******************************************//
        $this->start_controls_section(
            'fimg2_sec',
            [
                'label' => esc_html__( 'Second Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimg2', [
                'label' => esc_html__( 'Upload the featured mage', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen8.png', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position2',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-two' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position2',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-two' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        //*****************************  Third Featured image *******************************************//
        $this->start_controls_section(
            'fimg3_sec',
            [
                'label' => esc_html__( 'Third Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimg3', [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/home/screen9.png', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position3',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-two .search-box' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position3',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .mobile-screen-two .search-box' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();



        /// ---------------------------------------   Buttons ----------------------------------///
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Buttons', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'play_btn_label', [
                'label' => esc_html__( 'Top Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => 'Google Play'
            ]
        );

        $repeater->add_control(
            'play_btn', [
                'label' => esc_html__( 'Bottom Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => 'Get it on'
            ]
        );

        $repeater->add_control(
            'is_video_btn', [
                'label' => esc_html__( 'Video Button', 'rogan-core' ),
                'description' => esc_html__( 'If you switch to "yes", you can use YouTube video URL in the URL field.', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $repeater->add_control(
            'play_btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'play_store_img', [
                'label' => esc_html__( 'Button Logo', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/playstore.svg', __FILE__)
                ]
            ]
        );


        $repeater->add_control(
            'font_color', [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .main-wrapper .button-group li {{CURRENT_ITEM}}' => 'color: {{VALUE}}',
                ],

            ]
        );

        $repeater->add_control(
            'border_color',
            [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .main-wrapper .button-group li {{CURRENT_ITEM}}' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        // Buttons repeater field
        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ play_btn_label }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();



        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_prefix', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_prefix',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-five .main-title',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Subtitle ------------------------------
        $this->start_controls_section(
            'style_subtitle', [
                'label' => esc_html__( 'Style Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_subtitle', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .sub-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-five .sub-title',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Background Round Color ------------------------------
        $this->start_controls_section(
            'round_wave_sec', [
                'label' => esc_html__( 'Background Round Color', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_one', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .bg-shape-holder .big-round-one' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'color_two', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .bg-shape-holder .big-round-two' => 'background: {{VALUE}};',
                ],
            ]
        );


        $this->add_control(
            'color_three', [
                'label' => esc_html__( 'Color Three', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .bg-shape-holder .big-round-three' => 'background: {{VALUE}};',
                ],
            ]
        );


        $this->add_control(
            'snow_dot_color', [
                'label' => esc_html__( 'Snow Dots Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five .bg-shape-holder .shape-one' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


        //------------------------------ Gradient Color ------------------------------
        $this->start_controls_section(
            'bg_gradient_sec', [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-five' => 'background: linear-gradient( 145deg, {{bg_color.VALUE}} 0%, {{VALUE}} 100%);',
                ],
            ]
        );

        $this->end_controls_section();

        //------------------------------ Agency Style Section ------------------------------
        $this->start_controls_section(
            'bg_shape_sec', [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_shape',
            [
                'label' => esc_html__( 'Shape', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-32.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        $buttons = $settings['buttons']
        ?>
        <div id="theme-banner-five">
            <?php if(!empty($settings['fimg1']['url'])) : ?>
                <div class="mobile-screen-one wow fadeInRight animated" data-wow-duration="1.5s">
                    <img src="<?php echo esc_url($settings['fimg1']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>">
                </div>
            <?php endif; ?>
            <div class="mobile-screen-two wow fadeInRight animated" data-wow-duration="1.5s" data-wow-delay="0.8s">
                <?php if (!empty($settings['fimg2']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['fimg2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>">
                <?php endif; ?>
                <?php if (!empty($settings['fimg3']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['fimg3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="search-box wow zoomIn animated" data-wow-duration="1.5s" data-wow-delay="1.8s">
                <?php endif; ?>
            </div>
            <div class="bg-shape-holder">
                <span class="big-round-one wow fadeInLeft animated" data-wow-duration="3s"></span>
                <span class="big-round-two wow fadeInLeft animated" data-wow-duration="3s"></span>
                <span class="big-round-three wow fadeInLeft animated" data-wow-duration="3s"></span>
                <span class="shape-one"></span>
                <span class="shape-two"></span>
                <?php if (!empty($settings['bg_shape']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['bg_shape']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title_text'])) ?>" class="shape-three">
                <?php endif; ?>
                <span class="shape-four"></span>
            </div>
            <div class="main-wrapper">
                <?php if (!empty($settings['title_text'])) : ?>
                    <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s"><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></h1>
                <?php endif;
                if (!empty($settings['subtitle'])) : ?>
                    <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s"><?php echo wp_kses_post(nl2br($settings['subtitle'])) ?></p>
                <?php endif; ?>
                <ul class="button-group">

                    <?php
                    if (!empty($buttons)) {
                    foreach ($buttons as $button) {
                        $is_video_btn = '';
                        if ( $button['is_video_btn'] == 'yes' ) {
                            wp_enqueue_style('magnific-popup');
                            wp_enqueue_script('magnific-popup');
                            $is_video_btn = $button['is_video_btn'] ? 'video_btn popup-youtube' : '';
                        }

                        if ( !empty($button['play_btn_label']) ) : ?>
                            <li>
                                <a href="<?php echo esc_url($button['play_btn_url']['url']) ?>"
                                    <?php rogan_is_exno($button['play_btn_url']); ?> class="<?php echo $is_video_btn ?> google-button wow fadeInLeft animated elementor-repeater-item-<?php echo esc_attr($button['_id']) ?>" data-wow-delay="1.5s">
                                    <?php if ( !empty($button['play_store_img']['url']) ) : ?>
                                        <img src="<?php echo esc_url($button['play_store_img']['url']) ?>" alt="<?php echo esc_attr($button['play_btn_label']) ?>">
                                    <?php endif; ?>
                                    <span><?php echo esc_html($button['play_btn_label']) ?></span><?php echo esc_html($button['play_btn']) ?>
                                </a>
                            </li>
                        <?php endif;
                    }}
                    ?>
                </ul>
            </div> <!-- /.main-wrapper -->
        </div>
        <?php
    }
}
