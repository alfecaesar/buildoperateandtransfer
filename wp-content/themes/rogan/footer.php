<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rogan
 */
$opt = get_option( 'rogan_opt' );
$copyright_text = !empty($opt['copyright_txt']) ? $opt['copyright_txt'] : esc_html__( '© 2019 CreativeGigs. All rights reserved', 'rogan' );
$right_content = !empty($opt['right_content']) ? $opt['right_content'] : '';
$footer_visibility = function_exists( 'get_field' ) ? get_field( 'footer_visibility' ) : '1';
$footer_visibility = isset($footer_visibility) ? $footer_visibility : '1';
?>
    <?php
    if( $footer_visibility == '1' ) :
        if( !is_404() ) :
            ?>
            <footer class="theme-footer-one top-border" id="footer">
                <div class="shape-one" data-aos="zoom-in-right"></div>
                <img src="<?php echo esc_url(ROGAN_DIR_IMG) ?>/shape/shape-67.svg" alt="<?php esc_attr_e( 'Rotataed triangle shape', 'rogan' ) ?>" class="shape-two">
                <?php if ( is_active_sidebar( 'footer_widgets' ) ) : ?>
                    <div class="top-footer">
                        <div class="container">
                            <div class="row">
                                <?php dynamic_sidebar( 'footer_widgets' ) ?>
                            </div> <!-- /.row -->
                        </div> <!-- /.container -->
                    </div> <!-- /.top-footer -->
                <?php endif; ?>

                <div class="container">
                    <div class="bottom-footer">
                        <div class="clearfix">
                            <?php echo wp_kses_post(wpautop($copyright_text)); ?>
                            <?php echo wp_kses_post(wpautop($right_content)) ?>
                        </div>
                    </div> <!-- /.bottom-footer -->
                </div>
            </footer>
        <?php endif; ?>
    <?php endif; ?>

    </div> <!-- /.main-page-wrapper -->

<?php wp_footer(); ?>
</body>
</html>