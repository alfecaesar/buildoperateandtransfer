<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Rogan_navwalker_side_menu;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_op_quoteform extends Widget_Base {

    public function get_name() {
        return 'rogan_op_banner';
    }

    public function get_title() {
        return __( 'Quote Form <br> with Hamburger Menu', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-columns';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // Logo
        $this->start_controls_section(
            'logo_sec',
            [
                'label' => esc_html__( 'Logo', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'logo', [
                'label' => esc_html__( 'Logo', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => array (
                    'url' => plugins_url('images/logo13.svg', __FILE__)
                )
            ]
        );

        $this->end_controls_section();

        // Banner Title
        $this->start_controls_section(
            'op_banner',
            [
                'label' => esc_html__( 'Banner Content', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'We’r highly <br> <span>Expert & Idea</span> <br>Builder.',
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__( 'Title Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-banner-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'title_highlight_color',
            [
                'label' => esc_html__( 'Highlighted Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .op-banner-one .main-title span:before' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'heading_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-banner-one .main-title',
            ]
        );

        $this->end_controls_section();


        //***********************************  Designer Title ******************************************//
        $this->start_controls_section(
            'left_text_sec',
            [
                'label' => esc_html__( 'Left Text', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'left_text', [
                'label' => esc_html__( 'Left Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => '<span>Design with love</span> <br>by Rashed',
            ]
        );

        $this->add_control(
            'left_text_color',
            [
                'label' => esc_html__( 'Name Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-banner-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'left_text_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-banner-one .main-title',
            ]
        );

        $this->end_controls_section();


        //*********************************** Form Shortcode  ******************************************//
        $this->start_controls_section(
            'op_banner_form',
            [
                'label' => esc_html__( 'Form', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'form_shortcode', [
                'label' => esc_html__( 'Form Shortcode', 'rogan-core' ),
                'description' => esc_html__( 'Paste here the Contact Form 7 shortcode', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();

        //***********************************  social link  ******************************************//
        $this->start_controls_section(
            'social_links',
            [
                'label' => esc_html__( 'Social Profile URLs', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'facebook', [
                'label' => esc_html__( 'Facebook URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'twitter', [
                'label' => esc_html__( 'Twitter URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'instagram', [
                'label' => esc_html__( 'Instagram URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'youtube', [
                'label' => esc_html__( 'Youtube URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->add_control(
            'linkedin', [
                'label' => esc_html__( 'Linkedin URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .eCommerce-side-menu' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        ?>

        <div id="sidebar-menu" class="eCommerce-side-menu op-page-menu-one">
            <div class="inner-wrapper">
                <div class="logo-wrapper">
                    <button class="close-button">
                        <img src="<?php echo plugins_url('images/icon/icon43.svg', __FILE__) ?>" alt="">
                    </button>
                    <img src="<?php echo esc_url($settings['logo']['url']) ?>" alt="<?php bloginfo('name'); ?>">
                </div>
                <?php
                if( has_nav_menu( 'main_menu' ) && class_exists('Rogan_navwalker_side_menu') ) {
                    wp_nav_menu( array (
                        'menu' => 'main_menu',
                        'theme_location' => 'main_menu',
                        'container_class' => 'main-menu-list',
                        'walker' => new Rogan_navwalker_side_menu(),
                        'depth' => 2
                    ));
                }
                ?>
                <?php if ( !empty($opt['header6_content']) ) : ?>
                    <p class="copy-right"> <?php echo wp_kses_post($opt['header6_content']) ?> </p>
                <?php endif; ?>
            </div> <!-- /.inner-wrapper -->
        </div> <!-- #sidebar-menu -->


        <!--
        =============================================
            Theme Header
        ==============================================
        -->
        <?php if ( !empty($settings['logo']['url'])) : ?>
            <div class="one-page-header-one">
                <div class="logo">
                    <a href="<?php echo home_url('/') ?>">
                        <img src="<?php echo esc_url($settings['logo']['url']) ?>" alt="<?php bloginfo('name'); ?>">
                    </a>
                </div>
            </div> <!-- /.one-page-header-one -->
        <?php endif; ?>

        <div class="op-banner-one">
            <div class="op-menu-one">
                <ul class="social-icon">
                    <?php if(!empty($settings['f_link'])) : ?>
                        <li><a href="<?php echo esc_url($settings['f_link']); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($settings['t_link'])) : ?>
                        <li><a href="<?php echo esc_url($settings['t_link']); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                    <?php if(!empty($settings['l_link'])) : ?>
                        <li><a href="<?php echo esc_url($settings['l_link']); ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                    <?php endif; ?>
                </ul>
                <button class="op-menu-button sidebar-menu-open">
                    <img src="<?php echo plugins_url('images/icon/icon68.svg', __FILE__) ?>" alt="<?php bloginfo('name'); ?>">
                </button>
            </div>

            <div class="d-lg-flex ">
                <?php if ( !empty($settings['title_text']) ) : ?>
                    <h2 class="font-k2d main-title">
                        <?php echo wp_kses_post(nl2br($settings['title_text'])); ?>
                    </h2>
                <?php endif; ?>

                <?php if ( !empty($settings['form_shortcode']) ) : ?>
                    <div class="banner-form">
                        <?php echo do_shortcode($settings['form_shortcode']); ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php if ( !empty($settings['left_text']) ) : ?>
                <div class="credit-tag">
                    <p class="font-k2d"><?php echo htmlspecialchars_decode( $settings['left_text'] ); ?></p>
                </div> <!-- /.credit-tag -->
            <?php endif; ?>
        </div>
        <?php
    }
}