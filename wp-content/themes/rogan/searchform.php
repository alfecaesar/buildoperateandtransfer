<?php

add_filter( 'get_search_form', function($form) {
    $value = get_search_query() ? get_search_query() : '';
    $form = '<form role="search" method="get" id="searchform" class="sidebar-search" action="' . home_url( '/' ) . '">
                <input type="text" placeholder="'.esc_attr__( 'Search', 'rogan' ).'" value="' . esc_attr($value) . '" name="s" id="s">
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>';
    return $form;
});