<?php include '../bot/template/header.php';


include '../dbconfig.php';

?>

<?php include '../bot/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../bot/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>BOT Opportunities</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <?php 
                    $request_sent = (!empty($_GET['request_sent'])) ? $_GET['request_sent'] : '';
                        if ($request_sent === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            Request Successfully Sent! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <div class="project-box row project-box-head">
                        <div class="project-box-name col col-4 text-left">Title</div>
                        <div class="project-box-status col col-2 text-left">Budget</div>
                        <div class="project-box-date col col-3 text-left">Start Date - End Date</div>
                        <div class="project-box-action col col-3 text-right">Action</div>
                    </div>

                    <?php

                        $datas = $database->select("project", [
                            "project_id",
                            "project_name",
                            "specific_budget",
                            "specific_timeframe_to_complete_min",
                            "specific_timeframe_to_complete_max"
                        ]);

                        foreach($datas as $data)
                        {
                            $request_status = $database->select("bot_project_request", "request_status", [
                                "AND" => [ 
                                    "project_id" => $data['project_id'],
                                    "user_id" => $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ])
                                ]
                            ]);

                            if($request_status){
                                if($request_status[0] === 'Approved'){
                                    $taskLink = ' | <a href="task.php?project_id='. $data['project_id'] .'" class="action-link-view"><i class="fas fa-eye"></i> View Task</a>';
                                    $classStat = 'approved2';
                                }
                                else{
                                    $taskLink = '';
                                    $classStat = 'pending';
                                }
                                echo '<div class="project-box row '.$classStat.'">' .
                                    '<div class="project-box-name col col-4">'. $data['project_name'] .'</div>' .
                                    '<div class="project-box-status col col-2 text-left">'. $data['specific_budget'] .'</div>' .
                                    '<div class="project-box-date col col-3 text-left">'. $data['specific_timeframe_to_complete_min'] .' - '. $data['specific_timeframe_to_complete_max'] .'</div>' .
                                    '<div class="project-box-action col col-3 text-right">' . 
                                        '<a href="javascript:void(0)" class="action-link-view"> '. $request_status[0] .'</a>' . $taskLink .
                                    '</div>' . 
                                 '</div>';
                            }
                            else{
                                echo '<div class="project-box row">' .
                                    '<div class="project-box-name col col-4">'. $data['project_name'] .'</div>' .
                                    '<div class="project-box-status col col-2 text-left">'. $data['specific_budget'] .'</div>' .
                                    '<div class="project-box-date col col-3 text-left">'. $data['specific_timeframe_to_complete_min'] .' - '. $data['specific_timeframe_to_complete_max'] .'</div>' .
                                    '<div class="project-box-action col col-3 text-right">' . 
                                        '<a href="bot_project_request.php?project_id='. $data['project_id'] .'" class="action-link-view"><i class="fas fa-check"></i> Request Sign Up</a>' . 
                                    '</div>' . 
                                 '</div>';
                            }           
                        }
                    ?>

                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>


<?php include '../bot/template/footer.php';?>