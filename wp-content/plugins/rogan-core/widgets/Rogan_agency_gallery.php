<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_agency_gallery extends Widget_Base {

    public function get_name() {
        return 'Rogan_agency_gallery';
    }

    public function get_title() {
        return esc_html__( 'Gallery', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return [ 'owl-carousel', 'fancybox' ];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate', 'fancybox'];
    }


    protected function _register_controls() {

        // ***************************** Gallery Title **************************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Check some of our <br> Recent work.',
                'label_block' => true
            ]
        );

        $this->end_controls_section();

        // ***************************** Subtitle **************************************//
        $this->start_controls_section(
            'subtitle_sec', [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();


        // *************************************** Button Section *************************************//
        $this->start_controls_section(
            'button_section',
            [
                'label' => esc_html__( 'Button', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'View Gallery'
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' =>Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'style_tabs'
        );


        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery:hover' => 'color: {{VALUE}}',
                ]
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery:hover' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery .main-wrapper .view-gallery:hover' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();

        //************************************** Gallery Section ***********************************************//
        $this->start_controls_section(
            'gallery_sec',
            [
                'label' => esc_html__( 'Gallery', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'gallery',
            [
                'label' => esc_html__( 'Add Image', 'rogan-core' ),
                'type' => Controls_Manager::GALLERY,
            ]
        );

        $this->add_control(
            'gallery_text',
            [
                'label' => esc_html__( 'Hover Icon', 'rogan-core' ),
                'description' => esc_html__( 'Gallery Hover Icon Text', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '+'
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * @Title
         */
        /********************************* Title Style ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        /********************************* Subtitle Style ********************************/
        $this->start_controls_section(
            'style_sec_subtitle', [
                'label' => esc_html__( 'Section Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_subtitle', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .bottom-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .bottom-title',
            ]
        );

        $this->end_controls_section();

        // ************************************** Background shapes  *************************************//
        $this->start_controls_section(
            'shape_images_sec',
            [
                'label' => esc_html__( 'Shape Images', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-62.svg', __FILE__)
                ]
            ]
        );
        $this->add_control(
            'shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-55.svg', __FILE__)
                ]
            ]
        );
        $this->add_control(
            'shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-61.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-our-gallery' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        ?>
        <div class="agn-our-gallery">
            <style>
                .agn-our-gallery:before {
                    content: url(<?php echo esc_url($settings['bg_img']['url']) ?>);
                }
            </style>
            <?php if (!empty($settings['shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title']))?>" class="shape-one">
            <?php endif; ?>
            <?php if (!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title']))?>" class="shape-two">
            <?php endif; ?>
            <?php if (!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title']))?>" class="shape-three">
            <?php endif; ?>
            <div class="container">
                <div class="theme-title-one">
                    <?php if (!empty($settings['title'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['title'])) ?></h2>
                    <?php endif; ?>
                    <?php if (!empty($settings['subtitle'])) : ?>
                        <p class="bottom-title"><?php echo esc_html($settings['subtitle']) ?></p>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
            </div> <!-- /.container -->
            <div class="main-wrapper">
                <?php if (!empty($settings['btn_label'])) : ?>
                    <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="view-gallery"
                        <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                        <?php echo esc_html($settings['btn_label']) ?>
                    </a>
                <?php endif; ?>
                <div class="gallery-slider lightgallery">
                    <?php
                    if (!empty($settings['gallery'])) {
                    foreach ($settings['gallery'] as $image) {
                        ?>
                        <div class="item">
                            <div class="img-box">
                                <?php if (!empty($image['url'])) : ?>
                                    <img src="<?php echo esc_url($image['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title']))?>">
                                <?php endif; ?>
                                <div class="hover-content">
                                    <a href="<?php echo esc_url($image['url']) ?>" class="icon zoom fancybox" data-fancybox="images" data-caption="My caption">
                                        <?php echo esc_html($settings['gallery_text']) ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php
                    }}
                    ?>
                </div> <!-- /.gallery-slider -->
            </div> <!-- /.main-wrapper -->
        </div>
        <?php
    }
}