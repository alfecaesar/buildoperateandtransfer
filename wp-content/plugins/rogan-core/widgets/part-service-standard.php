<div class="our-service service-standard">
    <div class="container">
        <div class="clearfix">
            <div class="row">
                <?php
                $i = 0;
                while ($service->have_posts()) : $service->the_post();
                    ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="service-block" data-aos="fade-up" style="background-image: url(<?php the_post_thumbnail_url('rogan_370x474') ?>);">
                            <div class="hover-content">
                                <h6 class="title">
                                    <a href="<?php the_permalink() ?>">
                                        <?php the_title() ?>
                                    </a>
                                </h6>
                                <?php the_excerpt() ?>
                                <a href="<?php the_permalink() ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                            </div> <!-- /.hover-content -->
                        </div> <!-- /.service-block -->
                    </div> <!-- /.col- -->
                    <?php
                    ++$i;
                endwhile;
                ?>
            </div> <!-- /.row -->
        </div> <!-- /.clearfix -->
    </div> <!-- /.container -->
</div>