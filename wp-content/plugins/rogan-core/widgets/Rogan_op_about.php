<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_op_about extends Widget_Base {

    public function get_name() {
        return 'rogan_op_about';
    }

    public function get_title() {
        return esc_html__( 'About (Portfolio Studio)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-image-before-after';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        //********************** About Top Section  *****************//
        $this->start_controls_section(
            'about_section', [
                'label' => esc_html__( 'About Section Top', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'section_heading', [
                'label' => esc_html__( 'Section Heading', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Build idea for <span>your goal</span>.'
            ]
        );

        $this->add_control(
            'color_heading_title',
            [
                'label' => esc_html__( 'Section Heading Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .theme-title-four' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_heading_title',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-four',
            ]
        );


        $this->add_control(
            'topper_content', [
                'label' => esc_html__( 'Top Right Content', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Npem ipum dolor si amet coming onsecte dipisc ing elit, eiusd lorem  teu mpor incididunt labore et dolore aliqua. Ut enim ader mini veniam, loremullam laboris nisi ut  3.5 yers.'
            ]
        );


        $this->add_control(
            'color_topper_content',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-about-us-one .top-text p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_topper_content',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-about-us-one .top-text p',
            ]
        );


        $this->end_controls_section();


        //********************** About Image  *****************//
        $this->start_controls_section(
            'about_image_box', [
                'label' => esc_html__( 'About Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'about_img',
            [
                'label' => esc_html__( 'Image Upload', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'img_box_text', [
                'label' => esc_html__( 'Image Box Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Our Office & Creative Team'
            ]
        );


        $this->end_controls_section();
        
        //********************** About Info  *****************//

        $this->start_controls_section(
            'about_us_info', [
                'label' => esc_html__( 'About Informations', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'about_us_title', [
                'label' => esc_html__( 'About us', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'About us.',
            ]
        );

        $this->add_control(
            'about_us_text', [
                'label' => esc_html__( 'About us text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Lorem ipsum dolor sit, consectetur adipiscing elit eiusmod tempor incididunt ut labore et dol magna aliqua.mollit anim laborum.Duis aute iru dolor in re voluptate velit esse cillum dolore eu quifugi nulla pariatur. Excepteur uino sint occaecat cupidatat non. in culpa deserunt mollit aboru. et dolore aliqua. Ut enim ad mini veniam'
            ]
        );

        $this->add_control(
            'author_name', [
                'label' => esc_html__( 'Profile Name', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Samuel Ingram.'
            ]
        );

        $this->add_control(
            'signature',
            [
                'label' => esc_html__( 'Signature Upload', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA
            ]
        );

        $this->add_control(
            'about_us_button', [
                'label' => esc_html__( 'Button Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More about us'
            ]
        );
        
        $this->add_control(
            'about_us_button_url', [
                'label' => esc_html__( 'Button Link', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '#'
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        ?>
        <div class="op-about-us-one">
            <div class="upper-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                        <?php if ( !empty($settings['section_heading']) ) : ?>
                        <h2 class="theme-title-four font-k2d">
                            <?php echo htmlspecialchars_decode( $settings['section_heading'] ); ?>
                        </h2>                    
                        <?php endif; ?>

                        </div> <!-- /.col- -->
                        <div class="col-lg-6 ml-auto">
                            <?php if ( !empty($settings['topper_content']) ) : ?>
                            <div class="top-text">
                                <p class="font-lato"><?php echo htmlspecialchars_decode( $settings['topper_content'] ); ?></p>
                            </div> <!-- /.top-text -->
                            <?php endif; ?>
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.upper-wrapper -->

            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <?php if (!empty($settings['about_img']['url'])) : ?>
                        <div class="img-box">
                            <img src="<?php echo esc_url($settings['about_img']['url']) ?>" alt="">
                            <div class="text"><?php echo htmlspecialchars_decode( $settings['img_box_text'] ); ?></div>
                        </div>
                        <?php endif; ?>
                    </div>

                    <div class="col-lg-6">
                        <div class="bottom-text">
                            <?php if ( !empty($settings['about_us_title']) ) : ?>
                                <h4><?php echo htmlspecialchars_decode( $settings['about_us_title'] ); ?></h4>
                            <?php endif; ?>
                            <?php if ( !empty($settings['about_us_text']) ) : ?>
                                <p class="font-lato"><?php echo htmlspecialchars_decode( $settings['about_us_text'] ); ?></p>
                            <?php endif; ?>
                            <?php if ( !empty($settings['author_name']) ) : ?>
                                <h6 class="name"><?php echo htmlspecialchars_decode( $settings['author_name'] ); ?></h6>
                            <?php endif; ?>
                            <?php if (!empty($settings['signature']['url'])) : ?>
                               <img src="<?php echo esc_url($settings['signature']['url']) ?>" alt="">
                            <?php endif; ?>
                            <?php if ( !empty($settings['about_us_button']) ) : ?>
                                <a href="<?php echo $settings['about_us_button_url']; ?>" class="more-button"><?php echo htmlspecialchars_decode( $settings['about_us_button'] ); ?> <i class="flaticon-next-1"></i></a>
                            <?php endif; ?>
                        </div> <!-- /.bottom-text -->
                    </div>
                </div>
            </div>
        </div> <!-- /.op-about-us-one -->           
        <?php
    }
}