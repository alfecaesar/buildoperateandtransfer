<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_portfolio_with_title extends Widget_Base {

    public function get_name() {
        return 'rogan_portfolio_with_title';
    }

    public function get_title() {
        return __( 'Portfolio Filter <br> with Title & Button', 'rogan-hero' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_keywords() {
        return [ 'Projects', 'portfolio' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return [ 'fancybox' ];
    }

    public function get_script_depends() {
        return [ 'fancybox', 'isotope' ];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'PORJECT'
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Project Gallery.'
            ]
        );

        $this->end_controls_section();

        //************************* Button Settings ***************************//
        $this->start_controls_section(
            'btn_sec',
            [
                'label' => esc_html__( 'Button Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'btn_title',
            [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'View Gallery'
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
            ]
        );

        $this->start_controls_tabs(
            'style_tabs'
        );

        /// Normal Button Style
        $this->start_controls_tab(
            'style_normal_btn',
            [
                'label' => __( 'Normal', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'font_color', [
                'label' => __( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three' => 'background-color: {{VALUE}}; border-color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'style_hover_btn',
            [
                'label' => __( 'Hover', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'hover_font_color', [
                'label' => __( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .line-button-three:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_section();

        // -------------------------------------------- Filtering
        $this->start_controls_section(
            'portfolio_filter', [
                'label' => __( 'Portfolio Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'all_label', [
                'label' => esc_html__( 'All filter label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'All'
            ]
        );

        $this->add_control(
            'title_word_limit', [
                'label' => esc_html__( 'Title Word Limit', 'rogan-core' ),
                'description' => esc_html__( 'Portfolio Title Word Limit', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 3
            ]
        );

        $this->add_control(
            'show_cat_count', [
                'label' => esc_html__( 'Show Category Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 3
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 3
            ]
        );

        $this->add_control(
            'show_features', [
                'label' => esc_html__( 'Key Features', 'rogan-core' ),
                'description' => esc_html__( 'Limit the Key Features to display on hover.', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 1,
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'post__in', [
                'label' => esc_html__( 'Show Specific Portfolios', 'rogan-core' ),
                'description' => esc_html__( 'Enter the portfolio post IDs to show. Input the multiple ID with comma separated', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'display_block' => true
            ]
        );

        $this->add_control(
            'exclude', [
                'label' => esc_html__( 'Exclude Portfolio', 'rogan-core' ),
                'description' => esc_html__( 'Enter the portfolio post ID to hide. Input the multiple ID with comma separated', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'portfolio_color', [
                'label' => __( 'Colors', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'active_filter_color', [
                'label' => __( 'Active Filter Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-project .isotop-menu-wrapper li.is-checked' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'filter_color', [
                'label' => __( 'Filter Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .our-project .isotop-menu-wrapper li' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $portfolios = new WP_Query(array(
            'post_type'         => 'portfolio',
            'posts_per_page'    => $settings['show_count'],
            'order'             => $settings['order'],
            'post__not_in'      => !empty($settings['exclude']) ? explode(',', $settings['exclude']) : '',
            'post__in'          => !empty($settings['post__in']) ? explode(',', $settings['post__in']) : '',
        ));
        $portfolio_cats = get_terms(array(
            'taxonomy' => 'portfolio_cat',
            'hide_empty' => true
        ));
        ?>
        <div class="arch-project-gallery">
            <div class="container">
                <div class="d-lg-flex justify-content-between align-items-center">
                    <div class="theme-title-one arch-title">
                        <?php if ( !empty($settings['upper_title']) ) : ?>
                            <div class="upper-title"> <?php echo wp_kses_post($settings['upper_title']) ?> </div>
                        <?php endif; ?>
                        <?php if ( !empty($settings['title']) ) : ?>
                            <h2 class="main-title"> <?php echo wp_kses_post($settings['title']) ?> </h2>
                        <?php endif; ?>
                    </div>

                    <ul class="isotop-menu-wrapper">
                        <li class="is-checked" data-filter="*"> <?php echo esc_html($settings['all_label']); ?> </li>
                        <?php
                        $cat_i = 0;
                        foreach ($portfolio_cats as $portfolio_cat) :
                            if ( !empty($settings['show_cat_count']) ) {
                                if ($settings['show_cat_count'] == $cat_i) {
                                    break;
                                }
                            }
                            ?>
                            <li data-filter=".<?php echo $portfolio_cat->slug ?>">
                                <?php echo $portfolio_cat->name ?>
                            </li>
                            <?php
                            ++$cat_i;
                        endforeach;
                        ?>
                    </ul>
                    <?php if ( !empty($settings['btn_title']) ) : ?>
                        <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" <?php rogan_is_exno($settings['btn_url']) ?> class="line-button-three">
                            <?php echo esc_html($settings['btn_title']) ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div> <!-- /.container -->

            <div class="inner-container our-project">
                <!-- Project Image -->
                <div id="isotop-gallery-wrapper">
                    <div class="grid-sizer"></div>
                    <?php
                    while ($portfolios->have_posts()) : $portfolios->the_post();
                        $cats = get_the_terms(get_the_ID(), 'portfolio_cat');
                        $cat_slug = '';
                        if(is_array($cats)) {
                            foreach ($cats as $cat) {
                                $cat_slug .= $cat->slug.' ';
                            }
                        }
                        $title = wp_trim_words(get_the_title(), $settings['title_word_limit'], '');
                        $title_first_part = preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                        $title_pieces = explode(' ', $title);
                        $title_last_word = array_pop($title_pieces);
                        $main_title = $title_first_part . " <span>$title_last_word</span>";
                        ?>
                        <div class="isotop-item <?php echo esc_attr($cat_slug) ?>">
                            <div class="project-item">
                                <div class="img-box">
                                    <?php the_post_thumbnail('rogan_570x750') ?>
                                </div>
                                <div class="hover-valina">
                                    <div>
                                        <h4 class="title" title="<?php the_title() ?>">
                                            <a href="<?php the_permalink() ?>"> <?php echo wp_kses_post($main_title); ?> </a>
                                        </h4>
                                        <?php
                                        $i = 0;
                                        while ( have_rows('key_features') ) : the_row();
                                            if ( !empty($settings['show_features']) ) {
                                                if ($i == $settings['show_features']) {
                                                    break;
                                                }
                                            }
                                            echo wpautop( get_sub_field('feature_title') );
                                            ++$i;
                                        endwhile;
                                        ?>
                                        <a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery">
                                            <img src="<?php echo plugins_url('images/icon/zoom-in.svg', __FILE__) ?>" alt="<?php the_title() ?>" class="svg">
                                        </a>
                                    </div>
                                </div> <!-- /.hover-valina -->
                            </div> <!-- /.project-item -->
                        </div> <!-- /.isotop-item -->
                    <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
            </div> <!-- /#isotop-gallery-wrapper -->
            </div> <!-- /.inner-container -->
        </div>

        <script>
            (function ( $ ) {
                "use strict";
                $(window).load(function () {
                    if ($("#isotop-gallery-wrapper").length) {
                        var $grid = $('#isotop-gallery-wrapper').isotope({
                            // options
                            itemSelector: '.isotop-item',
                            percentPosition: true,
                            masonry: {
                                // use element for option
                                columnWidth: '.grid-sizer'
                            }
                        });

                        // filter items on button click
                        $('.isotop-menu-wrapper').on( 'click', 'li', function() {
                            var filterValue = $(this).attr('data-filter');
                            $grid.isotope({ filter: filterValue });
                        });

                        // change is-checked class on buttons
                        $('.isotop-menu-wrapper').each( function( i, buttonGroup ) {
                            var $buttonGroup = $( buttonGroup );
                            $buttonGroup.on( 'click', 'li', function() {
                                $buttonGroup.find('.is-checked').removeClass('is-checked');
                                $( this ).addClass('is-checked');
                            });
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }

}