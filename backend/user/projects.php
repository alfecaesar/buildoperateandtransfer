<?php include '../user/template/header.php';


include '../dbconfig.php';

?>

<?php include '../user/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../user/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Projects</h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="create-project.php" class="btn btn-yellow"><span>Create Project <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <?php 
                    $project_saved = (!empty($_GET['project_saved'])) ? $_GET['project_saved'] : '';
                        if ($project_saved === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            Project Saved! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <?php 
                    $project_deleted = (!empty($_GET['project_deleted'])) ? $_GET['project_deleted'] : '';
                        if ($project_deleted === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            Project Successfully Deleted! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <div class="project-box row project-box-head">
                        <div class="project-box-name col col-4 text-left">Title</div>
                        <div class="project-box-status col col-2 text-left">Budget</div>
                        <div class="project-box-date col col-3 text-left">Start Date - End Date</div>
                        <div class="project-box-action col col-3 text-right">Action</div>
                    </div>

                    <?php

                        $datas = $database->select("project", [
                            "project_id",
                            "project_name",
                            "specific_budget",
                            "specific_timeframe_to_complete_min",
                            "specific_timeframe_to_complete_max"
                        ], [
                            "user_email" => $_SESSION["user_email"]
                        ]);

                        foreach($datas as $data)
                        {
                            echo '<div class="project-box row">' .
                                    '<div class="project-box-name col col-4">'. $data['project_name'] .'</div>' .
                                    '<div class="project-box-status col col-2 text-left">'. $data['specific_budget'] .'</div>' .
                                    '<div class="project-box-date col col-3 text-left">'. $data['specific_timeframe_to_complete_min'] .' - '. $data['specific_timeframe_to_complete_max'] .'</div>' .
                                    '<div class="project-box-action col col-3 text-right">' . 
                                        //'<a href="#" class="action-link-view"><i class="fas fa-eye"></i> View</a> | <a href="#" class="action-link-edit"><i class="fas fa-pencil-alt"></i> Edit</a> | <a href="javascript:void(0)" onclick="deleteProjectURL('. $data['project_id'] .')" data-toggle="modal" data-target="#modalbox_deleteProject" class="action-link-delete"><i class="fas fa-trash-alt"></i> Delete</a>' . 
                                        '<a href="project_detail.php?project_id='. $data['project_id'] .'" class="action-link-view"><i class="fas fa-eye"></i> View</a> | <a href="edit_project.php?project_id='. $data['project_id'] .'" class="action-link-edit"><i class="fas fa-pencil-alt"></i> Edit</a> | <a href="javascript:void(0)" onclick="deleteProjectURL('. $data['project_id'] .')" data-toggle="modal" data-target="#modalbox_deleteProject" class="action-link-delete"><i class="fas fa-trash-alt"></i> Delete</a>' . 
                                    '</div>' . 
                                 '</div>';
                        }
                    ?>

                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>


<?php include '../user/template/modal/delete_project.php';?>
<?php include '../user/template/footer.php';?>