<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Repeater;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_tabs_horizontal extends Widget_Base {

    public function get_name() {
        return 'Rogan_tabs_horizontal';
    }

    public function get_title() {
        return esc_html__( 'Horizontal Tabs', 'rogan-hero' );
    }

    public function get_icon() {
        return ' eicon-tabs';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        /// ----------------------------------------  Title Section  ------------------------------///
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'About me'
            ]
        );

        $this->end_controls_section();

        /// ----------------------------------------  Title Section  ------------------------------///
        $this->start_controls_section(
            'featured_sec',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'featured_image',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'label_block' => true,
            ]
        );

        $this->end_controls_section();


        // ------------------------------ Feature list ------------------------------
        $this->start_controls_section(
            'section_tabs',
            [
                'label' => esc_html__( 'Tabs', 'rogan-core' ),
            ]
        );

        $repeater = new Repeater();

        $repeater->add_control(
            'tab_title',
            [
                'label' => esc_html__( 'Tab Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => esc_html__( 'Tab Title', 'rogan-core' ),
                'label_block' => true,
            ]
        );
        $repeater->add_control(
            'tab_content',
            [
                'label' => esc_html__( 'Tab Contents', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $this->add_control(
            'tabs',
            [
                'label' => esc_html__( 'Tabs Items', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ tab_title }}}',
            ]
        );

        $this->end_controls_section();


        /// --------------------  Buttons ----------------------------
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Buttons', 'saasland-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'btn_title', [
                'label' => esc_html__( 'Button Title', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Get Started'
            ]
        );
        $repeater->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'saasland-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );
        $repeater->start_controls_tabs(
            'style_tabs'
        );
        /// Normal Button Style
        $repeater->start_controls_tab(
            'style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );
        $repeater->add_control(
            'font_color', [
                'label' => esc_html__( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-color: {{VALUE}}; border-color: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'border_color', [
                'label' => esc_html__( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'border-color: {{VALUE}}',
                )
            ]
        );
        $repeater->end_controls_tab();
        /// ----------------------------- Hover Button Style
        $repeater->start_controls_tab(
            'style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );
        $repeater->add_control(
            'hover_font_color', [
                'label' => esc_html__( 'Font Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'color: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'background: {{VALUE}}',
                )
            ]
        );
        $repeater->add_control(
            'hover_border_color', [
                'label' => esc_html__( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );
        $repeater->end_controls_tab();
        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create buttons', 'saasland-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ btn_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section(); // End Buttons


        /**
         * Style Tab
         * Text Color
         * Background Gradient
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_prefix', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_prefix',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-two',
            ]
        );

        $this->end_controls_section();

        /// -------------------- Background Gradient ----------------------------
        $this->start_controls_section(
            'style_section',
            [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color 01', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );
        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color 02', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-three:before' => 'background: -webkit-radial-gradient( 50% 50%, circle closest-side, {{bg_color.VALUE}} 0%, {{VALUE}} 100%)',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        $tabs = $this->get_settings_for_display( 'tabs' );
        $id_int = substr( $this->get_id_int(), 0, 3 );
        ?>
        <div class="about-me-portfo section-portfo">
            <div class="container">
                <div class="inner-content">
                    <?php if (!empty($settings['title'])) : ?>
                        <h2 class="theme-title-two"> <?php echo wp_kses_post($settings['title']) ?> </h2>
                    <?php endif; ?>
                    <div class="img-box hide-pr">
                        <?php echo wp_get_attachment_image($settings['featured_image']['id'], 'full') ?>
                    </div>
                    <div class="profile-tab">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs hide-pr">
                            <?php
                            $i = 0.2;
                            foreach ( $tabs as $index => $item ) :
                                $tab_count = $index + 1;
                                $tab_title_setting_key = $this->get_repeater_setting_key( 'tab_title', 'tabs', $index );
                                $active = $tab_count == 1 ? 'active' : '';
                                $this->add_render_attribute( $tab_title_setting_key, [
                                    'class' => [ 'nav-link', $active ],
                                    'id' => 'rogan'.'-tab-'.$id_int . $tab_count,
                                    'data-toggle' => 'tab',
                                    'href' => '#rogan-tab-content-' . $id_int . $tab_count,
                                ]);
                                ?>
                                <li class="nav-item">
                                    <a <?php echo $this->get_render_attribute_string( $tab_title_setting_key ); ?>>
                                        <?php echo wp_kses_post($item['tab_title']); ?>
                                    </a>
                                </li>
                                <?php
                                $i = $i + 0.2;
                            endforeach;
                            ?>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <?php
                            foreach ( $tabs as $index => $item ) :
                                $tab_count = $index + 1;
                                $active = $tab_count == 1 ? 'active' : '';
                                $fade = $index != 0 ? 'fade' : '';
                                $tab_content_setting_key = $this->get_repeater_setting_key( 'tab_content', 'tabs', $index );
                                $this->add_render_attribute( $tab_content_setting_key, [
                                    'class' => [ 'tab-pane', $fade, $active ],
                                    'aria-labelledby' => 'rogan'.'-tab-'.$id_int . $tab_count,
                                    'id' => 'rogan-tab-content-' . $id_int . $tab_count,
                                ]);
                                ?>
                                <div <?php echo $this->get_render_attribute_string( $tab_content_setting_key ); ?>>
                                    <?php echo wp_kses_post( wpautop( $this->parse_text_editor( $item['tab_content'] )) ); ?>
                                </div>
                                <?php
                            endforeach;
                            ?>
                        </div>

                        <ul class="button-group">
                        <?php
                        $i = 0;
                        foreach ($settings['buttons'] as $button) {
                            ++$i;
                            $strip_class = ($i % 2 == 1) ? 'download-button' : 'hire-button';
                            echo "<li>
                                    <a href='{$button['btn_url']['url']}' class='$strip_class hire-btn elementor-repeater-item-{$button['_id']}'> 
                                        {$button['btn_title']}
                                    </a>
                                 </li>";
                        }
                        ?>
                        </ul>
                    </div> <!-- /.profile-tab -->
                </div> <!-- /.inner-content -->
            </div>
        </div>
        <?php
    }
}