<?php include '../user/template/header.php';

include '../dbconfig.php';

?>

<?php include '../user/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../user/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Dashboard</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <?php 
                        $project_saved = (!empty($_GET['project_saved'])) ? $_GET['project_saved'] : '';
                        if ($project_saved === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            Project Saved! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <!--
                    <h5>Enterprises</h5>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img class="d-block w-100" src="https://fakeimg.pl/440x150/282828/eae0d0/?retina=1?text=" alt="First slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Title 1</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut tempor odio. Sed tristique justo quis nisi condimentum efficitur.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://fakeimg.pl/440x150/282828/eae0d0/?retina=1?text=" alt="Second slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Title 2</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut tempor odio. Sed tristique justo quis nisi condimentum efficitur.</p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img class="d-block w-100" src="https://fakeimg.pl/440x150/282828/eae0d0/?retina=1?text=" alt="Third slide">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Title 3</h5>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent ut tempor odio. Sed tristique justo quis nisi condimentum efficitur.</p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    </div>

                    <p>&nbsp;</p>
                    -->

                    <?php
                        $botCount = $database->count("project", [
                            "user_email" => $_SESSION["user_email"]
                        ]);

                        $milestoneCount = $database->count("milestone", [
                            "project_owner_user_id" => $_SESSION["user_id"]
                        ]);

                        $taskCount = $database->count("task_list", [
                            "project_owner_user_id" => $_SESSION["user_id"]
                        ]);
                    ?>

                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Total BOT</h5>
                                    <p class="card-text"><?php echo $botCount; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Total Milestone</h5>
                                    <p class="card-text"><?php echo $milestoneCount; ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Total Task</h5>
                                    <p class="card-text"><?php echo $taskCount; ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Completed BOT</h5>
                                    <p class="card-text">0</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Completed Milestone</h5>
                                    <p class="card-text">0</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Completed Task</h5>
                                    <p class="card-text">0</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p>&nbsp;</p>
                    <h5><?php echo $_SESSION["user_name"]; ?> Activity</h5>

                    <?php

                    $datas = $database->select("user_activity", [
                        "user_id",
                        "activity_name",
                        "activity_description",
                        "activity_timestamp"
                    ], [
                        "user_id" => $_SESSION["user_id"],
                        "ORDER" => ["user_activity.activity_timestamp" => "DESC"],
                        "LIMIT" => 50
                    ]);

                    foreach($datas as $data){

                    $date = date('F j, Y \a\t g:ia', strtotime($data["activity_timestamp"]));  
                    
                    ?>

                        <div class="alert alert-secondary user-activity-text" role="alert">
                            <?php echo $data["activity_description"] ?> <span><?php echo $date; ?></span>
                        </div>


                    <?php } ?>
                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>

<?php include '../user/template/footer.php';?>