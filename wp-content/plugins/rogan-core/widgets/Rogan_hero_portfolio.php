<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_portfolio extends Widget_Base {

    public function get_name() {
        return 'rogan_hero_portfolio';
    }

    public function get_title() {
        return esc_html__( 'Hero Portfolio (Dark)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_keywords() {
        return [ 'animated', 'headline', 'hero' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return ['animated-headline'];
    }

    public function get_script_depends() {
        return ['animated-headline'];
    }

    protected function _register_controls() {

        // ----------------------------------------  Title Section  ------------------------------------//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'I Am Rashed Develop Quality',
            ]
        );

        $this->end_controls_section();



        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'fimg_sec',
            [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimg', [
                'label' => esc_html__( 'Upload the featured image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/banner-shape5.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'fimg1_position', [
                'label' => esc_html__( 'Position', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%' ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-three .shape-one' => 'top: {{TOP}}{{UNIT}}; right: {{RIGHT}}{{UNIT}}; bottom: {{BOTTOM}}{{UNIT}}; left: {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'isLinked' => false
                ]
            ]
        );

        $this->end_controls_section();

        /// --------------------------------------- Words Section ----------------------------------///
        $this->start_controls_section(
            'content_list',
            [
                'label' => esc_html__( 'Words', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'title', [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => 'Website'
            ]
        );

        $repeater->add_control(
            'append_text', [
                'label' => esc_html__( 'Append Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default'   => '.'
            ]
        );

        // repeater field
        $this->add_control(
            'words', [
                'label' => esc_html__( 'Word', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();



        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-three .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} #theme-banner-three .main-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Gradient Color ------------------------------
        $this->start_controls_section(
            'style_gradient_sec',
            [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-three:before' => 'background: -webkit-radial-gradient( 50% 50%, circle closest-side, {{bg_color.VALUE}} 0%, {{VALUE}} 100%)',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        ?>

        <div id="theme-banner-three">
            <?php if (!empty($settings['fimg']['url'])) : ?>
                <img src="<?php echo esc_url($settings['fimg']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="shape-one">
            <?php endif; ?>
            <div class="container">
                <div class="main-wrapper">
                    <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.2s">
                        <span class="cd-headline clip">
                            <?php if (!empty($settings['title_text'])) : ?>
                                <span><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></span>
                            <?php endif; ?>
			                    <span class="cd-words-wrapper">
                                   <?php
                                   if(!empty($settings['words'])) {
                                      foreach ($settings['words'] as $i => $word) {
                                          $is_visible =  ($i == 0) ? "class='is-visible'" : '';
                                          $append_text = !empty($word['append_text']) ? "<i>{$word['append_text']}</i>" : '';
                                          echo "<b $is_visible> {$word['title']} $append_text </b>";
                                      }
                                   }
                                   ?>
                                </span>
                        </span>
                    </h1>
                </div> <!-- /.main-wrapper -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}
