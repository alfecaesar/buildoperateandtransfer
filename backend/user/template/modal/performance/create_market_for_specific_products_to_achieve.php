<!-- Modal -->
<div class="modal fade" id="modalbox_performance_7" tabindex="-1" role="dialog" aria-labelledby="Create market for specific products to achieve" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create market for specific products to achieve</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Create market for specific products to achieve</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="create_market_for_products_to_achieve" id="create_market_for_products_to_achieve" class="form-control" multiple>
                            <option id="Revenue">Revenue</option>
                            <option id="Likes, Follows and Shares for product">Likes,Follows and Shares for product</option>
                            <option id="Positive comments ">Positive comments </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>