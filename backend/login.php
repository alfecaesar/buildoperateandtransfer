<?php

include 'dbconfig.php';

$user_email = (!empty($_POST['user_email'])) ? $_POST['user_email'] : '';
$user_password = (!empty($_POST['user_password'])) ? $_POST['user_password'] : '';


$count = $database->count("user", [
    "user_email" => $user_email,
    "user_password" => $user_password
]);


if($count !== 0){
    session_start();

    $_SESSION["user_name"] = $database->get("user", "user_name", [ "user_email" => $user_email ]);
    $_SESSION["user_email"] = $user_email;
    $_SESSION["user_password"] = $user_password;
    $_SESSION["access_type"] = $database->get("user", "access_type", [ "user_email" => $user_email ]);
    $_SESSION["user_id"] = $database->get("user", "user_id", [ "user_email" => $user_email ]);
    $access_type = $database->get("user", "access_type", [ "user_email" => $user_email ]);

    if($access_type === 'user'){
        header("Location: http://afarinsights.com/backend/user/?login_success=true");
    }
    elseif ($access_type === 'bot'){
        header("Location: http://afarinsights.com/backend/bot/?login_success=true");
    }
}
else{
    header("Location: http://afarinsights.com/backend/?login_error=true");
}
    

?>