<!-- Modal -->
<div class="modal fade" id="modalbox_features_7" tabindex="-1" role="dialog" aria-labelledby="What additional features are important to you?" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">What additional features are important to you?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">What additional features are important to you?</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="additional_features_are_important_to_you" id="additional_features_are_important_to_you" class="form-control" multiple>
                            <option id="Landing Page">Landing Page</option>
                            <option id="Website Development">Website Development</option>
                            <option id="API Integration">API Integration</option>
                            <option id="App Development ">App Development </option>
                            <option id="Bug Fixes">Bug Fixes</option>
                            <option id="Design Enhancement">Design Enhancement</option>
                            <option id="Maintenance">Maintenance</option>
                            <option id="Platform Plugin">Platform Plugin</option>
                            <option id="Responsive Design">Responsive Design</option>
                            <option id="Template">Template</option>
                            <option id="Theme Customization">Theme Customization</option>
                            <option id="Theme Development">Theme Development</option>
                            <option id="We Design">We Design</option>
                            <option id="Website Customization">Website Customization</option>
                            <option id="Website Migration">Website Migration</option>
                            <option id="Magento">Magento</option>
                            <option id="Squarespace">Squarespace</option>
                            <option id="Wix">Wix</option>
                            <option id="WordPress">WordPress</option>
                            <option id="Shopify">Shopify</option>
                            <option id="WooCommerce">WooCommerce</option>
                            <option id="BigCommerce">BigCommerce</option>
                            <option id="OpenCart">OpenCart</option>
                            <option id="PrestaShop">PrestaShop</option>
                            <option id="ClickFunnels">ClickFunnels</option>
                            <option id="3dcart">3dcart</option>
                            <option id="Commerce Cloud">Commerce Cloud</option>
                            <option id="Custom">Custom</option>
                            <option id="Drupal">Drupal</option>
                            <option id="nopCommerce">nopCommerce</option>
                            <option id="osCommerce">osCommerce</option>
                            <option id="Volusion">Volusion</option>
                            <option id="X-Cart">X-Cart</option>
                            <option id="Zen Cart">Zen Cart</option>
                            <option id="Java">Java</option>
                            <option id="JavaScript">JavaScript</option>
                            <option id="Angular">Angular</option>
                            <option id="AngularJS">AngularJS</option>
                            <option id="ASP.NET">ASP.NET</option>
                            <option id="Bootstrap">Bootstrap</option>
                            <option id="jQuery">jQuery</option>
                            <option id="PHP">PHP</option>
                            <option id="React">React</option>
                            <option id="Ruby">Ruby</option>
                            <option id="ue.js">ue.js</option>
                            <option id="Python">Python</option>
                            <option id="CSS">CSS</option>
                            <option id="CSS3">CSS3</option>
                            <option id="HTML">HTML</option>
                            <option id="HTML5">HTML5</option>
                            <option id="Node.js">Node.js</option>
                            <option id="Joomla">Joomla</option>
                            <option id="jQuery Mobile">jQuery Mobile</option>
                            <option id="Laravel ">Laravel </option>
                            <option id="Liquid">Liquid</option>
                            <option id="Ruby on Rails">Ruby on Rails</option>
                            <option id="PayPal">PayPal</option>
                            <option id="Stripe">Stripe</option>
                            <option id="Braintree">Braintree</option>
                            <option id="WePay">WePay</option>
                            <option id="Wordlpay">Wordlpay</option>
                            <option id="Authorize.Net">Authorize.Net</option>
                            <option id="2Checkout">2Checkout</option>
                            <option id="Sage Pay">Sage Pay</option>
                            <option id="Payoneer">Payoneer</option>
                            <option id="Bank APIs">Bank APIs</option>
                            <option id="SQLite">SQLite</option>
                            <option id="Realm Database">Realm Database</option>
                            <option id="Couchbase">Couchbase</option>
                            <option id="LevelDB">LevelDB</option>
                            <option id="PaperDb">PaperDb</option>
                            <option id="Azure Cosmos DB">Azure Cosmos DB</option>
                            <option id="MongoDB">MongoDB</option>
                            <option id="Microsoft SQL Server">Microsoft SQL Server</option>
                            <option id="MariaDB">MariaDB</option>
                            <option id="MySQL">MySQL</option>
                            <option id="Oracle NoSQL Database">Oracle NoSQL Database</option>
                            <option id="PostgreSQL">PostgreSQL</option>
                            <option id="Oracle Database">Oracle Database</option>
                            <option id="Neo4j">Neo4j</option>
                            <option id="Actian X">Actian X</option>
                            <option id="Adabas">Adabas</option>
                            <option id="Aerospike">Aerospike</option>
                            <option id="AllegroGraph">AllegroGraph</option>
                            <option id="Aitibase">Aitibase</option>
                            <option id="Very Small (1-9 employees)">Very Small (1-9 employees)</option>
                            <option id="Small (10-99 employees)">Small (10-99 employees)</option>
                            <option id="Mid (100-999 employees)">Mid (100-999 employees)</option>
                            <option id="Large (1000+ employees)">Large (1000+ employees)</option>
                            <option id="Startup">Startup</option>
                            <option id="Fortune 500">Fortune 500</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>