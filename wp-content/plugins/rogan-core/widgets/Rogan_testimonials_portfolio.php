<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_testimonials_portfolio extends Widget_Base {

    public function get_name() {
        return 'rogan_testimonials_portfolio';
    }

    public function get_title() {
        return esc_html__( 'Testimonials Dark', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-testimonial-carousel';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['owl-carousel'];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate'];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Testimonials<span>.</span>'
            ]
        );

        $this->end_controls_section();

        //***********************************  Inner Title ******************************************//
        $this->start_controls_section(
            'inner_title_sec',
            [
                'label' => esc_html__( 'Inner Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'inner_title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'What’s <br> Our Client Say About Us.'
            ]
        );

        $this->end_controls_section();


        //*********************************** Page Number ******************************************//
        $this->start_controls_section(
            'number_sec',
            [
                'label' => esc_html__( 'Number', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'number1',
            [
                'label' => esc_html__( 'Number One', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->add_control(
            'number2',
            [
                'label' => esc_html__( 'Number Two', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->end_controls_section();


        //********************************* Testimonials List **********************************//
        $this->start_controls_section(
            'testimonials_sec',
            [
                'label' => esc_html__( 'Testimonials Slider', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $repeater->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Developer'
            ]
        );

        $repeater->add_control(
            'contents',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'author_img',
            [
                'label' => esc_html__( 'Author Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'testimonials_sliders',
            [
                'label' => esc_html__( 'Sliders Section', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{ name }}}',
            ]
        );

        $this->end_controls_section();



        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_sec_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_sce_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-two',
            ]
        );

        $this->end_controls_section();

        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_inner_title', [
                'label' => esc_html__( 'Inner Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_inner_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .portfo-testimonial .inner-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_inner_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .portfo-testimonial .inner-title',
            ]
        );

        $this->end_controls_section();

        /********************************* Page Number ********************************/
        $this->start_controls_section(
            'style_page_number', [
                'label' => esc_html__( 'Number Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_page_number',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-portfo .section-num' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_page_number',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .section-portfo .section-num',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Gradient Color ------------------------------
        $this->start_controls_section(
            'style_gradient_sec',
            [
                'label' => esc_html__( 'Background Gradient', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .portfo-testimonial:before' => 'background: -webkit-radial-gradient( 50% 50%, {{bg_color.VALUE}} 0%, {{VALUE}} 100%)',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $testimonials = $settings['testimonials_sliders'];

        ?>

        <div class="portfo-testimonial section-portfo">
                <div class="section-num hide-pr">
                    <?php if(isset($settings['number1'])) : ?>
                        <span><?php echo esc_html($settings['number1']) ?></span>
                    <?php endif; ?>
                    <?php if(isset($settings['number2'])) : ?>
                        <span><?php echo esc_html($settings['number2']) ?></span>
                    <?php endif; ?>
                </div>
            <div class="container clearfix">
                <?php if(!empty($settings['title_text'])) : ?>
                    <h2 class="theme-title-two"><?php echo wp_kses_post($settings['title_text']) ?></h2>
                <?php endif; ?>
                <div class="row">
                    <?php if(!empty($settings['inner_title'])) : ?>
                        <div class="col-lg-4">
                            <h2 class="inner-title"><?php echo wp_kses_post($settings['inner_title']) ?></h2>
                        </div>
                    <?php endif; ?>
                    <div class="col-lg-8">
                        <div class="slider-wrapper">
                            <div class="agn-testimonial-slider">
                                <?php
                                if($testimonials) {
                                foreach ($testimonials as $testimonial) {
                                    $image = isset($testimonial['author_img']['id']) ? $testimonial['author_img']['id'] : '';
                                    $image = wp_get_attachment_image_src($image, 'full');
                                    ?>
                                    <div class="item elementor-repeater-item-<?php echo $testimonial['_id'] ?>">
                                        <?php echo !empty($testimonial['contents']) ? wpautop($testimonial['contents']) : ''; ?>
                                        <div class="author-info clearfix">
                                            <img src="<?php echo $image[0] ?>" alt="<?php echo esc_attr($testimonial['name']) ?>" class="author-img">
                                            <div class="name-info">
                                                <?php if (!empty($testimonial['name'])) : ?>
                                                    <h6 class="name"><?php echo esc_html($testimonial['name']) ?></h6>
                                                <?php endif; ?>
                                                <?php if (!empty($testimonial['designation'])) : ?>
                                                    <span><?php echo esc_html($testimonial['designation']) ?></span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }}
                                ?>
                            </div> <!-- /.agn-testimonial-slider -->
                        </div> <!-- /.slider-wrapper -->
                    </div>
                </div>
            </div> <!-- /.container -->
        </div>


        <script>
            (function ( $ ) {
                "use strict";
                $(document).on ('ready', function (){
                    var agnTslider = $ (".agn-testimonial-slider");
                    if(agnTslider.length) {
                        agnTslider.owlCarousel({
                            loop:true,
                            nav:true,
                            navText: ["<i class='flaticon-back'></i>" , "<i class='flaticon-next'></i>"],
                            dots:false,
                            autoplay:true,
                            autoplayTimeout:4000,
                            smartSpeed:1200,
                            autoplayHoverPause:true,
                            lazyLoad:true,
                            items:1
                        });
                    }
                });
            }( jQuery ));
        </script>
        <?php
    }
}