<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

$opt = get_option( 'rogan_opt' );
$blog_excerpt = !empty($opt['blog_excerpt']) ? $opt['blog_excerpt'] : 40;
$is_post_date = isset($opt['is_post_date']) ? $opt['is_post_date'] : '1';
$video_url = function_exists( 'get_field' ) ? get_field( 'video_url' ) : '';
?>
<div <?php post_class( 'single-blog-post' ) ?>>
    <div class="img-holder">
        <?php the_post_thumbnail(); ?>
        <?php if( $video_url ) : ?>
            <a data-fancybox href="<?php echo esc_url($video_url) ?>" class="fancybox video-button">
                <i class="flaticon-play-button-2"></i>
            </a>
        <?php endif; ?>
    </div>
    <div class="post-data">
        <?php if($is_post_date == '1' ) : ?>
            <a href="<?php rogan_day_link() ?>" class="date"> <?php the_date( 'j M, Y' ) ?> </a>
        <?php endif; ?>
        <h5 class="blog-title-two title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
        <p> <?php echo strip_shortcodes(rogan_excerpt( 'rogan_opt', 'blog_excerpt', false)); ?> </p>
        <a href="<?php the_permalink(); ?>" class="read-more"><i class="flaticon-next-1"></i></a>
    </div>
</div> <!-- /.single-blog-post -->