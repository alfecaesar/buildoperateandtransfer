<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_icon_boxes extends Widget_Base {

    public function get_name() {
        return 'Rogan_icon_boxes';
    }

    public function get_title() {
        return esc_html__( 'Icon Boxes', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-home-heart';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ***************************** Service Title **************************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_text',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Rogan is a Agency for Boost Up <br> Your Web Traffic.',
            ]
        );


        $this->add_control(
            'line_show',
            [
                'label' => esc_html__( 'Line Show', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'line_show_color',
            [
                'label' => esc_html__( 'Line Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one.upper-bar:before' => 'background: {{VALUE}}',
                ],
                'condition' => [
                    'line_show' => 'yes'
                ]
            ]
        );

        $this->end_controls_section();


        // *************************************** Service List *************************************//
        $this->start_controls_section(
            'service_sec',
            [
                'label' => esc_html__( 'Icon Boxes', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'default' => 'Customer Experience',
                'label_block' => true
            ]
        );

        $repeater->add_control(
            'title_url',
            [
                'label' => esc_html__( 'Title URL', 'rogan-core' ),
                'type' =>Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );

        $repeater->add_control(
            'icon_img',
            [
                'label' => esc_html__( 'Icon', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon4.svg', __FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'bg_color_icon_box',
            [
                'label' => esc_html__( 'Icon Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.single-block .wrapper .icon-box' => 'background: {{VALUE}};',
                ],
            ]
        );

        $repeater->add_control(
            'wave_color_icon_box',
            [
                'label' => esc_html__( 'Wave Color', 'rogan-core' ),
                'description' => esc_html__( 'The wave color will visible when hover over the icon box. Recommended to use rgba color as wave color.', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}.single-block .wrapper .icon-box:before' => 'background: {{VALUE}};',
                    '{{WRAPPER}} {{CURRENT_ITEM}}.single-block .wrapper .icon-box:after' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'icon_list',
            [
                'label' => esc_html__( 'Services', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @Title
         */
        /********************************* Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'section_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_section_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .seo-what-we-do' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $services = $settings['icon_list'];
        $is_line_show = ($settings['line_show'] == 'yes') ? 'upper-bar' : '';
        ?>
        <div class="seo-what-we-do">
            <div class="container">
                <?php if (!empty($settings['title_text'])) : ?>
                    <div class="theme-title-one title-underline text-center <?php echo esc_attr( $is_line_show) ?>">
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['title_text'])) ?></h2>
                    </div> <!-- /.theme-title-one -->
                <?php endif; ?>
                <div class="row">
                    <?php
                    $i = 0;
                    if (!empty($services)) {
                    foreach ($services as $service) {
                        ?>
                        <div class="col-lg-3 col-sm-6 single-block elementor-repeater-item-<?php echo $service['_id'] ?>" data-aos="fade-up"
                            <?php if ($i !=0) : ?> data-aos-delay="<?php echo $i; ?>"
                            <?php endif; ?>>
                            <div class="wrapper">
                                <?php if (!empty($service['icon_img']['url'])) : ?>
                                    <div class="icon-box">
                                        <img src="<?php echo esc_url($service['icon_img']['url']) ?>" alt="<?php echo esc_attr($service['title']) ?>">
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($service['title'])) : ?>
                                    <a href="<?php echo esc_url($service['title_url']['url']) ?>" class="title"
                                        <?php rogan_is_external($service['title_url']); rogan_is_nofollow($service['title_url']) ?>>
                                        <?php echo esc_html($service['title']) ?>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.wrapper -->
                        </div>
                        <?php
                        $i = $i + 100;
                    }}
                    ?>
                </div>
            </div> <!-- /.container -->
        </div>
        <?php
    }
}