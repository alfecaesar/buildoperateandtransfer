<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rogan
 */

if ( ! is_active_sidebar( 'sidebar_widgets' ) ) {
	return;
}
?>

<div class="col-lg-4 col-md-6 col-sm-8">
    <div class="theme-sidebar-widget">
	    <?php dynamic_sidebar( 'sidebar_widgets' ); ?>
	</div>
</div>