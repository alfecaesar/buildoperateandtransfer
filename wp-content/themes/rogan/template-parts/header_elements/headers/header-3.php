<?php
$opt = get_option( 'rogan_opt' );
$is_social_links_header3 = !empty($opt['is_social_links_header3']) ? $opt['is_social_links_header3'] : '1';
?>

<div class="theme-main-menu theme-menu-three">
    <div class="logo">
        <?php rogan_logo() ?>
    </div>
    <nav id="mega-menu-holder" class="navbar navbar-expand-lg">
        <div class="position-relative ml-auto nav-container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="flaticon-setup"></i>
            </button>
            <?php
            if(has_nav_menu( 'main_menu' )) {
                wp_nav_menu( array(
                    'menu' => 'main_menu',
                    'theme_location' => 'main_menu',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'navbarSupportedContent',
                    'menu_class' => 'navbar-nav',
                    'walker' => new Rogan_Nav_Navwalker(),
                    'depth' => 3
                ));
            }
            ?>
        </div> <!-- /.container -->
    </nav> <!-- /#mega-menu-holder -->
    <?php if( $is_social_links_header3 == '1' ) : ?>
    <div class="header-right-widget">
        <ul class="social-icon">
            <?php rogan_social_links() ?>
        </ul>
    </div> <!-- /.header-right-widget -->
    <?php endif; ?>
</div>