<?php

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

// Remove product single meta
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );


// Enabling the gallery in themes that declare
add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );


// Product Gallery thumbnail size
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
    return array(
        'width' => 120,
        'height' => 140,
        'crop' => 1,
    );
} );


/**
 * Checkout form fields customizing
 */
add_filter( 'woocommerce_checkout_fields' , function ( $fields ) {

    unset($fields['billing']['billing_address_2']);
    unset($fields['shipping']['shipping_address_2']);

    $fields['billing']['billing_first_name'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'First name *', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_last_name'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Last name *', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_company'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Company name (optional)', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_country'] = array(
        'label'         => '',
        'required'      => false,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'type'          => 'country',
        'input_class'   => array( 'theme-select-menu' )
    );

    $fields['billing']['billing_address_1'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Street address *', 'placeholder', 'rogan' ),
        'required'      => true,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_city'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Town/City *', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_state'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'State*', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'theme-select-menu' ),
        'type'          => 'state'
    );

    $fields['billing']['billing_postcode'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Zip Code* (optional)', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_phone'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Phone', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['billing']['billing_email'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Email address *', 'placeholder', 'rogan' ),
        'required'      => true,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    // Shipping Fields
    $fields['shipping']['shipping_first_name'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'First name *', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_last_name'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Last name *', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_company'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Company name (optional)', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_country'] = array(
        'label'         => '',
        'required'      => false,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'type'          => 'country',
        'input_class'   => array( 'theme-select-menu' )
    );

    $fields['shipping']['shipping_address_1'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Street address *', 'placeholder', 'rogan' ),
        'required'      => true,
        'class'         => array( 'col-12' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_city'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Town/City *', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_state'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'State*', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'theme-select-menu' ),
        'type'          => 'state'
    );

    $fields['shipping']['shipping_postcode'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Zip Code* (optional)', 'placeholder', 'rogan' ),
        'class'         => array( 'col-lg-4' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_phone'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Phone', 'placeholder', 'rogan' ),
        'required'      => false,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    $fields['shipping']['shipping_email'] = array(
        'label'         => '',
        'placeholder'   => esc_html_x( 'Email address *', 'placeholder', 'rogan' ),
        'required'      => true,
        'class'         => array( 'col-lg-6' ),
        'clear'         => true,
        'input_class'   => array( 'single-input-wrapper' )
    );

    return $fields;
});