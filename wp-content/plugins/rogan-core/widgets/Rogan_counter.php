<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_counter extends Widget_Base {

    public function get_name() {
        return 'rogan_counter';
    }

    public function get_title() {
        return __( 'Counter with <br>Call to Action', 'rogan-core' );
    }

    public function get_icon() {
        return ' eicon-counter-circle';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_keywords() {
        return [ 'Count', 'Stats' ];
    }

    public function get_script_depends() {
        return [ 'countto' ];
    }

    protected function _register_controls()
    {

        //******************************* Title Section***************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__('Title', 'rogan-core'),
            ]
        );

        $this->add_control(
            'title_text', [
                'label' => esc_html__('Title', 'rogan-core'),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'We completed 1500+ Projects Yearly <br> Successfully & counting',
            ]
        );

        $this->end_controls_section();


        //----------------------------- Counter Section --------------------------------------//
        $this->start_controls_section(
            'counter_sec',
            [
                'label' => esc_html__('Counter', 'rogan-core'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'count_value', [
                'label' => esc_html__('Count Value', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '16',
            ]
        );

        $repeater->add_control(
            'count_title', [
                'label' => esc_html__('Append Text', 'rogan-core'),
                'description' => esc_html__('You can append here a latter (eg. +, K, M)', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'K',
            ]
        );

        $repeater->add_control(
            'count_label', [
                'label' => esc_html__('Count Label', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Global Customer',
            ]
        );

        $this->add_control(
            'counter_section', [
                'label' => esc_html__('Counter', 'rogan-core'),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ count_value }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $repeater->end_controls_tab();

        $this->end_controls_section();

        //******************************* Call to Action Section ***************************************//
        $this->start_controls_section(
            'call_to_sec',
            [
                'label' => esc_html__('Call to Action', 'rogan-core'),
            ]
        );

        $this->add_control(
            'is_c2a', [
                'label' => esc_html__( 'Call to Action', 'rogan-core' ),
                'description' => esc_html__( 'Show / Hide the Call to Action part.', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'call_to_action_title',
            [
                'label' => esc_html__('Title', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Have any question about us?',
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'call_to_action_subtitle',
            [
                'label' => esc_html__('Subtitle', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Dont’t hesitate to contact us.',
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'btn_label',
            [
                'label' => esc_html__('Button Label', 'rogan-core'),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Contact us',
                'condition' => [
                    'is_c2a' => 'yes'
                ],
            ]
        );

        $this->add_control(
            'btn_url',
            [
                'label' => esc_html__('Button URL', 'rogan-core'),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#'
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        //---------------------------- Normal and Hover ---------------------------//
        $this->start_controls_tabs(
            'style_tabs'
        );


        // Normal Color
        $this->start_controls_tab(
            'normal_btn_style',
            [
                'label' => __( 'Normal', 'saasland-core' ),
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'normal_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one' => 'background: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'normal_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one' => 'border-color: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->end_controls_tab();


        // Hover Color
        $this->start_controls_tab(
            'hover_btn_style',
            [
                'label' => __( 'Hover', 'saasland-core' ),
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'hover_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one:hover' => 'color: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'hover_bg_color', [
                'label' => __( 'Background Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one:hover' => 'background: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'hover_border_color', [
                'label' => __( 'Border Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .solid-button-one:hover' => 'border-color: {{VALUE}}',
                ],
                'condition' => [
                    'is_c2a' => 'yes'
                ]
            ]
        );

        $this->end_controls_tab();

        $this->end_controls_tabs();

        $this->end_controls_section();


        /**
         * Style Tab
         * Theme Counter Style
         */
        /****************************** Section Title Color **************************/
        $this->start_controls_section(
            'section_title_color',
            [
                'label' => esc_html__('Section Title Color', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'title_color',
            [
                'label' => esc_html__('Title Color', 'rogan-core'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'content_typography',
                'label' => esc_html__('Typography', 'rogan-core'),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * Theme Counter Style
         */
        /****************************** Theme Counter **************************/
        $this->start_controls_section(
            'theme_counter_sec',
            [
                'label' => esc_html__('Counter Style', 'rogan-core'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_count_value', [
                'label' => esc_html__('Text Color', 'rogan-core'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-counter-section .counter-wrapper .single-counter-box .number' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_count_value',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .agn-counter-section .counter-wrapper .single-counter-box .number',
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * Background Shape
         */
        /****************************** Section Style **************************/
        $this->start_controls_section(
            'bg_shape_images_sec',
            [
                'label' => esc_html__( 'Shape Images', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'shape1',
            [
                'label' => esc_html__('Shape One', 'rogan-core'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-55.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2',
            [
                'label' => esc_html__('Shape Two', 'rogan-core'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-59.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3',
            [
                'label' => esc_html__('Shape Three', 'rogan-core'),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-61.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        //--------------------------------------------- Section Background -----------------------------------------//
        $this->start_controls_section(
            'section_background_sec',
            [
                'label' => esc_html__( 'Section Background', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_img',
            [
                'label' => esc_html__('Background Image', 'rogan-core'),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_responsive_control(
            'sec_padding',
            [
                'label' => __( 'Padding', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-counter-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'sec_margin',
            [
                'label' => __( 'Margin', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', 'em', '%' ],
                'devices' => [ 'desktop', 'tablet', 'mobile' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-counter-section' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $settings = $this->get_settings();
        $counters = $settings['counter_section'];

        ?>
        <div class="agn-counter-section">
            <?php if (!empty($settings['shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="shape-one">
            <?php endif; ?>
            <?php if (!empty($settings['shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="shape-two">
            <?php endif; ?>
            <?php if (!empty($settings['shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="<?php echo esc_attr($settings['title_text']) ?>" class="shape-three">
            <?php endif; ?>
            <div class="container">
                <div class="main-wrapper" style="background: url(<?php echo esc_url($settings['bg_img']['url']) ?>)
                        no-repeat bottom center;">
                    <div class="theme-title-one text-center">
                        <?php if ( !empty($settings['title_text']) ) : ?>
                            <h2 class="main-title"> <?php echo wp_kses_post(nl2br($settings['title_text'])) ?> </h2>
                        <?php endif; ?>
                    </div> <!-- /.theme-title-one -->
                    <div class="counter-wrapper">
                        <div class="row">
                            <?php
                            if (!empty($counters)) {
                            foreach ($counters as $counter) {
                                ?>
                                <div class="col-sm-4 elementor-repeater-item-<?php echo $counter['_id'] ?>">
                                    <div class="single-counter-box">
                                        <?php if (!empty($counter['count_title'])) : ?>
                                            <h2 class="number"><span class="timer" data-from="0" data-to="<?php echo esc_attr($counter['count_value']) ?>" data-speed="1200" data-refresh-interval="5">
                                                    <?php echo esc_html($counter['count_value']);?></span><?php echo wp_kses_post($counter['count_title']) ?>
                                            </h2>
                                        <?php endif; ?>
                                        <?php if (!empty($counter['count_label'])) : ?>
                                            <p> <?php echo esc_html($counter['count_label']) ?> </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php
                            }}
                            ?>
                        </div>
                    </div> <!-- /.counter-wrapper -->
                </div> <!-- /.main-wrapper -->

                <?php if( $settings['is_c2a'] == 'yes' ) : ?>
                    <div class="bottom-banner">
                        <div class="clearfix">
                            <div class="text">
                                <?php if (!empty($settings['call_to_action_title'])) : ?>
                                    <h3 class="title"><?php echo esc_html(nl2br($settings['call_to_action_title'])) ?></h3>
                                <?php endif; ?>
                                <?php if (!empty($settings['call_to_action_subtitle'])) : ?>
                                    <?php echo wpautop($settings['call_to_action_subtitle']) ?>
                                <?php endif; ?>
                            </div>
                            <?php if (!empty($settings['btn_label'])) : ?>
                                <a href="<?php echo esc_url($settings['btn_url']['url']) ?>"
                                    class="contact solid-button-one"
                                    <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                                    <?php echo esc_html($settings['btn_label']) ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div> <!-- /.container -->
        </div>
        <?php
    }
}