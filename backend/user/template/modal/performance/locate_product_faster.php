<!-- Modal -->
<div class="modal fade" id="modalbox_performance_4" tabindex="-1" role="dialog" aria-labelledby="Locate Product Faster" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Locate Product Faster</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Locate Product Faster</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="locate_product_faster" id="locate_product_faster" class="form-control" multiple>
                            <option id="Intutive Navigation">Intutive Navigation</option>
                            <option id="Recommended Searches">Recommended Searches</option>
                            <option id="Similar products">Similar products</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>