
<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

if (has_post_thumbnail()) {
    ?>
    <div class="img-holder"> <?php the_post_thumbnail( 'full' ) ?> </div>
    <?php
}
?>

<div class="post-data">
    <?php
    the_content();
    wp_link_pages( array (
        'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'rogan' ) . '</span>',
        'after'       => '</div>',
        'link_before' => '<span>',
        'link_after'  => '</span>',
        'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'rogan' ) . ' </span>%',
        'separator'   => '<span class="screen-reader-text">, </span>',
    ));
    ?>
</div> <!-- /.post-data -->

<div class="post-tag-area d-md-flex justify-content-between align-items-center mb-80">

    <?php
    the_tags( '<ul class="tags"> <li class="tag-title">'.esc_html__( 'Tags: ', 'rogan' ).'</li> <li>', ', </li> <li>', '</li></ul>' );
    ?>

    <?php
    if(function_exists( 'rogan_social_share' )) {
        rogan_social_share();
    }
    ?>

</div>