<?php
/**
 * Template Name: Portfolio Creative
 */

$opt = get_option( 'rogan_opt' );
$portfolio_show_count = !empty($opt['portfolio_show_count']) ? $opt['portfolio_show_count'] : -1;
$portfolios = new WP_Query(array(
    'post_type'     => 'portfolio',
    'posts_per_page'=> $portfolio_show_count,
));
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="js">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- For Responsive Device -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php wp_head(); ?>
    </head>
    <body <?php body_class( 'project-creative' ); ?>>
        <main>
            <?php if( wp_get_referer() ) : ?>
                <a href="<?php echo wp_get_referer() ?>" class="back-button white-shdw-button">
                    <i class="icon flaticon-back"></i>
                    <?php esc_html_e( 'Go Back', 'rogan' ); ?>
                </a>
            <?php endif; ?>
            <div class="slideshow">
                <div class="slideshow__deco"></div>
                <?php
                unset($i);
                $i = 1;
                while ($portfolios->have_posts()) : $portfolios->the_post();
                    $title = wp_trim_words(get_the_title(), 2, '' );
                    ?>
                    <div class="slide" title="<?php the_title_attribute() ?>">
                        <div class="slide__img-wrap">
                            <div class="slide__img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                        </div>
                        <div class="slide__side"> <?php rogan_first_category( 'portfolio_cat' ) ?> </div>
                        <div class="slide__title-wrap">
                            <span class="slide__number"> <?php echo esc_html($i); ?> </span>
                            <h3 class="slide__title" title="<?php the_title_attribute() ?>">
                                <?php echo esc_html($title) ?>
                            </h3>
                            <h4 class="slide__subtitle">
                                <?php
                                $key_i = 0;
                                while ( have_rows( 'key_features' ) ) : the_row();
                                    if ($key_i == 1) {
                                        break;
                                    }
                                    echo get_sub_field( 'feature_title' );
                                    ++$key_i;
                                endwhile;
                                ?>
                            </h4>
                        </div>
                    </div>
                    <?php
                    ++$i;
                endwhile;
                wp_reset_postdata();
                ?>
                <button class="nav nav--prev">
                    <span class="icon icon--navarrow-prev">
                        <span class="flaticon-next"></span>
                    </span>
                </button>
                <button class="nav nav--next">
                    <span class="icon icon--navarrow-next">
                        <span class="flaticon-next"></span>
                    </span>
                </button>
            </div>
            <div class="content">
                <?php
                unset($i);
                $i = 1;
                while ($portfolios->have_posts()) : $portfolios->the_post();
                    $title = wp_trim_words(get_the_title(), 2, '' );
                    ?>
                    <div class="content__item">
                        <span class="content__number"> <?php echo esc_html($i); ?> </span>
                        <h3 class="content__title" title="<?php the_title_attribute() ?>"> <?php echo esc_html($title); ?> </h3>
                        <h4 class="content__subtitle">
                            <?php
                            unset($key_i);
                            $key_i = 0;
                            while ( have_rows( 'key_features' ) ) : the_row();
                                if ($key_i == 1) {
                                    break;
                                }
                                echo get_sub_field( 'feature_title' );
                                ++$key_i;
                            endwhile;
                            ?>
                        </h4>
                        <div class="content__text"> <?php the_content(); ?> </div>
                    </div>
                    <?php
                    ++$i;
                endwhile; wp_reset_postdata();
                ?>
                <button class="content__close">
                    <span class="icon icon--longarrow">
                        <span class="flaticon-back"></span>
                    </span>
                </button>
            </div>
        </main>

    <?php wp_footer(); ?>
    </body>
</html>