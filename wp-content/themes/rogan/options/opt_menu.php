<?php

Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Menu Settings', 'rogan' ),
    'id'               => 'menu_opt',
    'icon'             => 'el el-lines',
    'fields'           => array(

        array(
            'id'            => 'menu_typo',
            'type'          => 'typography',
            'title'         => esc_html__( 'Menu Typography', 'rogan' ),
            'subtitle'      => esc_html__( 'Menu item typography options', 'rogan' ),
            'color'         => false,
            'output'        => '.theme-menu-one.d-align-item.color-white .navbar-nav .nav-item .nav-link'
        ),

        array(
            'title'     => esc_html__( 'Menu Item Color', 'rogan' ),
            'subtitle'  => esc_html__( 'Menu item font color', 'rogan' ),
            'id'        => 'menu_font_color',
            'type'      => 'link_color',
        ),

        array(
            'title'     => esc_html__( 'Menu item margin', 'rogan' ),
            'subtitle'  => esc_html__( 'Margin around menu item.', 'rogan' ),
            'id'        => 'menu_item_margin',
            'type'      => 'spacing',
            'output'    => array( '.navbar-nav .nav-item .nav-link' ),
            'mode'      => 'margin',
            'units'     => array( 'em', 'px', '%' ),      // You can specify a unit value. Possible: px, em, %
            'units_extended' => 'true',
        ),

        array (
            'title'     => esc_html__( 'Menu background color', 'rogan' ),
            'id'        => 'menu_bg_color',
            'output'    => '.header_area',
            'type'      => 'color',
            'mode'      => 'background',
        ),

        // Sticky menu settings section
        array(
            'id' => 'sticky_menu_start',
            'type' => 'section',
            'title' => esc_html__( 'Sticky menu settings', 'rogan' ),
            'subtitle' => esc_html__( 'The following settings will only apply on the sticky header mode.', 'rogan' ),
            'indent' => true
        ),

        array(
            'title'     => esc_html__( 'Menu Item Color', 'rogan' ),
            'subtitle'  => esc_html__( 'Menu item font color', 'rogan' ),
            'id'        => 'sticky_menu_font_color',
            'type'      => 'link_color',
        ),

        array (
            'title'     => esc_html__( 'Background Color', 'rogan' ),
            'subtitle'     => esc_html__( 'Sticky menu background color', 'rogan' ),
            'id'        => 'sticky_menu_bg_color',
            'output'    => 'header.header_area.navbar_fixed',
            'type'      => 'color',
            'mode'      => 'background',
        ),

        array(
            'id'     => 'sticky_menu_end',
            'type'   => 'section',
            'indent' => false,
        ),
    )
));