<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_service_agency extends Widget_Base {

    public function get_name() {
        return 'rogan_service_agency';
    }

    public function get_title() {
        return esc_html__( 'Service (Agency)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-posts-grid';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ***************************** Service Title **************************************//
        $this->start_controls_section(
            'title_sec', [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'What We Do',
            ]
        );


        $this->add_control(
            'main_title',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Provide awesome customer service <br>  with our tools.',
            ]
        );

        $this->end_controls_section();


        // *************************************** Service List *************************************//
        $this->start_controls_section(
            'content_section',
            [
                'label' => esc_html__( 'Service List', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true
            ]
        );

        $repeater->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' =>Controls_Manager::TEXTAREA,
            ]
        );

        $repeater->add_control(
            'icon_img',
            [
                'label' => esc_html__( 'Icon Image', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/icon/icon1.svg', __FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'bg_icon',
            [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-2.svg', __FILE__)
                ]
            ]
        );

        $repeater->add_control(
            'btn_label',
            [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' =>Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More About Strategy & Research'
            ]
        );

        $repeater->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' =>Controls_Manager::URL,
                'default' => [
                    'url'   => '#'
                ]
            ]
        );

        $repeater->add_control(
            'normal_text_color', [
                'label' => __( 'Text Color', 'saasland-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-what-we-do .single-block .more-button' => 'color: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'normal_btn_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .agn-what-we-do .single-block .more-button',
            ]
        );

        $this->add_control(
            'service_list',
            [
                'label' => esc_html__( 'Services', 'rogan-core' ),
                'type' =>Controls_Manager::REPEATER,
                'fields' => $repeater->get_controls(),
                'title_field' => '{{{title}}}'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @Title
         */
        /********************************* Upper Title ********************************/
        $this->start_controls_section(
            'style_sec_upper_title', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .upper-title',
            ]
        );

        $this->end_controls_section(); // End Upper Title

        //----------------------------------- Style Title ----------------------------------------------//
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        /********************************* Service List Style ********************************/
        $this->start_controls_section(
            'service_item_title_sec', [
                'label' => esc_html__( 'Service Item Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'service_item_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-what-we-do .single-block .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'setvice_item_title_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .agn-what-we-do .single-block .title',
            ]
        );

        $this->end_controls_section();

        /********************************* Service List Style ********************************/
        $this->start_controls_section(
            'service_item_subtitle_sec', [
                'label' => esc_html__( 'Service Item Description', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'service_item_subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-what-we-do .single-block p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'service_item_subtitle_typography',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .agn-what-we-do .single-block p',
            ]
        );

        $this->end_controls_section();


        // ************************************** Background shapes  *************************************//
        $this->start_controls_section(
            'style_bg_shapes',
            [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab'   => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-63.svg', __FILE__)
                ]
            ]
        );

        $this->add_control(
            'bg_shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' =>Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-56.svg', __FILE__)
                ]
            ]
        );

        $this->end_controls_section(); // End Background Shapes


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-what-we-do' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $services = $settings['service_list'];
        ?>
        <div class="agn-what-we-do pt-150 pb-200">
            <?php if (!empty($settings['bg_shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['bg_shape1']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-one" data-aos="fade-right">
            <?php endif; ?>
            <?php if (!empty($settings['bg_shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['bg_shape2']['url']) ?>" alt="<?php echo esc_attr($settings['upper_title']) ?>" class="shape-two">
            <?php endif; ?>
            <div class="container">
                <div class="theme-title-one text-center">
                    <?php if (!empty($settings['upper_title'])) : ?>
                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                    <?php endif; ?>
                    <?php if (!empty($settings['main_title'])) : ?>
                        <h2 class="main-title"><?php echo wp_kses_post(nl2br($settings['main_title'])) ?></h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
                <div class="row">
                    <?php
                    if (!empty($services)) {
                    foreach ($services as $service) {
                        ?>
                        <div class="col-lg-4 col-md-6">
                            <div class="single-block">
                                <div class="icon">
                                    <?php if (!empty($service['bg_icon']['url'])) : ?>
                                        <img src="<?php echo esc_url($service['bg_icon']['url']) ?>" alt="<?php echo esc_attr($service['title']) ?>" class="bg">
                                    <?php endif; ?>
                                    <?php if (!empty($service['icon_img']['url'])) : ?>
                                        <img src="<?php echo esc_url($service['icon_img']['url']) ?>" alt="<?php echo esc_attr($service['title']) ?>" class="shape" data-aos="zoom-in-down">
                                    <?php endif; ?>
                                </div>
                                <?php if (!empty($service['title'])) : ?>
                                    <h5 class="title"><?php echo esc_html($service['title']) ?></h5>
                                <?php endif; ?>
                                <?php if(!empty($service['subtitle'])) : ?>
                                    <?php echo wp_kses_post(wpautop($service['subtitle'])) ?>
                                <?php endif; ?>
                                <?php if (!empty($service['btn_label'])) : ?>
                                    <a href="<?php echo esc_url($service['btn_url']['url']) ?>" class="more-button"
                                        <?php rogan_is_external($service['btn_url']); rogan_is_nofollow($service['btn_url']) ?>>
                                        <?php echo esc_html($service['btn_label']) ?>
                                    </a>
                                <?php endif; ?>
                            </div> <!-- /.single-block -->
                        </div>
                        <?php
                    }}
                    ?>
                </div>
            </div> <!-- /.container -->
        </div>
        <?php
    }
}