<?php
$opt = get_option( 'rogan_opt' );
$main_logo = isset($opt['main_logo'] ['url']) ? $opt['main_logo'] ['url'] : '';
// Button Settings
$is_menu_btn = !empty($opt['is_menu_btn']) ? $opt['is_menu_btn'] : '';
$menu_btn_title = !empty($opt['menu_btn_label']) ? $opt['menu_btn_label'] : '';
$menu_btn_url = !empty($opt['menu_btn_url']) ? $opt['menu_btn_url'] : '';
?>
<div class="theme-main-menu theme-menu-one d-align-item">
    <div class="logo">
        <?php rogan_logo() ?>
    </div>
    <nav id="mega-menu-holder" class="navbar navbar-expand-lg">
        <div  class="position-relative ml-auto nav-container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="flaticon-setup"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <?php
                if( has_nav_menu( 'main_menu' ) ) {
                    wp_nav_menu( array(
                        'menu' => 'main_menu',
                        'theme_location' => 'main_menu',
                        'container' => false,
                        'menu_class' => 'navbar-nav',
                        'walker' => new Rogan_Nav_Navwalker(),
                        'depth' => 3
                    ));
                }
                if ( !empty($menu_btn_title) && $is_menu_btn == '1' ) : ?>
                    <a href="<?php echo esc_url($menu_btn_url) ?>" class="menu_action_btn header4_btn">
                        <?php echo esc_html($menu_btn_title); ?>
                    </a>
                    <?php
                endif;
                ?>
            </div>
        </div> <!-- /.container -->
    </nav> <!-- /#mega-menu-holder -->

    <?php if ( !empty($opt['header_content']) ) : ?>
        <div class="header-right-widget">
            <ul>
                <li class="call-us"> <?php echo !empty($opt['header_content']) ? wp_kses_post($opt['header_content']) : ''; ?> </li>
            </ul>
        </div> <!-- /.header-right-widget -->
    <?php endif; ?>

</div>