<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;
use Elementor\Group_Control_Box_Shadow;




// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_agency extends Widget_Base {

    public function get_name() {
        return 'rogan_hero_agency';
    }

    public function get_title() {
        return esc_html__( 'Hero Agency', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Slogan Text ------------------------------
        $this->start_controls_section(
            'slogan_text_sec',
            [
                'label' => esc_html__( 'Slogan', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'slogan_text',
            [
                'label' => esc_html__( 'Slogan Text', 'rogan-core' ),
                'description' => esc_html__( 'Wrap up the colored ', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => '<span>70% Off</span> for first 3 months',
            ]
        );

        $this->end_controls_section();

        // ----------------------------------------  Hero content ------------------------------
        $this->start_controls_section(
            'hero_content',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Digital Agency <br> with Excellence <br> Services.'
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section(); // End Hero content


        //*****************************  First Featured image *******************************************//
        $this->start_controls_section(
            'agency_fimg1_sec',
            [
                'label' => esc_html__( 'Featured Images', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'agency_fimg1', [
                'label' => esc_html__( 'First Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/banner-shape1.svg', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position1',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position1',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration .fimage-shape' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        //*****************************  Second Featured image *******************************************//
        $this->start_controls_section(
            'agency_fimg2_sec',
            [
                'label' => esc_html__( 'Second Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'agency_fimg2', [
                'label' => esc_html__( 'Second Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/banner-shape2.svg', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position2',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration .shape-one' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position2',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration .shape-one' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        //*****************************  Third Featured image *******************************************//
        $this->start_controls_section(
            'agency_fimg3_sec',
            [
                'label' => esc_html__( 'Third Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'agency_fimg3', [
                'label' => esc_html__( 'Third Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/banner-shape3.svg', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'horizontal_position3',
            [
                'label' => __( 'Horizontal', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration .shape-two' => 'left: {{SIZE}}{{UNIT}}; right: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'vertical_position3',
            [
                'label' => __( 'Vertical', 'rogan-core' ),
                'type' => Controls_Manager::SLIDER,
                'size_units' => [ 'px', '%' ],
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                    ],
                    '%' => [
                        'min' => -100,
                        'max' => 100,
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration .shape-two' => 'top: {{SIZE}}{{UNIT}}; bottom: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();


        //*****************************  Fourth Featured image *******************************************//
        $this->start_controls_section(
            'agency_bg_fimg4_sec',
            [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'agency_bg_fimg4', [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/1.png', __FILE__)
                ]
            ]
        );

        $this->end_controls_section();

        /// ---------------------------------------   Buttons ----------------------------------///
        $this->start_controls_section(
            'buttons_sec',
            [
                'label' => esc_html__( 'Buttons', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'btn_title', [
                'label' => esc_html__( 'Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'More About us'
            ]
        );

        $repeater->add_control(
            'btn_icon', [
                'label' => esc_html__( 'Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
            ]
        );

        $repeater->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->start_controls_tabs(
            'style_tabs'
        );

        /// Normal Button Style
        $repeater->start_controls_tab(
            'style_normal_btn',
            [
                'label' => esc_html__( 'Normal', 'rogan-core' ),
            ]
        );

        $repeater->add_control(
            'font_color', [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'color: {{VALUE}}',
                ],

            ]
        );

        $repeater->add_control(
            'bg_color',
            [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'background: {{VALUE}}',
                ],
            ]
        );

        $repeater->add_control(
            'border_color',
            [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'border-color: {{VALUE}}',
                ],
            ]
        );

        $repeater->end_controls_tab();

        /// Hover Button Style
        $repeater->start_controls_tab(
            'style_hover_btn',
            [
                'label' => esc_html__( 'Hover', 'rogan-core' ),
            ]
        );

        $repeater->add_control(
            'hover_font_color', [
                'label' => esc_html__( 'Font Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'color: {{VALUE}}',
                )
            ]
        );

        $repeater->add_control(
            'hover_bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'background: {{VALUE}}',
                )
            ]
        );

        $repeater->add_control(
            'hover_border_color', [
                'label' => esc_html__( 'Border Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} {{CURRENT_ITEM}}:hover' => 'border-color: {{VALUE}}',
                )
            ]
        );

        $repeater->end_controls_tab();
        $repeater->end_controls_tabs();

        $repeater->add_control(
            'hr_btn_divider',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );

        $repeater->add_responsive_control(
            'btn_padding',
            [
                'label' => __( 'Padding', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $repeater->add_responsive_control(
            'btn_border_radius',
            [
                'label' => __( 'Border Radius', 'rogan-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}}' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'btn_box_shadow',
                'label' => __( 'Box Shadow', 'rogan-core' ),
                'selector' => '{{WRAPPER}} {{CURRENT_ITEM}}',
            ]
        );

        // Buttons repeater field
        $this->add_control(
            'buttons', [
                'label' => esc_html__( 'Create buttons', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ btn_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        // Play button
        $this->add_control(
            'is_play_btn',
            [
                'label' => esc_html__( 'Play Button', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Yes', 'rogan-core' ),
                'label_off' => esc_html__( 'No', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'play_btn_title', [
                'label' => esc_html__( 'Play Button Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => 'Watch Video',
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_url', [
                'label' => esc_html__( 'Video URL', 'rogan-core' ),
                'description' => esc_html__( 'Enter here a YouTube video URL', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_btn_color', [
                'label' => esc_html__( 'Button Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .video-button-one' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .video-button-one i' => 'color: {{VALUE}};',
                ],
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->add_control(
            'play_btn_hover_color', [
                'label' => esc_html__( 'Hover Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .main-wrapper a.icon_class:hover' => 'color: {{VALUE}} !important;',
                    '{{WRAPPER}} .main-wrapper a.icon_class:hover i' => 'color: {{VALUE}} !important;',
                ],
                'condition' => [
                    'is_play_btn' => 'yes'
                ]
            ]
        );

        $this->end_controls_section(); // End Buttons



        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'position-opt', [
                'label' => esc_html__( 'Custom Position Images', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        // Positioning
        $start = is_rtl() ? __( 'Right', 'elementor' ) : __( 'Left', 'elementor' );
        $end = ! is_rtl() ? __( 'Right', 'elementor' ) : __( 'Left', 'elementor' );
        $this->add_control (
            'fi1_offset_orientation_h',
            [
                'label' => __( 'Horizontal Orientation', 'elementor' ),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'toggle' => false,
                'default' => 'start',
                'options' => [
                    'start' => [
                        'title' => $start,
                        'icon' => 'eicon-h-align-left',
                    ],
                    'end' => [
                        'title' => $end,
                        'icon' => 'eicon-h-align-right',
                    ],
                ],
                'classes' => 'elementor-control-start-end',
                'render_type' => 'ui',
            ]
        );

        $this->add_responsive_control(
            'fi1_offset_x',
            [
                'label' => __( 'Offset', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vw' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vh' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                ],
                'size_units' => [ 'px', '%', 'vw', 'vh' ],
                'selectors' => [
                    'body:not(.rtl) {{WRAPPER}} #theme-banner-one .illustration' => 'left: {{SIZE}}{{UNIT}}',
                    'body.rtl {{WRAPPER}} #theme-banner-one .illustration' => 'right: {{SIZE}}{{UNIT}}',
                ],
                'condition' => [
                    'fi1_offset_orientation_h!' => 'end',
                ],
            ]
        );

        $this->add_responsive_control(
            'fi1_offset_x_end',
            [
                'label' => __( 'Offset', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 0.1,
                    ],
                    '%' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vw' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vh' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                ],
                'size_units' => [ 'px', '%', 'vw', 'vh' ],
                'selectors' => [
                    'body:not(.rtl) {{WRAPPER}} #theme-banner-one .illustration' => 'right: {{SIZE}}{{UNIT}}',
                    'body.rtl {{WRAPPER}} #theme-banner-one .illustration' => 'left: {{SIZE}}{{UNIT}}',
                ],
                'condition' => [
                    'fi1_offset_orientation_h' => 'end',
                ],
            ]
        );

        $this->add_control(
            'fi1_offset_orientation_v',
            [
                'label' => __( 'Vertical Orientation', 'elementor' ),
                'type' => Controls_Manager::CHOOSE,
                'label_block' => false,
                'toggle' => false,
                'default' => 'start',
                'options' => [
                    'start' => [
                        'title' => __( 'Top', 'elementor' ),
                        'icon' => 'eicon-v-align-top',
                    ],
                    'end' => [
                        'title' => __( 'Bottom', 'elementor' ),
                        'icon' => 'eicon-v-align-bottom',
                    ],
                ],
                'render_type' => 'ui',
            ]
        );

        $this->add_responsive_control(
            'fi1_offset_y',
            [
                'label' => __( 'Offset', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vh' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vw' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                ],
                'size_units' => [ 'px', '%', 'vh', 'vw' ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration' => 'top: {{SIZE}}{{UNIT}}',
                ],
                'condition' => [
                    'fi1_offset_orientation_v!' => 'end',
                ],
            ]
        );

        $this->add_responsive_control(
            'fi1_offset_y_end',
            [
                'label' => __( 'Offset', 'elementor' ),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => -1000,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vh' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                    'vw' => [
                        'min' => -200,
                        'max' => 200,
                    ],
                ],
                'size_units' => [ 'px', '%', 'vh', 'vw' ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .illustration' => 'bottom: {{SIZE}}{{UNIT}}',
                ],
                'condition' => [
                    'fi1_offset_orientation_v' => 'end',
                ],
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_prefix', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .main-wrapper .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_prefix',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-one .main-wrapper .main-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), [
                'name' => 'text_shadow_prefix',
                'selector' => '
                    {{WRAPPER}} #theme-banner-one .main-wrapper .main-title',
            ]
        );

        $this->end_controls_section();



        //------------------------------ Style Subtitle ------------------------------
        $this->start_controls_section(
            'style_subtitle', [
                'label' => esc_html__( 'Style Subtitle', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_subtitle', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .main-wrapper .sub-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_subtitle',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-one .main-wrapper .sub-title',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Agency Style Section ------------------------------
        $this->start_controls_section(
            'agency_bg_shapes', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'agency_shape1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/oval-1.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape2',
            [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-1.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape3',
            [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-55.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape4',
            [
                'label' => esc_html__( 'Shape four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-56.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape5',
            [
                'label' => esc_html__( 'Shape Five', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-57.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape6',
            [
                'label' => esc_html__( 'Shape Six', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-58.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape7',
            [
                'label' => esc_html__( 'Shape Seven', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-59.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape8',
            [
                'label' => esc_html__( 'Shape Eight', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-60.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape9',
            [
                'label' => esc_html__( 'Shape Nine', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-61.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'agency_shape10',
            [
                'label' => esc_html__( 'Shape Ten', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-62.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'sec_bg_img', [
                'label' => esc_html__( 'Background Shape', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/1.png', __FILE__)
                ]
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-one .main-wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();
        $buttons = $settings['buttons'];
        ?>
        <div id="theme-banner-one" style="background: url(<?php echo esc_url($settings['sec_bg_img']['url']) ?>)
            no-repeat top right;">
            <div class="illustration wow zoomIn animated" data-wow-delay="1s" data-wow-duration="2s">
                <?php if (!empty($settings['agency_fimg1']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['agency_fimg1']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="fimage-shape">
                <?php endif; ?>
                <?php if (!empty($settings['agency_fimg2']['url'])) : ?>
                    <img src="<?php echo esc_html($settings['agency_fimg2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-one wow fadeInDown animated" data-wow-delay="1.8s">
                <?php endif; ?>
                <?php if (!empty($settings['agency_fimg1']['url'])) : ?>
                    <img src="<?php echo esc_html($settings['agency_fimg3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-two wow fadeInUp animated" data-wow-delay="2.7s">
                <?php endif; ?>
            </div>

            <?php if (!empty($settings['agency_shape1']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape1']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="oval-one">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape2']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape2']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-three">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape3']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape3']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-four">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape4']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape4']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-five">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape5']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape5']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-six">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape6']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape6']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-seven">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape7']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape7']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-eight">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape8']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape8']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-nine">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape9']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape9']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-ten">
            <?php endif; ?>
            <?php if (!empty($settings['agency_shape10']['url'])) : ?>
                <img src="<?php echo esc_url($settings['agency_shape10']['url']) ?>" alt="<?php echo esc_attr(strip_tags($settings['title'])) ?>" class="shape-eleven">
            <?php endif; ?>
            <div class="container">
                <div class="main-wrapper">
                    <?php if(!empty($settings['slogan_text'])) : ?>
                        <div class="slogan wow fadeInDown animated" data-wow-delay="0.2s">
                            <?php echo wp_kses_post($settings['slogan_text']) ?>
                        </div>
                    <?php endif; ?>
                    <?php if(!empty($settings['title'])) : ?>
                        <h1 class="main-title wow fadeInUp animated" data-wow-delay="0.4s">
                            <?php echo wp_kses_post(nl2br($settings['title'])); ?>
                        </h1>
                    <?php endif; ?>
                    <?php if(!empty($settings['subtitle'])) : ?>
                        <p class="sub-title wow fadeInUp animated" data-wow-delay="0.9s">
                            <?php echo wp_kses_post(nl2br($settings['subtitle'])); ?>
                        </p>
                    <?php endif; ?>
                    <ul class="button-group lightgallery">
                        <?php
                        $i = 0;
                        if (!empty($buttons)) {
                            foreach ($buttons as $button) {
                                ++$i;
                                ?>
                                <?php if(!empty($button['btn_title'])) : ?>
                                    <li>
                                        <a href="<?php echo esc_url($button['btn_url']['url']) ?>" class="more-button solid-button-one wow fadeInLeft animated elementor-repeater-item-<?php echo $button['_id'] ?>" data-wow-delay="1.5s"
                                            <?php rogan_is_external($button['btn_url']); rogan_is_nofollow($button['btn_url']) ?>>
                                            <?php echo esc_html($button['btn_title']) ?>
                                            <i class="<?php echo esc_attr( $button['btn_icon'] ) ?> icon-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                <?php endif; ?>
                                <?php
                            }}
                        if($settings['is_play_btn'] == 'yes') : ?>
                            <li>
                                <a data-fancybox href="<?php echo esc_url($settings['play_url']) ?>" class="icon_class fancybox video-button-one wow fadeInRight animated" data-wow-delay="1.5s">
                                    <?php echo esc_html($settings['play_btn_title']) ?>
                                    <i class="flaticon-play-button icon-right"></i>
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div> <!-- /.main-wrapper -->
            </div> <!-- /.container -->
        </div> <!-- /#theme-banner-one -->
        <?php
    }
}
