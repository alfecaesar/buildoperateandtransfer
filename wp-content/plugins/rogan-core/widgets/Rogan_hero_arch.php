<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_hero_arch extends Widget_Base {

    public function get_name() {
        return 'rogan_hero_arch';
    }

    public function get_title() {
        return esc_html__( 'Hero Architecture', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_script_depends() {
        return [ 'iziModal' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {


        /// --------------------------------------- Hero Section ----------------------------------///
        $this->start_controls_section(
            'hero_contents',
            [
                'label' => esc_html__( 'Hero Contents', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();
        $repeater->add_control(
            'main_title', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'default' => 'Interior & Exterior <br> Solution.'
            ]
        );

        $repeater->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'flaticon',
            [
                'label'      => esc_html__( 'Icon', 'rogan-core' ),
                'type'       => Controls_Manager::ICON,
                'options'    => rogan_flat_icons(),
                'include'    => rogan_include_flaticon_icons(),
                'default'    => 'flaticon-next',
            ]
        );

        $repeater->add_control(
            'icon_link',
            [
                'label' => esc_html__( 'Link', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'navigation_bullet',
            [
                'label' => esc_html__( 'Navigation Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '01 Blue.'
            ]
        );

        $repeater->add_control(
            'tab_no', [
                'label' => esc_html__( 'Tab Number', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => '01'
            ]
        );

        $repeater->add_control(
            'tab_title', [
                'label' => esc_html__( 'Tab Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Blue Sea Resort'
            ]
        );

        $repeater->add_control(
            'tab_subtitle', [
                'label' => esc_html__( 'Tab Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Blue lbl inc.'
            ]
        );

        $repeater->add_control(
            'plus_mark', [
                'label' => esc_html__( 'Plus Icon', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '+'
            ]
        );

        $repeater->add_control(
            'slider_img', [
                'label' => esc_html__( 'Slider Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                        'url' => '#'
                ]
            ]
        );

        $repeater->add_control(
            'hr',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );

        $repeater->add_control(
            'hero_content', [
                'label' => esc_html__( 'Hero Contents', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        // Buttons repeater field
        $this->add_control(
            'architecture',
            [
                'label' => esc_html__( 'Hero Title', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ main_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section(); //



        /**
         * Style Tab
         * ------------------------------ Style Title ------------------------------
         */
        $this->start_controls_section(
            'style_title', [
                'label' => esc_html__( 'Style Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_title', [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} #theme-banner-four .main-wrapper .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} #theme-banner-four .main-wrapper .main-title',
            ]
        );

        $this->add_group_control(
            Group_Control_Text_Shadow::get_type(), [
                'name' => 'text_shadow_prefix',
                'selector' => '
                    {{WRAPPER}} #theme-banner-four .main-wrapper .main-title',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings();
        $architectures = $settings['architecture'];
        ?>
        <div class="arch-main-banner">
            <div class="main-carousel-wrapper">
                <div id="arch-carousel" class="carousel slide" data-ride="carousel" data-interval="5000">
                    <div class="carousel-inner">
                        <?php
                        $i = 0;
                        if ( !empty($architectures) ) {
                        foreach ( $architectures as $index => $architecture ) {
                            $slider_image = isset($architecture['slider_img']['id']) ? $architecture['slider_img']['id'] : '';
                            $slider_image = wp_get_attachment_image_src($slider_image, 'full');
                            ?>
                            <div class="carousel-item<?php echo esc_attr($index == 0) ? ' active' : ''; ?>" style="background-image: url(<?php echo $slider_image[0] ?>);
                                    background-size: cover;
                                    background-repeat: no-repeat;
                                    background-position: top center;">
                                <div class="inner-container">
                                    <div class="inner-item-wrapper">
                                        <div class="shape" data-animation="animated zoomIn"></div>
                                        <?php if ( !empty($architecture['upper_title']) ) : ?>
                                            <p class="font-lato upper-title" data-animation="animated fadeInDown"><?php echo esc_html( $architecture['upper_title'] ) ?></p>
                                        <?php endif; ?>
                                        <?php if ( !empty($architecture['main_title']) ) : ?>
                                            <h2 class="main-title" data-animation="animated fadeInUp"><?php echo wp_kses_post(nl2br( $architecture['main_title']) ) ?></h2>
                                        <?php endif; ?>
                                        <?php if ( !empty($architecture['flaticon'])) : ?>
                                            <a href="<?php echo esc_url($architecture['icon_link']['url']) ?>" class="read-more" data-animation="animated fadeInUp">
                                                <i class="<?php echo esc_attr($architecture['flaticon']) ?>"></i>
                                            </a>
                                        <?php endif; ?>
                                    </div> <!-- /.inner-item-wrapper -->
                                    <button class="details-info-button font-lato" data-animation="animated fadeInDown" data-izimodal-open="#elementor-repeater-item-<?php echo $architecture['_id'] ?>" data-izimodal-transitionout="fadeIn">
                                        <?php echo esc_attr($architecture['plus_mark']) ?>
                                    </button>
                                </div> <!-- /.inner-container -->
                            </div> <!-- /.carousel-item -->
                            <?php
                        }}
                        ?>
                    </div> <!-- /.carousel-inner -->

                    <ol class="carousel-indicators indicators-one">
                        <?php
                        $i = 0;
                        if ( !empty($architectures) ) {
                        foreach ( $architectures as $index => $architecture ) {
                            ?>
                            <?php if ( !empty($architecture['navigation_bullet'])) : ?>
                                <li data-target="#arch-carousel" data-slide-to="<?php echo esc_attr($i) ?>" <?php echo esc_attr($index == 0) ? 'class="active"' : ''; ?>>
                                    <i><?php echo esc_html($architecture['navigation_bullet']) ?></i><span></span>
                                </li>
                            <?php endif; ?>
                            <?php
                            ++$i;
                        }}
                        ?>
                    </ol>

                    <ol class="carousel-indicators indicators-two">
                        <?php
                        $i = 0;
                        if ( !empty($architectures) ) {
                        foreach ( $architectures as $index => $architecture ) {
                            ?>
                            <li data-target="#arch-carousel" data-slide-to="<?php echo esc_attr($i) ?>" <?php echo esc_attr($index == 0) ? 'class="active"' : ''; ?>>
                                <?php if ( !empty($architecture['tab_no'])) : ?>
                                    <div><?php echo esc_html($architecture['tab_no']) ?></div>
                                <?php endif; ?>
                                <?php if ( !empty($architecture['tab_title'])) : ?>
                                    <h6 class="title"><?php echo wp_kses_post($architecture['tab_title']) ?></h6>
                                <?php endif; ?>
                                <?php if ( !empty($architecture['tab_subtitle'])) : ?>
                                    <p> <?php echo wp_kses_post($architecture['tab_subtitle']) ?> </p>
                                <?php endif; ?>
                            </li>
                            <?php
                            ++$i;
                        }}
                        ?>
                    </ol>

                </div>
            </div> <!-- /.main-carousel-wrapper -->
        </div>


        <?php
        $i = 0;
        foreach ( $architectures as $index => $architecture ) {
            ?>
            <div id="elementor-repeater-item-<?php echo $architecture['_id'] ?>" class="iziModal arch-modal" data-izimodal-group="alerts">
                <button data-izimodal-close="" data-izimodal-transitionout="fadeOutDown" class="close-button">
                    <img src="<?php echo plugins_url('images/icon/icon43.svg', __FILE__) ?>" alt="close icon">
                </button>
                <button data-izimodal-next="" class="next"><i class="flaticon-next"></i></button>
                <button data-izimodal-prev="" class="prev"><i class="flaticon-back"></i></button>
                <div class="main-bg-wrapper">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-6 order-lg-last">
                                <?php if ( !empty($architecture['main_title']) ) : ?>
                                    <h3 class="title"><?php echo wp_kses_post(nl2br( $architecture['main_title']) ) ?></h3>
                                <?php endif; ?>
                                <?php if ( !empty($architecture['upper_title']) ) : ?>
                                    <h5 class="font-lato"><?php echo esc_html( $architecture['upper_title'] ) ?></h5>
                                <?php endif; ?>
                                <?php if ( !empty($architecture['hero_content'])) : ?>
                                    <?php echo wp_kses_post($architecture['hero_content']) ?>
                                <?php endif; ?>
                            </div>
                            <?php if ( !empty($architecture['slider_img']['id']) ) : ?>
                                <div class="col-lg-6 order-lg-first">
                                    <?php echo wp_get_attachment_image( $architecture['slider_img']['id'], 'full' ); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div> <!-- /.container -->
                </div> <!-- /.main-bg-wrapper -->
            </div> <!-- /.arch-modal -->
            <?php
            ++$i;
        }
        ?>
        <script>
            (function ( $ ) {
                "use strict";
                $(document).ready(function (){

                    function doAnimations(elems) {
                        //Cache the animationend event in a variable
                        var animEndEv = "webkitAnimationEnd animationend";
                        var $myCarousel = $(".carousel"),
                            $firstAnimatingElems = $myCarousel.find(".carousel-item:first").find("[data-animation ^= 'animated']");
                    }

                    //Variables on page load
                    var $myCarousel = $(".carousel"),
                        $firstAnimatingElems = $myCarousel.find(".carousel-item:first").find("[data-animation ^= 'animated']");
                    //Initialize carousel
                    $myCarousel.carousel();

                    //Animate captions in first slide on page load
                    doAnimations($firstAnimatingElems);

                    //Other slides to be animated on carousel slide event
                    $myCarousel.on("slide.bs.carousel", function(e) {
                        var $animatingElems = $(e.relatedTarget).find(
                            "[data-animation ^= 'animated']"
                        );
                        doAnimations($animatingElems);
                    });

                    // scroll slides on mouse scroll
                    $('#eCommerce-carousel').bind('mousewheel DOMMouseScroll', function(e){
                        if(e.originalEvent.wheelDelta > 0 || e.originalEvent.detail < 0) {
                            $(this).carousel('prev');
                        }
                        else{
                            $(this).carousel('next');
                        }
                    });

                });
            }( jQuery ));
        </script>
        <?php

    }
}
