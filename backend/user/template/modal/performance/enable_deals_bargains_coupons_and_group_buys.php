<!-- Modal -->
<div class="modal fade" id="modalbox_performance_6" tabindex="-1" role="dialog" aria-labelledby="Enable Deals, Bargains, Coupons and Group buys" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Enable Deals, Bargains, Coupons and Group buys</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Enable Deals, Bargains, Coupons and Group buys</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="enable_deals_bargains_coupons_and_group_buys" id="enable_deals_bargains_coupons_and_group_buys" class="form-control" multiple>
                            <option id="Weekly/Monthly Discounts">Weekly/Monthly Discounts</option>
                            <option id="Prelaunch offers">Prelaunch offers</option>
                            <option id="Holiday and seasonal deals">Holiday and seasonal deals</option>
                            <option id="Abandones cart offers">Abandones cart offers</option>
                            <option id="Email/newsletter subscription offer">Email/newsletter subscription offer</option>
                            <option id="Incentives for likes, follows and shares on social media">Incentives for likes, follows and shares on social media</option>
                            <option id="Referral promos">Referral promos</option>
                            <option id="First-time shopper offer">First-time shopper offer</option>
                            <option id="Minimum purchase discount">Minimum purchase discount</option>
                            <option id="Exclusive social offers">Exclusive social offers</option>
                            <option id="Customer Loyalty Offers">Customer Loyalty Offers</option>
                            <option id="Exit Intent offers">Exit Intent offers</option>
                            <option id="Retargeted promotions">Retargeted promotions</option>
                            <option id="Influencer offers">Influencer offers</option>
                            <option id="Customer loyalty programs and member incentives">Customer loyalty programs and member incentives</option>
                            <option id="Offers for purchasing online">Offers for purchasing online</option>
                            <option id="Coupons for purchasing in-person">Coupons for purchasing in-person</option>
                            <option id="Event attendance offers">Event attendance offers</option>
                            <option id="Bulk or Group buys">Bulk or Group buys</option>
                            <option id="Customer milestone discounts">Customer milestone discounts</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>