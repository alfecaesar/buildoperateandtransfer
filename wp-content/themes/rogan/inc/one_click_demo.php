<?php
// Disable regenerating images while importing media
add_filter( 'pt-ocdi/regenerate_thumbnails_in_content_import', '__return_false' );
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );

// Change some options for the jQuery modal window
function rogan_ocdi_confirmation_dialog_options ( $options ) {
    return array_merge( $options, array(
        'width'       => 400,
        'dialogClass' => 'wp-dialog',
        'resizable'   => false,
        'height'      => 'auto',
        'modal'       => true,
    ) );
}
add_filter( 'pt-ocdi/confirmation_dialog_options', 'rogan_ocdi_confirmation_dialog_options', 10, 1 );



function rogan_ocdi_intro_text( $default_text ) {
    $default_text .= '<div class="ocdi_custom-intro-text notice notice-info inline">';
    $default_text .= sprintf (
        '%1$s <a href="%2$s" target="_blank">%3$s</a> %4$s',
        esc_html__( 'Install and activate all ', 'rogan'),
        get_admin_url(null, 'themes.php?page=tgmpa-install-plugins'),
        esc_html__( 'required plugins', 'rogan' ),
        esc_html__( 'before you click on the "Import" button.', 'rogan' )
    );
    $default_text .= sprintf (
        ' %1$s <a href="%2$s" target="_blank">%3$s</a> %4$s',
        esc_html__( 'You will find all the pages in ', 'rogan'),
        get_admin_url(null, 'edit.php?post_type=page'),
        esc_html__( 'Pages.', 'rogan' ),
        esc_html__( 'Other pages will be imported along with the main Homepage.', 'rogan' )
    );
    $default_text .= '<br>';
    $default_text .= sprintf (
        '%1$s <a href="%2$s" target="_blank">%3$s</a>',
        esc_html__( 'If you fail to import the demo data, follow the alternative way', 'rogan'),
        'https://is.gd/wojS1D',
        esc_html__( 'here.', 'rogan' )
    );
    $default_text .= '</div>';

    return $default_text;
}
add_filter( 'pt-ocdi/plugin_intro_text', 'rogan_ocdi_intro_text' );

// OneClick Demo Importer
add_filter( 'pt-ocdi/import_files', 'rogan_import_files' );
function rogan_import_files() {
    return array(
        array(
            'import_file_name'             => esc_html__( 'Saas Landing', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/saas.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'SEO & Business', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/seo.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'Creative Agency', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/agency.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'Personal Portfolio', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/personal-portfolio.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'Mobile App Landing', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/app.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'Portfolio Studio', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/portfolio-studio.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'Architecture', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/architecture.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
        array(
            'import_file_name'             => esc_html__( 'eCommerce', 'rogan' ),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'inc/demos/all/contents.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'inc/demos/all/widgets.wie',
            'import_preview_image_url'     => trailingslashit( get_template_directory_uri() ).'inc/demos/all/ecommerce.jpg',
            'import_notice'                => esc_html__( 'Install and active all required plugins before you click on the "Import Demo Data" button.', 'rogan' ),
            'preview_url'                  => 'http://droitthemes.com/wp/rogan/',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit(get_template_directory()).'inc/demos/all/settings.json',
                    'option_name' => 'rogan_opt',
                ),
            ),
        ),
    );
}


function rogan_after_import_setup( $selected_import ) {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'main_menu' => $main_menu->term_id,
        )
    );

    // Assign front page
    if ( 'Saas Landing' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home - Saas');
    }
    if ( 'SEO & Business' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home SEO');
    }
    if ( 'Creative Agency' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home');
    }
    if ( 'Personal Portfolio' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home - Portfolio');
    }
    if ( 'Mobile App Landing' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home Mobile App');
    }
    if ( 'Portfolio Studio' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home Portfolio Studio');
    }
    if ( 'Architecture' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home - Architecture');
    }
    if ( 'eCommerce' == $selected_import['import_file_name'] ) {
        $front_page_id = get_page_by_title('Home - eCommerce');
    }

    // Assign the Blog Page
    $blog_page_id  = get_page_by_title( 'Blog' );

    // Set the pages (after demo import)
    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

    // Disable Elementor's Default Colors and Default Fonts
    update_option( 'elementor_disable_color_schemes', 'yes' );
    update_option( 'elementor_disable_typography_schemes', 'yes' );
    update_option( 'elementor_global_image_lightbox', '' );

}
add_action( 'pt-ocdi/after_import', 'rogan_after_import_setup' );