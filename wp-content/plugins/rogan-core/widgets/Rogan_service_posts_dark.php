<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_service_posts_dark extends Widget_Base {

    public function get_name() {
        return 'rogan_service_posts_dark';
    }

    public function get_title() {
        return esc_html__( 'Service Posts (Dark)', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-grid';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        //***********************************  Section Title ******************************************//
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Services'
            ]
        );

        $this->add_control(
            'title_append',
            [
                'label' => esc_html__( 'Title Append', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'default' => '.'
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
            ]
        );

        $this->end_controls_section();


        //************************* Page Number ****************************//
        $this->start_controls_section(
            'number_sec',
            [
                'label' => esc_html__( 'Section Number', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'number1',
            [
                'label' => esc_html__( 'Number One', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->add_control(
            'number2',
            [
                'label' => esc_html__( 'Number Two', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tabs
         */
        /********************************* Section Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_sec_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_sce_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-two',
            ]
        );

        $this->add_control(
            'title_style_hr',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );


        $this->add_control(
            'color_sec_title_append',
            [
                'label' => esc_html__( 'Title Append Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-two span' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


        /********************************* Page Number ********************************/
        $this->start_controls_section(
            'style_page_number', [
                'label' => esc_html__( 'Number Style', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_page_number',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .section-portfo .section-num' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_page_number',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .section-portfo .section-num',
            ]
        );

        $this->end_controls_section();

        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter Options', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 4
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'exclude', [
                'label' => esc_html__( 'Exclude Service', 'saasland-core' ),
                'description' => __( 'Enter the service post ID to hide. Input the multiple ID with comma separated. <a href="https://goo.gl/nb2zwe">Find post ID</a>', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->end_controls_section();


        //------------------------------ Section Style ------------------------------
        $this->start_controls_section(
            'sec_style',
            [
                'label' => esc_html__( 'Style Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE
            ]
        );

        $this->add_control(
            'column', [
                'label' => esc_html__( 'Column', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    '6' => esc_html__('Two', 'rogan-core'),
                    '4' => esc_html__('Three', 'rogan-core'),
                    '3' => esc_html__('Four', 'rogan-core')
                ],
                'default' => '6'
            ]
        );

        // Gradient Color
        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Color One', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
            ]
        );

        $this->add_control(
            'bg_color2', [
                'label' => esc_html__( 'Color Two', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .skill-section-portfo:before' => 'background: -webkit-radial-gradient( 50% 50%, {{bg_color.VALUE}} 0%, {{VALUE}} 100%)',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $title_append = !empty($settings['title_append']) ? '<span>'.$settings['title_append'].'</span>' : '';

        $service = new WP_Query(array(
            'post_type'     => 'service',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
        ));
        ?>
        <div class="section-portfo our-service-portfo">
            <div class="section-num hide-pr">
                <?php if(isset($settings['number1'])) : ?>
                    <span> <?php echo esc_html($settings['number1']) ?> </span>
                <?php endif; ?>
                <?php if(isset($settings['number2'])) : ?>
                    <span> <?php echo esc_html($settings['number2']) ?> </span>
                <?php endif; ?>
            </div>
            <div class="container">
                <div class="title-wrapper">
                    <?php if(!empty($settings['title'])) : ?>
                        <h2 class="theme-title-two">
                            <?php
                                echo wp_kses_post($settings['title']);
                                echo wp_kses_post($title_append);
                            ?>
                        </h2>
                    <?php endif; ?>
                    <?php echo (!empty($settings['subtitle'])) ? wpautop($settings['subtitle']) : ''; ?>
                </div>

                <div class="row">
                    <?php
                    while ($service->have_posts()) : $service->the_post();
                        ?>
                        <!-- <div class="col-lg-<?php echo $settings['column'] ?>" data-aos="flip-right"> -->
                        <div class="col-lg-4" data-aos="flip-right">
                            <div class="service-block">
                                <?php the_post_thumbnail('full', array('class' => 'icon')) ?>
                                <h5 class="title">
                                    <a href="<?php the_permalink() ?>"> <?php the_title() ?> </a>
                                </h5>
                                <?php the_excerpt() ?>
                                <a href="<?php the_permalink() ?>" class="read-more">
                                    <i class="flaticon-next"></i>
                                </a>
                            </div> <!-- /.service-block -->
                        </div>
                        <?php
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <?php
    }
}