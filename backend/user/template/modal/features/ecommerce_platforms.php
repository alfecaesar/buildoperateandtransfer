<!-- Modal -->
<div class="modal fade" id="modalbox_features_2" tabindex="-1" role="dialog" aria-labelledby="eCommerce Platforms" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">eCommerce Platforms</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add eCommerce Platforms</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="ecommerce_platforms" id="ecommerce_platforms" class="form-control" multiple>
                            <option id="Magento">Magento</option>
                            <option id="Squarespace">Squarespace</option>
                            <option id="Wix">Wix</option>
                            <option id="WordPress">WordPress</option>
                            <option id="Shopify">Shopify</option>
                            <option id="WooCommerce">WooCommerce</option>
                            <option id="BigCommerce">BigCommerce</option>
                            <option id="OpenCart">OpenCart</option>
                            <option id="PrestaShop">PrestaShop</option>
                            <option id="ClickFunnels">ClickFunnels</option>
                            <option id="3dcart">3dcart</option>
                            <option id="Commerce Cloud">Commerce Cloud</option>
                            <option id="Custom">Custom</option>
                            <option id="Drupal">Drupal</option>
                            <option id="nopCommerce">nopCommerce</option>
                            <option id="osCommerce">osCommerce</option>
                            <option id="Volusion">Volusion</option>
                            <option id="X-Cart">X-Cart</option>
                            <option id="Zen Cart">Zen Cart</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>