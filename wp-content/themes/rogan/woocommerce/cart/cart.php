<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>

<form class="cart-list-form woocommerce-cart-form" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
	<?php do_action( 'woocommerce_before_cart_table' ); ?>
    <div class="table-responsive">
	<table class="table" cellspacing="0">
		<thead>
			<tr>
				<th class="product-name" colspan="2"><?php esc_html_e( 'Product', 'rogan' ); ?></th>
				<th class="product-price"><?php esc_html_e( 'Price', 'rogan' ); ?></th>
				<th class="product-quantity"><?php esc_html_e( 'QTY', 'rogan' ); ?></th>
				<th class="product-subtotal"><?php esc_html_e( 'Total', 'rogan' ); ?></th>
                <th class="product-remove">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
					<tr class="woocommerce-cart-form__cart-item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">

						<td class="product-thumbnail">
                            <?php
                            $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                            if ( ! $product_permalink ) {
                                echo wp_kses_post($thumbnail); // PHPCS: XSS ok.
                            } else {
                                printf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail ); // PHPCS: XSS ok.
                            }
                            ?>
						</td>

						<td class="product-info" data-title="<?php esc_attr_e( 'Product', 'rogan' ); ?>">
                            <?php
                            if ( ! $product_permalink ) {
                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) . '&nbsp;' );
                            } else {
                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s" class="product-name">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $cart_item_key ) );
                            }

                            do_action( 'woocommerce_after_cart_item_name', $cart_item, $cart_item_key );

                            // Meta data.
                            echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok.

                            // Backorder notification.
                            if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                echo wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'rogan' ) . '</p>', $product_id ) );
                            }
                            ?>
                            <div class="serial"> <?php echo esc_html($_product->get_sku()); ?> </div>

                            <?php
                            // Get product attributes
                            $attributes = $_product->get_attributes();

                            if ( ! $attributes ) {
                                echo "";
                            }
                            if ( is_array($attributes) ) {
                                echo "<ul>";
                                foreach ($attributes as $attribute) {

                                    echo "<li> {$attribute['name']} : ";
                                    $product_attributes = array();
                                    $product_attributes = explode( '|', $attribute['value']);
                                    $attributes_dropdown = '';
                                    $last_atts = end($product_attributes);
                                    foreach ($product_attributes as $pa) {
                                        $separator = $pa == $last_atts ? '' : ',';
                                        $attributes_dropdown .= $pa . $separator;
                                    }
                                    echo esc_html($attributes_dropdown);
                                    echo "<li>";
                                }
                                echo "</ul>";
                            }
                            ?>
						</td>

						<td class="price" data-title="<?php esc_attr_e( 'Price', 'rogan' ); ?>">
                            <span>
							<?php
								echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
                            </span>
						</td>

						<td class="quantity" data-title="<?php esc_attr_e( 'Quantity', 'rogan' ); ?>">
                            <ul class="order-box">
                                <?php
                                if ( $_product->is_sold_individually() ) {
                                    $product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
                                } else {
                                    $product_quantity = woocommerce_quantity_input( array(
                                        'input_name'   => "cart[{$cart_item_key}][qty]",
                                        'input_value'  => $cart_item['quantity'],
                                        'max_value'    => $_product->get_max_purchase_quantity(),
                                        'min_value'    => '0',
                                        'product_name' => $_product->get_name(),
                                    ), $_product, false );
                                }

                                echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item ); // PHPCS: XSS ok.
                                ?>
                            </ul>
						</td>

						<td class="price total-price" data-title="<?php esc_attr_e( 'Total', 'rogan' ); ?>">
							<?php
								echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // PHPCS: XSS ok.
							?>
						</td>

                        <td class="product-remove">
                            <?php
                            // @codingStandardsIgnoreLine
                            echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                '<a href="%s" class="remove remove-product" aria-label="%s" data-product_id="%s" data-product_sku="%s">&times;</a>',
                                esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                __( 'Remove this item', 'rogan' ),
                                esc_attr( $product_id ),
                                esc_attr( $_product->get_sku() )
                            ), $cart_item_key );
                            ?>
                        </td>
					</tr>
					<?php
				}
			}
			?>

		</tbody>
	</table>
    </div>

    <div class="d-sm-flex justify-content-between cart-footer">

        <?php do_action( 'woocommerce_cart_contents' ); ?>
        <?php if ( wc_coupons_enabled() ) : ?>
        <div class="coupon-section">
            <div class="coupon-form d-lg-flex align-items-center">
                <input type="text" name="coupon_code" class="input-text" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code', 'rogan' ); ?>" />
                <button type="submit" class="dark-button-one" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'rogan' ); ?>">
                    <?php esc_attr_e( 'Apply coupon', 'rogan' ); ?>
                </button>
            </div> <!-- /.coupon-form -->
            <button type="submit" id="update_cart" class="dark-button-one update-cart-button" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'rogan' ); ?>">
                <?php esc_html_e( 'Update cart', 'rogan' ); ?>
            </button>
            <?php do_action( 'woocommerce_cart_actions' ); ?>
            <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
        </div> <!-- /.coupon-section -->
        <?php endif; ?>
        <?php do_action( 'woocommerce_after_cart_contents' ); ?>

        <?php
        /**
         * Cart collaterals hook.
         *
         * @hooked woocommerce_cross_sell_display
         * @hooked woocommerce_cart_totals - 10
         */
        do_action( 'woocommerce_cart_collaterals' );
        ?>

    </div>

	<?php do_action( 'woocommerce_after_cart_table' ); ?>
</form>

<?php do_action( 'woocommerce_after_cart' ); ?>
