<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

get_header();
$blog_column = is_active_sidebar( 'sidebar_widgets' ) ? '8' : '12';
?>

    <!--
    =============================================
        Blog Default
    ==============================================
    -->
    <div class="our-blog blog-default pt-150 mb-200">
        <div class="container">
            <div class="row">
                <div class="col-lg-<?php echo esc_attr($blog_column) ?>">
                    <?php
                    while (have_posts()) : the_post();
                        if(has_post_format( 'video' )) {
                            wp_enqueue_style( 'fancybox' );
                            wp_enqueue_script( 'fancybox' );
                        }
                        get_template_part( 'template-parts/contents/content', get_post_format());
                    endwhile;
                    ?>
                    <div class="pd-footer d-flex justify-content-between align-items-center pt-50">
                        <?php
                        previous_posts_link( '<span class="flaticon-back"></span> &nbsp;&nbsp; '.esc_html__( 'Previous', 'rogan' ) );
                        next_posts_link( esc_html__( 'Next', 'rogan' ). '&nbsp;&nbsp;<span class="flaticon-next"></span>' );
                        ?>
                    </div>

                </div> <!-- /.col- -->

                <?php get_sidebar(); ?>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.our-blog -->

<?php
get_footer();