<!-- Modal -->
<div class="modal fade" id="modalbox_specialty" tabindex="-1" role="dialog" aria-labelledby="Choose A Performance" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Choose A Performance</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="select_specialty">Select a Specialty</label>
                    </div>
                    <div class="form-group-tag">
                        <select type="text" name="select_specialty" id="select_specialty" class="form-control" multiple>
                            
                        </select>
                    </div>
                    <div class="form-group-text">
                        <p><a class="modalbox_notwhatyoulookingfor_btn" href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_notwhatyoulookingfor">Not what you are looking for?</a></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>