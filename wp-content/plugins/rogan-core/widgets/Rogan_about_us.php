<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_about_us extends Widget_Base {

    public function get_name() {
        return 'Rogan_about_us';
    }

    public function get_title() {
        return esc_html__( 'About us', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-device-desktop';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ----------------------------------------  Section Style ------------------------------
        $this->start_controls_section(
            'select_style',
            [
                'label' => esc_html__( 'Select Style', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'style', [
                'label' => esc_html__( 'Style', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'style_01' => esc_html__('Style One', 'rogan-core'),
                    'style_02' => esc_html__('Style Two', 'rogan-core'),
                ],
                'default' => 'style_01'
            ]
        );

        $this->end_controls_section(); // End Style

        // ---------------------------------------- Title Section ------------------------------
        $this->start_controls_section(
            'title_sec',
            [
                'label' => esc_html__( 'Title Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'upper_title',
            [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'About us'
            ]
        );

        $this->add_control(
            'main_title',
            [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section(); // End

        // ---------------------------------------- Content Section ------------------------------
        $this->start_controls_section(
            'content_sec',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'quote',
            [
                'label' => esc_html__( 'Block Quote', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->add_control(
            'contents',
            [
                'label' => esc_html__( 'Contents', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
            ]
        );

        $this->end_controls_section();

        // ---------------------------------------- Author Section ------------------------------
        $this->start_controls_section(
            'author_sec',
            [
                'label' => esc_html__( 'Author Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'author_name',
            [
                'label' => esc_html__( 'Author Name', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Eh Jewel'
            ]
        );

        $this->add_control(
            'designation',
            [
                'label' => esc_html__( 'Designation', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'CEO CreativeGigs',
                'condition' => [
                   'style'  => 'style_01'
                ]
            ]
        );

        $this->add_control(
            'signature',
            [
                'label' => esc_html__( 'Signature', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->end_controls_section();

        //------------------------------ Section Image ------------------------------
        $this->start_controls_section(
            'f_img_sec', [
                'label' => esc_html__( 'Featured Image', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'fimage', [
                'label' => esc_html__( 'Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_control(
            'color_img_box',
            [
                'label' => esc_html__( 'Box Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .agn-about-us .img-box:before' => 'background: {{VALUE}};',
                ],
                'condition' => [
                    'style'  => 'style_01'
                ]
            ]
        );


        $this->end_controls_section();

        // ---------------------------------------- Box Contents ------------------------------
        $this->start_controls_section(
            'box_contents_sec',
            [
                'label' => esc_html__( 'Box Content', 'rogan-core' ),
                'condition' => [
                    'style'  => 'style_02'
                ]
            ]
        );

        $this->add_control(
            'box_contents',
            [
                'label' => esc_html__( 'Box Content', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
            ]
        );

        $this->end_controls_section();

        /// --------------------  Buttons ----------------------------------
        $this->start_controls_section(
            'button_sec',
            [
                'label' => esc_html__( 'Button', 'rogan-core' ),
                'condition' => [
                    'style'  => 'style_01'
                ]
            ]
        );

        $this->add_control(
            'btn_label', [
                'label' => esc_html__( 'Button Label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Learn More '
            ]
        );

        $this->add_control(
            'btn_icon', [
                'label' => esc_html__( 'Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON
            ]
        );
        

        $this->add_control(
            'btn_url', [
                'label' => esc_html__( 'Button URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                        'url' => '#'
                ]
            ]
        );

        $this->add_control(
            'color_btn_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .agn-about-us .learn-more' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography_btn_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .agn-about-us .learn-more',
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         */
        //----------------------------- Style Title ------------------------------
        $this->start_controls_section(
            'upper_title_sec', [
                'label' => esc_html__( 'Upper Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_upper_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .upper-title' => 'color: {{VALUE}};',
                    '{{WRAPPER}} .theme-title-one.arch-title .upper-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography_upper_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .upper-title,
                    {{WRAPPER}} .theme-title-one.arch-title .upper-title
                    ',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Title ------------------------------
        $this->start_controls_section(
            'main_title_sec', [
                'label' => esc_html__( 'Main Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'typography_main_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'bg_shapes', [
                'label' => esc_html__( 'Background Shapes', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
                'condition' => [
                    'style' => 'style_01'
                ]
            ]
        );


        $this->add_control(
            'shape1', [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-61.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-64.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape3', [
                'label' => esc_html__( 'Shape Three', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-65.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape4', [
                'label' => esc_html__( 'Shape Four', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => plugins_url('images/shape/shape-60.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'section_background', [
                'label' => esc_html__( 'Section Background', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_shape_img', [
                'label' => esc_html__( 'Background Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .agn-about-us' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();

            if ($settings['style'] == 'style_01') :
            ?>
            <div class="agn-about-us">
                <style>
                    .agn-about-us:before {
                        content: url(<?php echo esc_url($settings['bg_shape_img']['url']) ?>);
                    }
                </style>
                <?php if (!empty($settings['shape1']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape1']['url']) ?>" alt="" class="shape-one">
                <?php endif; ?>
                <?php if (!empty($settings['shape2']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape2']['url']) ?>" alt="" class="shape-two" data-aos="fade-left">
                <?php endif; ?>
                <?php if (!empty($settings['shape3']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape3']['url']) ?>" alt="" class="shape-three" data-aos="fade-right">
                <?php endif; ?>
                <?php if (!empty($settings['shape4']['url'])) : ?>
                    <img src="<?php echo esc_url($settings['shape4']['url']) ?>" alt="" class="shape-four">
                <?php endif; ?>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 order-lg-last">
                            <div class="text-wrapper">
                                <div class="theme-title-one">
                                    <?php if (!empty($settings['upper_title'])) : ?>
                                        <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                                    <?php endif; ?>
                                    <?php if (!empty($settings['main_title'])) : ?>
                                        <h2 class="main-title"><?php echo esc_html($settings['main_title']) ?></h2>
                                    <?php endif; ?>
                                </div> <!-- /.theme-title-one -->
                                <?php if (!empty($settings['contents'])) : ?>
                                    <?php echo wp_kses_post(wpautop($settings['contents'])) ?>
                                <?php endif; ?>
                                <?php if (!empty($settings['quote'])) : ?>
                                    <p class="quote"><?php echo esc_html($settings['quote']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['author_name'])) : ?>
                                    <div class="author">
                                        <span class="name"><?php echo wp_kses_post($settings['author_name']) ?></span>
                                        <?php echo esc_html($settings['designation']) ?>
                                    </div>
                                <?php endif; ?>
                                <?php if (!empty($settings['signature']['id'])) : ?>
                                    <?php echo wp_get_attachment_image($settings['signature']['id'], 'full') ?>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-6 order-lg-first">
                            <div class="img-box">
                                <?php echo wp_get_attachment_image($settings['fimage']['id'], 'full','', array('class' => 'main-img')) ?>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.container -->
                <?php if (!empty($settings['btn_label'])) : ?>
                    <a href="<?php echo esc_url($settings['btn_url']['url']) ?>" class="learn-more theme-button-two" data-aos="fade-left"
                        <?php rogan_is_external($settings['btn_url']); rogan_is_nofollow($settings['btn_url']) ?>>
                        <?php echo esc_html($settings['btn_label']) ?>
                        <?php if ( !empty($settings['btn_icon'] ) ) : ?>
                            <i class="<?php echo esc_attr($settings['btn_icon']) ?> icon-right" aria-hidden="true"></i>
                        <?php endif; ?>
                    </a>
                <?php endif; ?>
            </div>

            <?php
            elseif ($settings['style'] == 'style_02') :
            ?>
            <style>
                .about-arch:before {
                    content: url(<?php echo esc_url($settings['bg_shape_img']['url']) ?>);
                }
            </style>
            <div class="about-arch">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="theme-title-one arch-title">
                                <?php if (!empty($settings['upper_title'])) : ?>
                                    <div class="upper-title"><?php echo esc_html($settings['upper_title']) ?></div>
                                <?php endif; ?>
                                <?php if (!empty($settings['main_title'])) : ?>
                                    <h2 class="main-title"><?php echo esc_html($settings['main_title']) ?></h2>
                                <?php endif; ?>
                            </div>
                            <div class="text-wrapper">
                                <?php if (!empty($settings['quote'])) : ?>
                                    <p class="font-lato mark-text"><?php echo esc_html($settings['quote']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['contents'])) : ?>
                                    <p class="font-lato"><?php echo wp_kses_post($settings['contents']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['author_name'])) : ?>
                                    <h6 class="name"><?php echo esc_html($settings['author_name']) ?></h6>
                                <?php endif; ?>
                                <?php echo wp_get_attachment_image($settings['signature']['id'], 'full') ?>
                            </div> <!-- /.text-wrapper -->
                        </div> <!-- /.col- -->
                        <div class="col-lg-6">
                            <div class="img-box">
                                <?php echo wp_get_attachment_image($settings['fimage']['id'], 'full') ?>
                                <?php if (!empty($settings['box_contents'])) : ?>
                                    <div class="sq-box">
                                        <?php echo wp_kses_post($settings['box_contents']) ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div>
            <?php
            endif;
    }
}
