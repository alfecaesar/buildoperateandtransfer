<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_portfolio_carousel extends Widget_Base {

    public function get_name() {
        return 'rogan_portfolio_minimal';
    }

    public function get_title() {
        return __( 'Portfolio Carousel', 'rogan-hero' );
    }

    public function get_icon() {
        return 'eicon-carousel';
    }

    public function get_keywords() {
        return [ 'Projects', 'Carousel',  'minimal', 'creative'];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_style_depends() {
        return ['owl-carousel', 'owl-theme', 'animate', 'fancybox'];
    }

    protected function _register_controls() {

        // -------------------------------------------- Filtering
        $this->start_controls_section(
            'portfolio_filter', [
                'label' => __( 'Portfolio Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'title_word_limit', [
                'label' => esc_html__( 'Title Word Limit', 'rogan-core' ),
                'description' => esc_html__( 'Portfolio Title Word Limit', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 3
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'label_block' => true,
                'default' => 6
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->add_control(
            'exclude', [
                'label' => esc_html__( 'Exclude Portfolio', 'rogan-core' ),
                'description' => esc_html__( 'Enter the portfolio post ID to hide. Input the multiple ID with comma separated', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
            ]
        );

        $this->add_control(
            'show_features', [
                'label' => esc_html__( 'Key Features', 'rogan-core' ),
                'description' => esc_html__( 'Limit the Key Features to display on hover.', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 1
            ]
        );

        $this->add_control(
            'is_social_share', [
                'label' => esc_html__( 'Social Share Options', 'rogan-core' ),
                'description' => esc_html__( 'Show / Hide social share options', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'label_on' => esc_html__( 'Show', 'rogan-core' ),
                'label_off' => esc_html__( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->end_controls_section();


    }

    protected function render() {

        $settings = $this->get_settings();

        $portfolios = new WP_Query(array(
            'post_type'     => 'portfolio',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
            'post__not_in' => !empty($settings['exclude']) ? explode(',', $settings['exclude']) : ''
        ));

        $portfolio_cats = get_terms(array(
            'taxonomy' => 'portfolio_cat',
            'hide_empty' => true
        ));
        ?>

        <?php
        wp_enqueue_script( 'owl-carousel' );
        wp_enqueue_script( 'fancybox' );
        ?>
        <div class="our-project project-minimal-style">
            <div class="inner-wrapper">
                <div class="project-minimal-slider">
                    <?php
                    while ($portfolios->have_posts()) : $portfolios->the_post();
                        $title = wp_trim_words(get_the_title(), $settings['title_word_limit'], '');
                        $title_first_part = preg_replace('/\W\w+\s*(\W*)$/', '$1', $title);
                        $title_pieces = explode(' ', $title);
                        $title_last_word = array_pop($title_pieces);
                        $main_title = $title_first_part . " <span>$title_last_word</span>";
                        ?>
                        <div class="item">
                            <div class="project-item">
                                <div class="img-box">
                                    <?php the_post_thumbnail( 'rogan_550x700' ); ?>
                                </div>
                                <div class="hover-valina">
                                    <div>
                                        <h4 class="title" title="<?php the_title() ?>">
                                            <a href="<?php the_permalink() ?>"> <?php echo wp_kses_post($main_title) ?> </a>
                                        </h4>
                                        <?php
                                        if ( have_rows('key_features') ) : ?>
                                            <div>
                                                <?php
                                                $i = 0;
                                                while ( have_rows('key_features') ) : the_row();
                                                    if ( !empty($settings['show_features']) ) {
                                                        if ($i == $settings['show_features']) {
                                                            break;
                                                        }
                                                    }
                                                    echo wpautop( get_sub_field('feature_title') );
                                                    ++$i;
                                                endwhile;
                                                ?>
                                            </div>
                                        <?php endif; ?>
                                        <a href="<?php the_post_thumbnail_url() ?>" class="zoom fancybox" data-fancybox="gallery">
                                            <img src="<?php echo plugins_url('images/icon/zoom-in.svg', __FILE__) ?>" alt="<?php the_title() ?>" class="svg">
                                        </a>
                                    </div>
                                </div> <!-- /.hover-valina -->
                            </div> <!-- /.project-item -->
                        </div> <!-- /.item -->
                        <?php
                    endwhile;
                    ?>
                </div> <!-- /.project-minimal-slider -->

                <?php
                if( $settings['is_social_share'] == 'yes' ) {
                    echo function_exists('rogan_social_share') ? '<ul class="share-icon">' . rogan_social_share() . '</ul>' : '';
                }
                ?>

            </div> <!-- /.inner-wrapper -->
        </div>

        <?php
    }

}