<?php
$opt = get_option( 'rogan_opt' );
?>
<div class="project-details project-details-standard pt-150 pb-200">
    <div class="container">
        <div class="pd-header text-center">
            <h2 class="project-title-two"> <?php echo wp_kses_post(the_title()) ?> </h2>
            <?php the_excerpt() ?>
            <?php
            // check if the repeater field has rows of data
            if (have_rows( 'portfolio_attributes' )):
                ?>
                <div class="project-info">
                    <ul class="d-sm-flex justify-content-between">
                        <?php
                        while (have_rows( 'portfolio_attributes' )) : the_row();
                            ?>
                            <li>
                                <h6 class="list-title"> <?php echo esc_html(get_sub_field( 'attribute_title' )); ?> </h6>
                                <span class="date"> <?php echo esc_html(get_sub_field( 'attribute_value' )); ?> </span>
                            </li>
                            <?php
                        endwhile;
                        ?>
                    </ul>
                </div> <!-- /.project-info -->
                <?php
            endif;
            ?>
        </div> <!-- /.pd-header -->
        <div class="pd-img-box">
            <?php the_post_thumbnail( 'full' ) ?>
        </div>
        <div class="pd-body">
            <div class="row">
                <?php the_content() ?>
            </div> <!-- /.row -->
        </div> <!-- /.pd-body -->
        <div class="pd-footer d-flex justify-content-between align-items-center">
            <?php
            $prev_post = get_previous_post();
            if ($prev_post) : ?>
                <a href="<?php echo get_permalink($prev_post->ID) ?>" class="theme-pager prev">
                    <span class="flaticon-back"></span>
                    <?php esc_html_e( 'Previous Project', 'rogan' ) ?>
                </a>
            <?php endif; ?>

            <ul class="share-icon">
                <?php rogan_portfolio_social_share() ?>
            </ul>

            <?php
            $next_post = get_next_post();
            if ($next_post) : ?>
                <a href="<?php echo get_permalink($next_post->ID) ?>" class="theme-pager next">
                    <?php esc_html_e( 'Next Project', 'rogan' ) ?>
                    <span class="flaticon-next"></span>
                </a>
            <?php endif; ?>
        </div> <!-- /.pd-footer -->
    </div> <!-- /.container -->
</div>