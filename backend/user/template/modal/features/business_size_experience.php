<!-- Modal -->
<div class="modal fade" id="modalbox_features_6" tabindex="-1" role="dialog" aria-labelledby="Business Size Experience" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Business Size Experience</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Business Size Experience</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="business_size_experience" id="business_size_experience" class="form-control" multiple>
                            <option id="Very Small (1-9 employees)">Very Small (1-9 employees)</option>
                            <option id="Small (10-99 employees)">Small (10-99 employees)</option>
                            <option id="Mid (100-999 employees)">Mid (100-999 employees)</option>
                            <option id="Large (1000+ employees)">Large (1000+ employees)</option>
                            <option id="Startup">Startup</option>
                            <option id="Fortune 500">Fortune 500</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>