<?php

$project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

?>

<!-- Modal -->
<div class="modal fade" id="modalbox_create_task" tabindex="-1" role="dialog" aria-labelledby="Create Task" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create Task</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="create_task_form" method="POST" action="save_task.php">
                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="title">Title</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="description">Description</label>
                        </div>
                        <div class="form-group-tag">
                            <textarea name="description" id="description" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="estimated_time">Estimated Time</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="estimated_time" id="estimated_time" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="due_date">Due Date</label>
                        </div>
                        <div class="form-group-tag  date input-group">
                            <input type="text" name="due_date" id="due_date" class="form-control"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="status">Status</label>
                        </div>
                        <div class="form-group-tag">
                            <select name="status" id="status" class="form-control">
                                <option value="">-- Select Status --</option>
                                <option value="Closed">Closed</option>
                                <option value="Completed">Completed</option>
                                <option value="Estimate">Estimate</option>
                                <option value="In Progress">In Progress</option>
                                <option value="On Hold">On Hold</option>
                                <option value="Pending">Pending</option>
                                <option value="Waiting Assessment">Waiting Assessment</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="assigned_to">Assigned To</label>
                        </div>
                        <div class="form-group-tag">
                                
                            <select name="assigned_to" id="assigned_to" class="form-control">
                                <option value="">-- Select Assigned To --</option>
                                <option value="<?php echo $_SESSION["user_name"]; ?>"><?php echo $_SESSION["user_name"]; ?> (Owner)</option>
                                <?php
                                    $bot_requested = $database->select("bot_project_request", [
                                        "user_id",
                                        "request_status"
                                    ], [
                                        "project_id" => $project_id
                                    ]);
                                    foreach($bot_requested as $bot_requested_i)
                                    {
                                        $user_all = $database->select("user", [
                                            "user_name"
                                        ], [
                                            "user_id" => $bot_requested_i['user_id']
                                        ]);
                                        foreach($user_all as $user_all_i){

                                            if($bot_requested_i['request_status'] === 'Approved'){
                                                echo '<option value="'.$user_all_i['user_name'].'">'.$user_all_i['user_name'].'</option>';
                                            }                                  
                                        }
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <label for="assigned_to">Milestone</label>
                        </div>
                        <div class="form-group-tag">
                                
                            <select name="milestone_id" id="milestone_id" class="form-control">
                                <option value="">-- Select Milestone --</option>
                                <?php
                                    $available_milestone = $database->select("milestone", [
                                        "id",
                                        "title"
                                    ], [
                                        "project_id" => $project_id
                                    ]);
                                    foreach($available_milestone as $available_milestone_i)
                                    {
                                        echo '<option value="'.$available_milestone_i["id"].'">'.$available_milestone_i["title"].'</option>';
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]); ?>">
                    <input type="hidden" name="project_id" id="project_id" value="<?php echo $project_id; ?>">
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="document.create_task_form.submit()" class="btn btn-yellow"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>