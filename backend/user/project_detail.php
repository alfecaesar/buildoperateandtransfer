<?php include '../user/template/header.php';


include '../dbconfig.php';

$project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

$datas = $database->select("project", "*", 
[
    "user_email" => $_SESSION["user_email"],
    "project_id" => $project_id
]);

foreach($datas as $data){

?>

<?php include '../user/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../user/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2><?php echo $data['project_name']; ?></h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="projects.php" class="btn btn-grey"><span><i class="fas fa-angle-double-left"></i> View All Projects</span></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                <div class="form-group">
                        <div class="form-group-label">
                            <h4>Title</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Enter the name of BOT Project</p>
                            <p id="cp_sum_1"><?php echo $data['project_name']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Project Category</p>
                            <p id="cp_sum_2"><?php echo $data['category']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Specialty</p>
                            <p id="cp_sum_3"><?php echo $data['category_specialty']; ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Description</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">A good description includes</p>
                            <p id="cp_sum_4"><?php echo $data['description']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What do you consider an ideal minimum viable product?</p>
                            <p id="cp_sum_5"><?php echo $data['ideal_minimum_viable_product'] .' '. $data['ideal_minimum_viable_product_others'] ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Additional project files</p>
                            <p id="cp_sum_6"><?php echo $data['additional_project_files']; ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Performance</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Target markets</p>
                            <p id="cp_sum_7"><?php echo $data['target_markets']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Increased Social Media Awareness</p>
                            <p id="cp_sum_8"><?php echo $data['increased_social_media_awareness']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Lower Acquisition Costs by </p>
                            <p id="cp_sum_9"><?php echo $data['lower_acquisition_costs_by']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Locate Product Faster </p>
                            <p id="cp_sum_10"><?php echo $data['locate_product_faster']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Compare Shopping on </p>
                            <p id="cp_sum_11"><?php echo $data['compare_shopping_on']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Enable Deals, Bargains, Coupons and Group buys </p>
                            <p id="cp_sum_12"><?php echo $data['enable_deals_bargains_coupons_and_group_buys']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Create market for specific products to achieve </p>
                            <p id="cp_sum_13"><?php echo $data['create_market_for_products_to_achieve']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What additional performances are important to you? </p>
                            <p id="cp_sum_14"><?php echo $data['additional_performances_important']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Not what you are looking for? </p>
                            <p id="cp_sum_15"><?php echo $data['performance_not_what_you_looking_for']; ?></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <h4>Features</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce development deliverables</p>
                            <p id="cp_sum_16"><?php echo $data['ecommerce_development_deliverables']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Platforms</p>
                            <p id="cp_sum_17"><?php echo $data['ecommerce_platforms']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Development Skills </p>
                            <p id="cp_sum_18"><?php echo $data['ecommerce_development_skills']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Payment Systems </p>
                            <p id="cp_sum_19"><?php echo $data['ecommerce_payment_systems']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">eCommerce Database </p>
                            <p id="cp_sum_20"><?php echo $data['ecommerce_database']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Business Size Experience </p>
                            <p id="cp_sum_21"><?php echo $data['business_size_experience']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">What additional features are important to you? </p>
                            <p id="cp_sum_22"><?php echo $data['additional_features_are_important_to_you']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Not what you are looking for? </p>
                            <p id="cp_sum_23"><?php echo $data['features_not_what_you_looking_for']; ?></p>
                        </div>
                    </div>

                    <div class="form-group no-border">
                        <div class="form-group-label">
                            <h4>Budget & Timeframe</h4>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Do you have a specific budget for the minimum viable product?</p>
                            <p id="cp_sum_24"><?php echo $data['specific_budget']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">Do you have a specific timeframe to complete the minimum viable product?</p>
                            <p id="cp_sum_25"><?php echo $data['specific_timeframe_to_complete_min'].' - '.$data['specific_timeframe_to_complete_max']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">How long are you prepared to work with the freelancer or team?</p>
                            <p id="cp_sum_26"><?php echo $data['how_long_prepared_to_work_with_min'].' - '.$data['how_long_prepared_to_work_with_max']; ?></p>
                        </div>
                        <div class="form-group-text">
                            <p class="text-bold">How would you like to work with the freelancer or team?</p>
                            <p id="cp_sum_27"><?php echo $data['how_would_you_like_to_work_with']; ?></p>
                        </div>

                        <div class="form-group-text">
                            <p class="text-bold">When would you like to pay the freelancer or team?</p>
                            <p id="cp_sum_28"><?php echo $data['when_would_you_like_to_pay']; ?></p>
                        </div>
                    </div>

                </div>
                <a name="Pending_BOT_Request"></a>
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>BOT Request</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                </div>
                <div class="content_col_box_container">

                    <?php 
                    $bot_project_request_approved = (!empty($_GET['bot_project_request_approved'])) ? $_GET['bot_project_request_approved'] : '';
                        if ($bot_project_request_approved === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            BOT Successfully Approved! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <div class="project-box row project-box-head">
                        <div class="project-box-name col col-4 text-left">BOT Name</div>
                        <div class="project-box-status col col-4 text-left">BOT Email Address</div>
                        <div class="project-box-action col col-4 text-right">Action</div>
                    </div>

                    <?php

                        $bot_requested = $database->select("bot_project_request", [
                            "user_id",
                            "request_status"
                        ], [
                            "project_id" => $project_id
                        ]);


                        foreach($bot_requested as $bot_requested_i)
                        {

                            $user_all = $database->select("user", [
                                "user_id",
                                "user_name",
                                "user_email"
                            ], [
                                "user_id" => $bot_requested_i['user_id']
                            ]);


                            foreach($user_all as $user_all_i){

                                if($bot_requested_i['request_status'] === 'Pending'){
                                    echo '<div class="project-box row">'.
                                        '<div class="project-box-name col col-4 text-left"> '.$user_all_i['user_name'].' </div>'.
                                        '<div class="project-box-status col col-4 text-left">'.$user_all_i['user_email'].'</div>'.
                                        '<div class="project-box-action col col-4 text-right"><a href="approve_bot_project_request.php?user_id='.$user_all_i['user_id'].'&project_id='.$project_id.'" class="action-link-view"><i class="fas fa-check"></i> Approve</a></div>'.
                                    '</div>';
                                }
                                else{
                                    echo '<div class="project-box row approved">'.
                                        '<div class="project-box-name col col-4 text-left"> '.$user_all_i['user_name'].' </div>'.
                                        '<div class="project-box-status col col-4 text-left">'.$user_all_i['user_email'].'</div>'.
                                        '<div class="project-box-action col col-4 text-right"><a href="approve_bot_project_request.php?user_id='.$user_all_i['user_id'].'&project_id='.$project_id.'" class="action-link-view">'.$bot_requested_i['request_status'].'</a></div>'.
                                    '</div>';
                                }
                                
                                
                            }

                        }
                    ?>
                    <p>&nbsp;</p>
                </div>
                

                <a name="milestoneList"></a>
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Milestone</h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#modalbox_create_milestone" class="btn btn-yellow"><span>Create Milestone <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="content_col_box_container">  
                    <?php 
                            $milestone_saved = (!empty($_GET['milestone_saved'])) ? $_GET['milestone_saved'] : '';
                            if ($milestone_saved === 'true') { ?>
                            <div class="alert alert-primary" role="alert">
                                Milestone Successfully Created! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>      

                    <div class="task-box row task-box-head">
                            <div class="task-box-1 col col-6 text-left">Title</div>
                            <div class="task-box-2 col col-6 text-left">Due Date</div>
                    </div>

                    <?php

                        $project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

                        $datas = $database->select("milestone", [
                            "title",
                            "due_date"
                        ], [
                            "project_id" => $project_id
                        ]);

                        foreach($datas as $data){
                            echo '<div class="task-box row">'.
                                '<div class="task-box-1 col col-6 text-left">'.$data['title'].'</div>'.
                                '<div class="task-box-2 col col-6 text-left">'.$data['due_date'].'</div>'.
                                '</div>';
                        }
                        ?>

                    <p>&nbsp;</p>
                </div>
                

                <a name="taskList"></a>
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Task</h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#modalbox_create_task" class="btn btn-yellow"><span>Create Task <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="content_col_box_container">

                        <?php 
                            $task_saved = (!empty($_GET['task_saved'])) ? $_GET['task_saved'] : '';
                            if ($task_saved === 'true') { ?>
                            <div class="alert alert-primary" role="alert">
                                Task Successfully Created! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php } ?>

                        <div class="task-box row task-box-head">
                            <div class="task-box-1 col col-3 text-left">Title</div>
                            <div class="task-box-3 col col-1 text-left">Est Time</div>
                            <div class="task-box-4 col col-2 text-left">Due Date</div>
                            <div class="task-box-5 col col-2 text-left">Status</div>
                            <div class="task-box-6 col col-2 text-left">Assigned To</div>
                            <div class="task-box-1 col col-2 text-left">Milestone</div>
                        </div>

                        <?php

                        $project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

                        $datas = $database->select("task_list", [
                            "title",
                            "description",
                            "estimated_time",
                            "due_date",
                            "status",
                            "assigned_to",
                            "milestone_id"
                        ], [
                            "project_id" => $project_id
                        ]);

                        foreach($datas as $data){
                            //echo $data['milestone_id'];
                            $milestone_name = $database->get("milestone", "title", [ "id" => $data['milestone_id'] ]);

                            echo '<div class="task-box row">'.
                                '<div class="task-box-1 col col-3 text-left"><div><strong>'.$data['title'].'</strong></div><div>'.$data['description'].'</div></div>'.
                                '<div class="task-box-3 col col-1 text-left">'.$data['estimated_time'].'</div>'.
                                '<div class="task-box-4 col col-2 text-left">'.$data['due_date'].'</div>'.
                                '<div class="task-box-5 col col-2 text-left">'.$data['status'].'</div>'.
                                '<div class="task-box-6 col col-2 text-left">'.$data['assigned_to'].'</div>'.
                                '<div class="task-box-6 col col-2 text-left">'.$milestone_name.'</div>'.
                                '</div>';
                        }
                        ?>
                </div>

            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>

<?php } ?>

<?php include '../user/template/modal/create_milestone.php';?>
<?php include '../user/template/modal/create_task.php';?>
<?php include '../user/template/modal/delete_project.php';?>
<?php include '../user/template/footer.php';?>