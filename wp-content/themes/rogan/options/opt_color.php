<?php

// Color option
Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Colors', 'rogan' ),
	'id'        => 'color',
	'icon'      => 'dashicons dashicons-admin-appearance',
	'fields'    => array(
		array(
			'id'          => 'accent_color',
			'type'        => 'color',
			'title'       => esc_html__( 'Accent Color', 'rogan' ),
            'output'      => array(
                'color' => '
                    .theme-footer-one .top-footer .footer-list ul li a:hover,.theme-footer-one .bottom-footer ul li a:hover,.seo-our-goal .text-wrapper .request-button:hover,body .video-button-one:hover,body .video-button-one:hover i,
.seo-our-pricing .single-pr-table .pr-header .price,.seo-our-pricing .single-pr-table .pr-footer .trial-button:hover, body .white-shdw-button,
.sass-faq-section .sub-heading a:hover,.theme-Ecommerce-menu .right-content .user-profile-action .dropdown-menu ul li a:hover,
.eCommerce-side-menu .main-menu-list>ul>li:hover>a,.eCommerce-side-menu .main-menu-list>ul .sub-menu a:hover,.eCommerce-hero-section .social-share li a:hover,.theme-counter-three .single-counter-box .number,
.product-details-modal .main-bg-wrapper .close-button:hover,.theme-sidebar-widget .list-item li:hover a,.product-showcase .single-product-case:hover .info .name,.theme-title-one .upper-title,
.theme-pagination-one ul li a:hover,.theme-pagination-one ul li.active a,.theme-sidebar-widget .sidebar-search button:hover,.theme-sidebar-widget .list-item li .sub-menu a:hover,.agn-what-we-do .single-block .more-button,
.cart-list-form .table .product-info .product-name:hover,.cart-list-form .table .remove-product:hover,.checkout-toggle-area p button:hover,.checkout-toggle-area form .lost-passw:hover,
.solid-inner-banner .page-breadcrumbs li a:hover,.theme-sidebar-widget .sidebar-popular-product li:hover .info a,.shop-details .procuct-details .product-info .price,.faq-page .faq-search-form button:hover,
.shop-details .procuct-details .product-info .cart-button:hover,.realated-product .owl-theme .owl-nav [class*=owl-]:hover,.faq-tab-wrapper.faq-page .faq-panel .panel .panel-heading.active-panel .panel-title a:before,
.faq-page .submit-faq button:hover,.team-standard .single-team-member .hover-content ul li a:hover,body .theme-button-one,.team-minimal .single-team-member .hover-content ul li a:hover,
.team-classic .wrapper .hover-content li a:hover,.service-minimal .service-block:hover .read-more,.service-modren .service-block .service-info:hover .read-more,.gallery-sidebar .sidebar-icon ul li a:hover,
.service-standard .service-block .hover-content .read-more:hover,.our-project .isotop-menu-wrapper li.is-checked,.gallery-sidebar .sidebar-list li a:hover,.gallery-sidebar .sidebar-list li.active a,
.project-with-sidebar .project-item .hover-coco .title a:hover,.project-minimal-style .owl-theme .owl-nav [class*=owl-]:hover,.pd-footer .theme-pager:hover,.pd-footer .theme-pager:hover span,
.related-project .owl-theme .owl-nav [class*=owl-]:hover,.our-blog .single-blog-post:hover .post-data .title a,.theme-sidebar-widget .recent-news-item li:hover .rn-title a,body .line-button-one,
.blog-details .post-tag-area .share-icon li a:hover,#contact-form .form-group .help-block li:before,.contact-address-two .address-block p a:hover,.contact-address-two .address-block ul li a:hover,
body .solid-button-one:hover,.navbar .navbar-toggler,.faq-tab-wrapper .faq-panel .panel .panel-heading.active-panel .panel-title a:before,.faq-tab-wrapper-two .faq-panel .panel .panel-heading.active-panel .panel-title a:before,
.faq-tab-wrapper-four .faq-panel .panel .panel-heading .panel-title a i,.breadcrumbs-two .page-breadcrumbs li a:hover,.b-wh-text .page-breadcrumbs li a:hover,.agn-counter-section .counter-wrapper .single-counter-box .icon,
.agn-counter-section .counter-wrapper .single-counter-box .number,.theme-counter-three .single-counter-box .icon,.theme-list-item li .color-style, .theme-footer-two .social-icon li a:hover,
.agn-our-pricing .table-wrapper .pr-column .pr-header .price,.theme-footer-one .about-widget .phone,.navbar .mega-dropdown-list li a:hover,.theme-menu-one .header-right-widget .language-switcher .dropdown-menu ul li a:hover,
.theme-footer-three .phone,.theme-footer-four .footer-list ul li a:hover,.theme-footer-four .phone,.theme-footer-four .bottom-footer ul li a:hover,.theme-footer-four.light-v .footer-list ul li a:hover,
.theme-footer-four.light-v .bottom-footer ul li a:hover,.shrt-menu .main-header .call-us a,.shrt-menu .top-header .infoList li a:hover,.shrt-menu .top-header .right-widget .language-switcher .dropdown-menu ul li a:hover,
.shrt-menu .top-header .right-widget .user-profile-action .dropdown-menu ul li a:hover,.shrt-menu.text-light .navbar .dropdown-menu .dropdown-item:hover, .shrt-menu.text-light .navbar .mega-dropdown-list li a:hover,
.shrt-menu.text-light .top-header .infoList li a:hover,.shrt-menu.dark-bg .top-header .right-widget .language-switcher .dropdown-menu ul li a:hover,.shrt-menu.dark-bg .top-header .right-widget .user-profile-action .dropdown-menu ul li a:hover,
.navbar .mega-dropdown-list li a:hover .icon,.pricing-tab-menu .nav-tabs .nav-item .nav-link,.signin-form-wrapper .signUp-text a,#arch-carousel .inner-item-wrapper .upper-title,#arch-carousel .indicators-one li i,
.arch-project-gallery .isotop-menu-wrapper li.is-checked,.arch-service .single-block:hover .title a,.arch-blog .blog-post:hover .title a,.arch-contact .form-wrapper .contact-text .call-us,.arch-footer .top-footer h2 a,
.arch-footer .top-footer .phone,.theme-footer-one.arch-footer .top-footer .footer-list ul li a:hover,.theme-tab-basic.theme-tab .tabs-menu li.z-active a,.theme-tab .z-content-inner ol li a:hover,
.z-tabs.mobile > ul.z-tabs-mobile > li > a > span.drp-icon, .widget_recent_comments #recentcomments .recentcomments a:hover,
.theme-pagination-one ul li span.page-numbers.current,
.theme-menu-one.d-align-item .navbar .navbar-toggler, .theme-sidebar-widget ul li a:hover,
.theme-menu-one.d-align-item .navbar .mega-dropdown-list li a:hover, .theme-menu-one.d-align-item .navbar .mega-dropdown-list li a:hover .icon, .box_alert.box_error .icon, .theme-menu-one.fixed .navbar-nav .nav-item:hover .nav-link,
.navbar .dropdown-menu .nav-item.active .dropdown-item, #theme-banner-one .main-wrapper .slogan span, .theme-footer-one .top-footer .widget_nav_menu ul li a:hover,
.theme-menu-one.d-align-item.app-menu .navbar .dropdown-item:focus,
.theme-menu-one.d-align-item.app-menu .navbar .mega-dropdown-list li a:hover, .theme-menu-one.d-align-item.app-menu .navbar .mega-dropdown-list li a:hover .icon,
.theme-menu-one .navbar-nav .nav-item.current-menu-ancestor .nav-link, span.wpcf7-not-valid-tip::before, .navbar .dropdown-item:focus, .navbar .dropdown-item:hover, .navbar .dropdown-submenu.dropdown:hover>.dropdown-item
                    ',
                'border-color' => '
                    .theme-footer-one .top-footer .footer-information ul li a:hover,.seo-contact-banner .contact-button:hover,.theme-Ecommerce-menu .right-content .cart-action-wrapper .button-group a:hover,body .theme-button-one,
.theme-sidebar-widget .size-filter li a:hover,.shop-details .procuct-details .product-info .cart-button,.shop-details .procuct-details .product-info .wishlist-button:hover,.faq-page .submit-faq button,
.theme-action-banner-two .banner-button:hover,.our-project .isotop-menu-wrapper li.is-checked,#isotop-gallery-wrapper .isotop-item .hover-jojo ul li a:hover,.theme-sidebar-widget .keywords-tag li a:hover,
.blog-details .user-comment-area .single-comment .reply:hover,.contact-us-section .contact-info ul li a:hover,.form-style-three .send-button:hover,body .line-button-one,body .solid-button-one,body .line-button-two:hover,
.agn-our-pricing .table-wrapper .pr-column:hover .line-button-two,#theme-banner-two .main-wrapper .button-group .contact-button:hover,.agn-our-gallery .main-wrapper .view-gallery:hover,.theme-footer-three .social-icon li a:hover,
.theme-footer-four .social-icon li a:hover,.pricing-tab-menu .nav-wrapper,body .line-button-three:hover,.arch-testimonial .owl-theme .owl-dots .owl-dot span
                    ',
                'background-color' => '
                    .scroll-top,.theme-footer-one .top-footer .footer-information ul li a:hover,.theme-menu-two .quote-button:hover,.theme-Ecommerce-menu .right-content .cart-action-wrapper .button-group a:hover,
#eCommerce-carousel .inner-item-wrapper .button-group .shop-now:hover,#eCommerce-carousel .inner-item-wrapper .button-group .details-info-button:hover,.product-details-modal .main-bg-wrapper .cart-button,
.theme-sidebar-widget .price-ranger .ui-slider .ui-slider-range,.theme-sidebar-widget .size-filter li a:hover,.shop-demo-filter .selectize-dropdown .option:hover,.shop-demo-filter .selectize-dropdown .active,
.product-showcase .single-product-case .info .cart-button span,.dark-button-one:hover,.shop-details .procuct-details .product-info .cart-button,.shop-details .procuct-details .product-info .wishlist-button:hover,
.shop-details .product-review-tab .tab-content .list-item li:before,.faq-page .submit-faq button,body .theme-button-one:before,.newsletter-section.agn-theme .main-wrapper form button,
.woocommerce .widget_price_filter .ui-slider .ui-slider-range, .widget_product_tag_cloud .tagcloud a:hover, .our-blog .bg-solid-post.single-blog-post .post-data,
.woocommerce .widget_price_filter .price_slider_amount .button:hover, .theme-Ecommerce-menu .right-content .cart-action-wrapper .dropdown-toggle .item-count,
.newsletter-section.agn-theme .theme-title-one.upper-bar:before,.video-action-banner-one .video-button,.video-action-banner-one .video-button:before,.theme-action-banner-two .banner-button:hover,
#isotop-gallery-wrapper .isotop-item .hover-jojo ul li a:hover,.gallery-sidebar .sidebar-title:before,.project-details .side-block .block-title:before,.our-blog .single-blog-post:hover .img-holder .video-button,
.theme-sidebar-widget .keywords-tag li a:hover,.blog-details .user-comment-area .single-comment .reply:hover,body .theme-button-two,.blog-details-fg .blog-fg-data .video-banner-blog .video-button:hover,
#contact-form .form-group .help-block,.contact-us-section .contact-info ul li a:hover,.form-style-three .send-button:hover,body .line-button-one:hover,body .solid-button-one,body .line-button-two:hover,
.agn-our-pricing .table-wrapper .pr-column:hover .line-button-two,body .white-shdw-button:hover,#theme-banner-two .main-wrapper .button-group .contact-button:hover,.seo-what-we-do .single-block:nth-child(1) .wrapper .icon-box,
.faq-tab-wrapper-three .faq-panel .panel .panel-heading.active-panel .panel-title a,.faq-tab-wrapper-four .faq-panel .panel .panel-heading.active-panel .panel-title a,.agn-our-gallery .main-wrapper .view-gallery:hover,
.agn-home-blog .single-blog-post .flip-box-back,.seo-contact-banner .contact-button:hover,.navbar .dropdown-menu .dropdown-item span,.ln-inner-page-demo .inner-wrapper .single-page a .new,.theme-footer-three .social-icon li a:hover,
.theme-footer-four .social-icon li a:hover,.navbar .mega-dropdown-list li a span,.btn-white:hover,.ln-home-demo .single-page-demo .new,.shrt-menu .main-header .cart-action-wrapper .button-group a:hover,
.pricing-tab-menu .nav-tabs .nav-item .nav-link.active,#arch-carousel .indicators-one li.active span,.about-arch .text-wrapper .mark-text:before,.about-arch .img-box .sq-box,body .line-button-three:hover,
.arch-testimonial .owl-theme .owl-dots .owl-dot.active span,.arch-testimonial .owl-theme .owl-dots .owl-dot span:hover,#arch-carousel .details-info-button,.theme-tab .tabs-menu li a:before,
.theme-tab .z-content-inner .list-item li:before,.theme-tab-solid.theme-tab .tabs-menu li.z-active a,.theme-tab .nested-tab-menu li.z-active a:before, .theme-menu-one.d-align-item .navbar-nav .nav-item .nav-link:before, span.wpcf7-not-valid-tip, .blog-default .single-blog-post p.sticky-label
                    ',
                'fill' => '.theme-Ecommerce-menu .right-content .user-profile-action .dropdown-menu ul li a:hover .icon path'
            ),
        ),
	)
));