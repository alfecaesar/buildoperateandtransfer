<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_blog_masonry_grid extends Widget_Base {

    public function get_name() {
        return 'rogan_blog_masonry_grid';
    }

    public function get_title() {
        return esc_html__( 'Blog Masonry Grid', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-posts-grid';
    }

    public function get_keywords() {
        return [ 'rogan', 'blog', 'masonry', 'grid' ];
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['isotope'];
    }

    protected function _register_controls() {

        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter Options', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'show_count', [
                'label' => esc_html__( 'Show Posts Count', 'rogan-core' ),
                'type' => Controls_Manager::NUMBER,
                'default' => 9
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

        /**
         * Style Tab
         * SVG Shape Images
         */
        // *************************** Section Title Style ***********************************************
        $this->start_controls_section(
            'sec_title_style',
            [
                'label' => esc_html__( 'Blog Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'color_main_title',
            [
                'label' => esc_html__( 'Main Title Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .blog-masonry .single-blog-post .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_main_title',
                'label' => esc_html__( 'Main Title Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} ..blog-masonry .single-blog-post .title',
            ]
        );

        $this->end_controls_section();
    }

    protected function render() {

        $settings = $this->get_settings();

        $blog_posts = new WP_Query(array(
            'post_type'     => 'post',
            'posts_per_page'=> $settings['show_count'],
            'order' => $settings['order'],
        ));
        ?>
            <div class="our-blog blog-masonry pt-150 mb-200">
                <div class="container">
                    <div class="row masnory-blog-wrapper">
                        <div class="grid-sizer"></div>
                        <?php
                            while ($blog_posts->have_posts()) : $blog_posts->the_post();
                            if( has_post_format('video') ) {
                                wp_enqueue_style( 'fancybox' );
                                wp_enqueue_script( 'fancybox' );
                            } 
                        ?>
                        <div class="isotop-item">
                            <div class="single-blog-post">
                                <?php if ( has_post_thumbnail() ) : ?>
                                   <div class="img-holder"><?php the_post_thumbnail('rogan_355x390'); ?></div>
                                <?php endif; ?>
                                <div class="post-data">
                                    <a href="<?php rogan_day_link(); ?>" class="date"><?php the_time( get_option( 'date_format' ) ); ?></a>
                                    <h5 class="blog-title-one title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                                    <a href="<?php the_permalink(); ?>" class="read-more"><i class="flaticon-next-1"></i></a>
                                </div> <!-- /.post-data -->
                            </div> <!-- /.single-blog-post -->
                        </div> <!-- /.isotop-item -->
                        <?php
                        endwhile;
                        wp_reset_postdata();
                        ?>
                    </div> <!-- /.masnory-blog-wrapper -->

                    <div class="text-center pt-90">
                        <a href="#" class="theme-button-one">Load More</a>
                    </div>
                </div> <!-- /.container -->
            </div> <!-- /.our-blog -->
            
            <script>
                (function ( $ ) {
                    "use strict";
                    $(document).ready( function() {

                    // ----------------------------- isotop gallery
                    if ($(".masnory-blog-wrapper").length) {
                        var $grid = $('.masnory-blog-wrapper').isotope({
                          // options
                          itemSelector: '.isotop-item',
                          percentPosition: true,
                          masonry: {
                            // use element for option
                            columnWidth: '.grid-sizer'
                          }

                        });
                    }

                    });
                }( jQuery ));
            </script>
        <?php
    }
}