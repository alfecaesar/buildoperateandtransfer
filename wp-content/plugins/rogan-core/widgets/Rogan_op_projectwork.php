<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_op_projectwork extends Widget_Base {

    public function get_name() {
        return 'rogan_op_projectwork';
    }

    public function get_title() {
        return esc_html__( 'Portfolio Studio', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-gallery-masonry';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    public function get_script_depends() {
        return ['isotope'];
    }

    protected function _register_controls() {

        //********************** Project Top Section  *****************//
        $this->start_controls_section(
            'project_section', [
                'label' => esc_html__( 'Heading Section', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'section_heading', [
                'label' => esc_html__( 'Section Heading', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Check our latest <span>projects.</span>'
            ]
        );

        $this->add_control(
            'color_heading_title',
            [
                'label' => esc_html__( 'Section Heading Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .theme-title-four font-k2d' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_heading_title',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .theme-title-four font-k2d',
            ]
        );

        $this->add_control(
            'topper_content', [
                'label' => esc_html__( 'Top Right Content', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Lorem ipsum dolor sit, consectetur some adipiscing elit eiusmod tempor incididu nt ut labore et dol magna aliqua.mollit ani mui laborum.'
            ]
        );

        $this->add_control(
            'color_topper_content',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-project-one .upper-heading p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_topper_content',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-project-one .upper-heading p',
            ]
        );

        $this->add_control(
            'big_heading', [
                'label' => esc_html__( 'Big Heading', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'work'
            ]
        );

        $this->add_control(
            'big_text_heading_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-project-one .inner-wrapper .work-text' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'big_text_heading_typo',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-project-one .inner-wrapper .work-text',
            ]
        );

        $this->end_controls_section();



        // ------------------------------ Project List ------------------------------
        $this->start_controls_section(
            'project_show',
            [
                'label' => esc_html__( 'Projects', 'rogan-core' ),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'project_thumb', [
                'label' => esc_html__( 'Project Image', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
            ]
        );

        $repeater->add_control(
            'icon',
            [
                'label' => __( 'Icon', 'rogan-core' ),
                'type' => Controls_Manager::ICON,
            ]
        );

        $repeater->add_control(
            'icon_bg_color',
            [
                'label' => esc_html__( 'Icon BG Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} {{CURRENT_ITEM}} .icon-box' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $repeater->add_control(
            'project_title', [
                'label' => esc_html__( 'Project Title', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Uber.'
            ]
        );

        $repeater->add_control(
            'project_des', [
                'label' => esc_html__( 'Project Description', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'The best client we work with.'
            ]
        );


        $repeater->add_control(
            'project_url', [
                'label' => esc_html__( 'Project URL', 'rogan-core' ),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'default' => [
                    'url' => '#'
                ]
            ]
        );

        $this->add_control(
            'all_project', [
                'label' => esc_html__( 'Project List', 'rogan-core' ),
                'type' => Controls_Manager::REPEATER,
                'title_field' => '{{{ project_title }}}',
                'fields' => $repeater->get_controls(),
            ]
        );

        $this->end_controls_section();

        
        //********************** About Info  *****************//

        $this->start_controls_section(
            'project_section_info', [
                'label' => esc_html__( 'Project Informations', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'info_uper_text', [
                'label' => esc_html__( 'Project Bottom - Top Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Check More.',
            ]
        );

        $this->add_control(
            'project_info_text', [
                'label' => esc_html__( 'Project Bottom Description', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'We have done more than 150+ project with well know company and still counting'
            ]
        );

        $this->add_control(
            'project_section_button', [
                'label' => esc_html__( 'Button Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Check our all work'
            ]
        );
        
        $this->add_control(
            'project_section_button_url', [
                'label' => esc_html__( 'Button Link', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '#'
            ]
        );

        $this->add_control(
            'button_color',
            [
                'label' => esc_html__( 'Button BG Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-project-one .more-text .more-button' => 'background: {{VALUE}};',
                ],
            ]
        );        

        $this->add_control(
            'button_text_color',
            [
                'label' => esc_html__( 'Button Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'scheme' => [
                    'type' => Scheme_Color::get_type(),
                    'value' => Scheme_Color::COLOR_1,
                ],
                'selectors' => [
                    '{{WRAPPER}} .op-project-one .more-text .more-button' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'button_color_typo',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .op-project-one .more-text .more-button',
            ]
        );        

        $this->end_controls_section();


        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'bg_color', [
                'label' => esc_html__( 'Background Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => array(
                    '{{WRAPPER}} .op-project-one' => 'background: {{VALUE}};',
                )
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .op-project-one' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        ?>
            <div class="op-project-one">
                <div class="container">
                    <div class="upper-heading">
                        <div class="row">
                            <?php if ( !empty($settings['section_heading']) ) : ?>
                            <div class="col-lg-6">
                                <h2 class="theme-title-four font-k2d">
                                    <?php echo htmlspecialchars_decode( $settings['section_heading'] ); ?>
                                </h2>     
                            </div>
                            <?php endif; ?>
                            <?php if ( !empty($settings['topper_content']) ) : ?>
                            <div class="col-lg-6">
                                <p class="font-lato"><?php echo htmlspecialchars_decode( $settings['topper_content'] ); ?></p>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="inner-wrapper">
                    <?php if ( !empty($settings['big_heading']) ) : ?>
                    <div class="work-text"><?php echo htmlspecialchars_decode( $settings['big_heading'] ); ?></div>
                    <?php endif; ?>
                    <div class="bg-wrapper">
                        <div class="container">
                            <div class="portfolio-studio-gallery" id="isotop-gallery-wrapper">
                                <div class="grid-sizer"></div>

                                <?php 
                                $x = 0;
                                if ( !empty($settings['all_project'] )) {
                                    foreach ($settings['all_project'] as $project) {
                                    $project_img = isset($project['project_thumb']['id']) ? $project['project_thumb']['id'] : '';
                                    $project_img = wp_get_attachment_image_src($project_img, 'full');
                                    $target = $project['project_url']['is_external'] ? 'target="_blank"' : '';
                                    $x++;
                                    ?>
                                    <?php if (!empty($project_img[0])) : ?>
                                    <div class="isotop-item elementor-repeater-item-<?php echo $project['_id'] ?> <?=($x == 2) ? 'mt-150' : ''?>">
                                        <div class="item-content">
                                            <div class="img-box">
                                                <img src="<?php echo $project_img[0] ?>" alt="<?php echo esc_attr($project['project_title']) ?>">
                                            </div> <!-- /.img-box -->

                                            <div class="text">
                                                <?php
                                                if ( !empty($project['icon']['value']) ) { ?>
                                                    <div class="icon-box cl-<?php echo $x; ?>">
                                                       <i class="<?php echo $project['icon']['value'] ?>" aria-hidden="true"></i>
                                                    </div>
                                                <?php } elseif( !empty($project['icon']) ) { ?>
                                                    <div class="icon-box cl-<?php echo $x; ?>">
                                                        <i class="<?php echo $project['icon'] ?>" aria-hidden="true"></i>
                                                    </div>
                                                <?php } ?>
                                                <?php if ( !empty($project['project_title']) ) : ?>
                                                    <h3 class="font-k2d"><?php echo htmlspecialchars_decode( $project['project_title'] ); ?></h3>
                                                <?php endif; ?>
                                                <?php if ( !empty($project['project_des']) ) : ?>
                                                    <p><?php echo htmlspecialchars_decode( $project['project_des'] ); ?></p>
                                                <?php endif; ?>
                                                <?php if ( !empty($project['project_url']['url']) ) : ?>
                                                    <a href="<?php echo esc_url($project['project_url']['url']) ?>" class="read-more" <?php echo $target ?>>+</a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php
                                }
                            }
                            ?>
                            </div> <!-- /#isotop-gallery-wrapper -->

                            <div class="more-text ml-auto">
                                <?php if ( !empty($settings['info_uper_text']) ) : ?>
                                <span><?php echo htmlspecialchars_decode( $settings['info_uper_text'] ); ?></span>
                                <?php endif; ?>

                                <?php if ( !empty($settings['project_info_text']) ) : ?>
                                    <p><?php echo htmlspecialchars_decode( $settings['project_info_text'] ); ?></p>
                                <?php endif; ?>
                                <?php if ( !empty($settings['project_section_button']) ) : ?>
                                    <a href="<?php echo $settings['project_section_button_url']; ?>" class="more-button"><?php echo htmlspecialchars_decode( $settings['project_section_button'] ); ?> <i class="flaticon-next-1"></i></a>
                                <?php endif; ?>
                            </div>

                        </div> <!-- /.container -->
                        <div class="project-text"> <?php esc_html_e( 'projects', 'rogan-core' ); ?> </div>
                    </div>
                </div> <!-- /.inner-wrapper -->
            </div> <!-- /.op-project-one -->

            <script>
                (function ( $ ) {
                    "use strict";
                    $(document).ready( function() {

                    // ----------------------------- isotop gallery
                    if ($(".portfolio-studio-gallery").length) {
                        var $grid = $('.portfolio-studio-gallery').isotope({
                          // options
                          itemSelector: '.isotop-item',
                          percentPosition: true,
                          masonry: {
                            // use element for option
                            columnWidth: '.grid-sizer'
                          }
                        });
                    }
                    });
                }( jQuery ));
            </script>

         
        <?php
    }
}