<?php

session_start();

include '../dbconfig.php';

$project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';
$project_name = $database->get("project", "project_name", [ "project_id" => $project_id ]);

//echo $project_id;

    $database->delete("project", [
        "project_id" => $project_id
    ]);

    

    //echo $project_name;

    $database->insert("user_activity", [
        "user_id" => $_SESSION["user_id"],
        "activity_name" => "Deleted a Project",
        "activity_description" => "<em>".$_SESSION["user_name"]."</em> deleted <em>".$project_name."</em>"
    ]);

    header("Location: http://afarinsights.com/backend/user/projects.php?project_deleted=true");
    //var_dump($database->log());

?>