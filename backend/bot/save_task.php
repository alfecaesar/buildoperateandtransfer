<?php

include '../dbconfig.php';

$user_id = (!empty($_POST['user_id'])) ? $_POST['user_id'] : '';
$project_id = (!empty($_POST['project_id'])) ? $_POST['project_id'] : '';
$title = (!empty($_POST['title'])) ? $_POST['title'] : '';
$description = (!empty($_POST['description'])) ? $_POST['description'] : '';
$estimated_time = (!empty($_POST['estimated_time'])) ? $_POST['estimated_time'] : '';
$due_date = (!empty($_POST['due_date'])) ? $_POST['due_date'] : '';
$status = (!empty($_POST['status'])) ? $_POST['status'] : '';
$assigned_to = (!empty($_POST['assigned_to'])) ? $_POST['assigned_to'] : '';



    $database->insert("task_list", [
        "user_id" => $user_id,
        "project_id" => $project_id,
        "title" => $title,
        "description" => $description,
        "estimated_time" => $estimated_time,
        "due_date" => $due_date,
        "status" => $status,
        "assigned_to" => $assigned_to,
    ]);

    header("Location: http://afarinsights.com/backend/bot/task.php?project_id=".$project_id."&task_saved=true");
    //var_dump($database->log());

?>