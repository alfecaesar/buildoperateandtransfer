<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use WP_Query;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}



/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_faq extends Widget_Base {

    public function get_name() {
        return 'Rogan_faq';
    }

    public function get_title() {
        return esc_html__( 'FAQ with Tab', 'rogan-hero' );
    }

    public function get_icon() {
        return 'eicon-menu-bar';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ------------------------------  Category Title ------------------------------
        $this->start_controls_section(
            'section_tabs',
            [
                'label' => __( 'Category Title', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'title',
            [
                'label' => esc_html__( 'Title', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Questions? Look here.',
            ]
        );

        $this->add_control(
            'subtitle',
            [
                'label' => esc_html__( 'Subtitle', 'saasland-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
            ]
        );

        $this->end_controls_section();


        // ------------------------------  Faq Title ------------------------------
        $this->start_controls_section(
            'faq_section',
            [
                'label' => __( 'FAQ Title', 'saasland-core' ),
            ]
        );

        $this->add_control(
            'faq_title',
            [
                'label' => esc_html__( 'Title', 'saasland-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'How to purchase',
            ]
        );

        $this->end_controls_section();


        //*********************************  Shape Image ********************************//
        $this->start_controls_section(
            'shape',
            [
                'label' => esc_html__( 'Title Icon Box', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'shape_img1',
            [
                'label' => esc_html__( 'Shape One', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/shape/bg-shape1.svg', __FILE__)
                ],
            ]
        );

        $this->add_control(
            'shape_img2', [
                'label' => esc_html__( 'Shape Two', 'rogan-core' ),
                'type' => Controls_Manager::MEDIA,
                'description' => esc_html__( 'Recommended to upload a SVG to PNG image.', 'rogan-core' ),
                'default' => [
                    'url' => plugins_url('images/icon/icon29.svg', __FILE__)
                ],
            ]
        );

        $this->end_controls_section();


        // ---------------------------------- Filter Options ------------------------
        $this->start_controls_section(
            'filter', [
                'label' => esc_html__( 'Filter Options', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'all_label', [
                'label' => esc_html__( 'All filter label', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'All'
            ]
        );

        $this->add_control(
            'order', [
                'label' => esc_html__( 'Order', 'rogan-core' ),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ],
                'default' => 'ASC'
            ]
        );

        $this->end_controls_section();

         // ------------------------------ Scroll to Bottom ------------------------
        $this->start_controls_section(
            'scroll2btm', [
                'label' => esc_html__( 'Scroll to Icon', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'is_scroll_icon', [
                'label' => __( 'Scroll Icon', 'rogan-core' ),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __( 'Show', 'rogan-core' ),
                'label_off' => __( 'Hide', 'rogan-core' ),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'target_id', [
                'label' => esc_html__( 'Target ID', 'rogan-core' ),
                'description' => esc_html__( 'Enter here the Targeted HTML ID where the scroll will navigate.', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'footer'
            ]
        );

        $this->end_controls_section();


        /**
         * Style Tab
         * @Title
         */
        /********************************* Title ********************************/
        $this->start_controls_section(
            'style_sec_title', [
                'label' => esc_html__( 'Section Title', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'section_title_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .theme-title-one .main-title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_section_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'selector' => '
                    {{WRAPPER}} .theme-title-one .main-title',
            ]
        );

        $this->end_controls_section();


        /* ------------------------------ Style Subtitle ------------------------------*/
        $this->start_controls_section(
            'style_subtitle_sec', [
                'label' => esc_html__( 'Subtitle Section', 'rogan-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'subtitle_color',
            [
                'label' => esc_html__( 'Text Color', 'rogan-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sass-faq-section .sub-heading' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'subtitle_typography',
                'label' => esc_html__( 'Typography', 'rogan-core' ),
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '
                {{WRAPPER}} .sass-faq-section .sub-heading',
            ]
        );

        $this->end_controls_section();

        //------------------------------ Style Section ------------------------------
        $this->start_controls_section(
            'style_section', [
                'label' => esc_html__( 'Style section', 'saasland-core' ),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'sec_padding', [
                'label' => esc_html__( 'Section padding', 'saasland-core' ),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => [ 'px', '%', 'em' ],
                'selectors' => [
                    '{{WRAPPER}} .sass-faq-section' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'default' => [
                    'unit' => 'px', // The selected CSS Unit. 'px', '%', 'em',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $cats = get_terms( array (
            'taxonomy' => 'faq_cat',
            'hide_empty' => true
        ));
        ?>
        <div class="sass-faq-section">
            <div class="section-shape-one">
                <img src="<?php echo plugins_url('images/shape/shape-18.svg', __FILE__) ?>" alt="Rogan floating object">
            </div>
            <img src="<?php echo plugins_url('images/shape/shape-26.svg', __FILE__) ?>" alt="Rogan floating object" class="section-shape-two">
            <img src="<?php echo plugins_url('images/shape/shape-29.svg', __FILE__) ?>" alt="Rogan floating object" class="section-shape-three">
            <img src="<?php echo plugins_url('images/shape/shape-30.svg', __FILE__) ?>" alt="Rogan floating object" class="section-shape-four">
            <div class="container">
                <div class="theme-title-one text-center hide-pr">
                    <div class="icon-box hide-pr">
                        <?php if (!empty($settings['shape_img1']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['shape_img1']['url']) ?>" alt="Rogan background shape image" class="bg-shape">
                        <?php endif; ?>
                        <?php if (!empty($settings['shape_img2']['url'])) : ?>
                            <img src="<?php echo esc_url($settings['shape_img2']['url']) ?>" alt="" class="icon">
                        <?php endif; ?>
                    </div>
                    <?php if ( !empty($settings['title']) ) : ?>
                        <h2 class="main-title"> <?php echo esc_html($settings['title']) ?> </h2>
                    <?php endif; ?>
                </div> <!-- /.theme-title-one -->
                <?php if ( !empty($settings['subtitle']) ) : ?>
                    <p class="sub-heading"> <?php echo wp_kses_post($settings['subtitle']) ?> </p>
                <?php endif; ?>
                <div class="faq-tab-wrapper">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <?php
                        foreach ( $cats as $index => $cat ) :
                            $tab_count = $index + 1;
                            $tab_title_faq_setting_key = $this->get_repeater_setting_key( 'tab_li', '', $index );
                            $active = $tab_count == 1 ? ' active' : '';
                            $this->add_render_attribute( $tab_title_faq_setting_key, [
                                'class' => [ 'nav-link', $active ],
                                'data-toggle' => 'tab',
                                'href' => '#'.$cat->slug,
                            ]);
                            ?>
                            <li class="nav-item">
                                <a <?php echo $this->get_render_attribute_string( $tab_title_faq_setting_key ); ?>>
                                    <?php echo $cat->name; ?>
                                </a>
                            </li>
                        <?php
                        endforeach;
                        ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <!-- Single Tab -->
                         <?php
                        foreach ( $cats as $index => $cat ) :
                            $tab_count = $index + 1;
                            $tab_content_setting_key = $this->get_repeater_setting_key( 'tab_content', '', $index );
                            $active = $tab_count == 1 ? ' show active' : '';
                            $this->add_render_attribute( $tab_content_setting_key, [
                                'class' => [ 'tab-pane fade', $active ],
                                'id' => $cat->slug,
                            ]);
                            ?>

                            <div <?php echo $this->get_render_attribute_string( $tab_content_setting_key ); ?>>
                                <div class="row">
                                    <?php
                                    $faqs = new WP_Query( array (
                                        'post_type' => 'faq',
                                        'tax_query' => array (
                                            array(
                                                'taxonomy' => 'faq_cat',
                                                'field'    => 'slug',
                                                'terms'    => $cat->slug,
                                            ),
                                        ),
                                    ));
                                    ?>

                                    <!-- ================== FAQ Panel ================ -->
                                    <div class="faq-panel">
                                        <div class="panel-group theme-accordion" id="accordion-<?php echo $cat->slug; ?>">
                                            <?php
                                            $faq_i = 0;
                                            while ($faqs->have_posts()) : $faqs->the_post(); $faq_i++;
                                                $active = $faq_i == 0 ? ' active-panel' : '';
                                                $active_show = $faq_i == 0 ? 'show' : '';
                                                ?>
                                                <div class="panel">
                                                    <div class="panel-heading <?php /*echo $active*/ ?>">
                                                        <h6 class="panel-title">
                                                            <a data-toggle="collapse" data-parent="#accordion-<?php echo $cat->slug; ?>" href="#collapse<?php the_ID(); ?>">
                                                               <?php the_title() ?>
                                                            </a>
                                                        </h6>
                                                    </div>
                                                    <div id="collapse<?php the_ID(); ?>" class="panel-collapse collapse <?php /*echo $active_show;*/ ?>">
                                                        <div class="panel-body">
                                                            <?php the_content() ?>
                                                        </div>
                                                    </div>
                                                </div> <!-- /panel 1 -->
                                                <?php
                                            endwhile;
                                            wp_reset_postdata();
                                            ?>
                                        </div> <!-- end #accordion -->
                                    </div> <!-- End of .faq-panel -->

                                </div> <!-- /.row -->

                            </div> <!-- /.tab-pane -->

                            <?php
                        endforeach;
                        ?>

                    </div> <!-- /.tab-content -->
                <?php if ( $settings['is_scroll_icon'] == 'yes' ) :  ?>
                    <a href="#<?php echo esc_attr($settings['target_id']) ?>" class="down-button scroll-target">
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div> <!-- /.container -->
        </div>
        <?php
    }

}