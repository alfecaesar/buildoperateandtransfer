<?php

session_start();

include '../dbconfig.php';

$user_id = (!empty($_POST['user_id'])) ? $_POST['user_id'] : '';
$project_id = (!empty($_POST['project_id'])) ? $_POST['project_id'] : '';
$title = (!empty($_POST['title'])) ? $_POST['title'] : '';
$due_date = (!empty($_POST['due_date'])) ? $_POST['due_date'] : '';
$milestone_id = (!empty($_POST['milestone_id'])) ? $_POST['milestone_id'] : '';


    $database->insert("milestone", [
        "user_id" => $user_id,
        "project_id" => $project_id,
        "project_owner_user_id" => $database->get("project", "project_owner_user_id", [ "project_id" => $project_id ]),
        "title" => $title,
        "due_date" => $due_date,
        "milestone_id" => $milestone_id
    ]);

    $project_name = $database->get("project", "project_name", [ "project_id" => $project_id ]);

    $database->insert("user_activity", [
        "user_id" => $_SESSION["user_id"],
        "activity_name" => "Created a Milestone",
        "activity_description" => "<em>".$_SESSION["user_name"]."</em> created a milestone for <em>".$project_name."</em>"
    ]);

    header("Location: http://afarinsights.com/backend/user/project_detail.php?project_id=".$project_id."&milestone_saved=true#milestoneList");
    //var_dump($database->log());

?>