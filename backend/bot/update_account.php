<?php

include '../dbconfig.php';

$user_name = (!empty($_POST['user_name'])) ? $_POST['user_name'] : '';
$user_email = (!empty($_POST['user_email'])) ? $_POST['user_email'] : '';
$user_password = (!empty($_POST['user_password'])) ? $_POST['user_password'] : '';


if($user_name === '' or $user_email === '' or $user_password === '' ){
    header("Location: http://afarinsights.com/backend/bot/account.php?update_field_error=true");
}
else{
        $database->update("user", [
            "user_name" => $user_name,
            "user_email" => $user_email,
            "user_password" => $user_password
        ]);

        session_start();

        $_SESSION["user_name"] = $user_name;
        $_SESSION["user_email"] = $user_email;
        $_SESSION["user_password"] = $user_password;
    
        header("Location: http://afarinsights.com/backend/bot/account.php?user_saved=true");
}



?>