<?php

session_start();

include '../dbconfig.php';

$user_name = (!empty($_GET['user_name'])) ? $_GET['user_name'] : '';
$user_email = (!empty($_GET['user_email'])) ? $_GET['user_email'] : '';
$project_name = (!empty($_GET['project_name'])) ? $_GET['project_name'] : '';
$category = (!empty($_GET['category'])) ? $_GET['category'] : '';
$category_specialty = (!empty($_GET['category_specialty'])) ? $_GET['category_specialty'] : '';
$suggest_specialty = (!empty($_GET['suggest_specialty'])) ? $_GET['suggest_specialty'] : '';
$description = (!empty($_GET['description'])) ? $_GET['description'] : '';
$ideal_minimum_viable_product = (!empty($_GET['ideal_minimum_viable_product'])) ? $_GET['ideal_minimum_viable_product'] : '';
$ideal_minimum_viable_product_others = (!empty($_GET['ideal_minimum_viable_product_others'])) ? $_GET['ideal_minimum_viable_product_others'] : '';
$additional_project_files = (!empty($_GET['additional_project_files'])) ? $_GET['additional_project_files'] : '';
$target_markets = (!empty($_GET['target_markets'])) ? $_GET['target_markets'] : '';
$increased_social_media_awareness = (!empty($_GET['increased_social_media_awareness'])) ? $_GET['increased_social_media_awareness'] : '';
$lower_acquisition_costs_by = (!empty($_GET['lower_acquisition_costs_by'])) ? $_GET['lower_acquisition_costs_by'] : '';
$locate_product_faster = (!empty($_GET['locate_product_faster'])) ? $_GET['locate_product_faster'] : '';
$compare_shopping_on = (!empty($_GET['compare_shopping_on'])) ? $_GET['compare_shopping_on'] : '';
$enable_deals_bargains_coupons_and_group_buys = (!empty($_GET['enable_deals_bargains_coupons_and_group_buys'])) ? $_GET['enable_deals_bargains_coupons_and_group_buys'] : '';
$create_market_for_products_to_achieve = (!empty($_GET['create_market_for_products_to_achieve'])) ? $_GET['create_market_for_products_to_achieve'] : '';
$additional_performances_important = (!empty($_GET['additional_performances_important'])) ? $_GET['additional_performances_important'] : '';
$performance_not_what_you_looking_for = (!empty($_GET['performance_not_what_you_looking_for'])) ? $_GET['performance_not_what_you_looking_for'] : '';
$ecommerce_development_deliverables = (!empty($_GET['ecommerce_development_deliverables'])) ? $_GET['ecommerce_development_deliverables'] : '';
$ecommerce_platforms = (!empty($_GET['ecommerce_platforms'])) ? $_GET['ecommerce_platforms'] : '';
$ecommerce_development_skills = (!empty($_GET['ecommerce_development_skills'])) ? $_GET['ecommerce_development_skills'] : '';
$ecommerce_payment_systems = (!empty($_GET['ecommerce_payment_systems'])) ? $_GET['ecommerce_payment_systems'] : '';
$ecommerce_database = (!empty($_GET['ecommerce_database'])) ? $_GET['ecommerce_database'] : '';
$business_size_experience = (!empty($_GET['business_size_experience'])) ? $_GET['business_size_experience'] : '';
$additional_features_are_important_to_you = (!empty($_GET['additional_features_are_important_to_you'])) ? $_GET['additional_features_are_important_to_you'] : '';
$features_not_what_you_looking_for = (!empty($_GET['features_not_what_you_looking_for'])) ? $_GET['features_not_what_you_looking_for'] : '';
$specific_budget = (!empty($_GET['specific_budget'])) ? $_GET['specific_budget'] : '';
$specific_timeframe_to_complete_min = (!empty($_GET['specific_timeframe_to_complete_min'])) ? $_GET['specific_timeframe_to_complete_min'] : '';
$specific_timeframe_to_complete_max = (!empty($_GET['specific_timeframe_to_complete_max'])) ? $_GET['specific_timeframe_to_complete_max'] : '';
$how_long_prepared_to_work_with_min = (!empty($_GET['how_long_prepared_to_work_with_min'])) ? $_GET['how_long_prepared_to_work_with_min'] : '';
$how_long_prepared_to_work_with_max = (!empty($_GET['how_long_prepared_to_work_with_max'])) ? $_GET['how_long_prepared_to_work_with_max'] : '';
$how_would_you_like_to_work_with = (!empty($_GET['how_would_you_like_to_work_with'])) ? $_GET['how_would_you_like_to_work_with'] : '';
$when_would_you_like_to_pay = (!empty($_GET['when_would_you_like_to_pay'])) ? $_GET['when_would_you_like_to_pay'] : '';

$contractors_email = (!empty($_GET['contractors_email'])) ? $_GET['contractors_email'] : '';
$message_to_contractors = (!empty($_GET['message_to_contractors'])) ? $_GET['message_to_contractors'] : '';
$allow_contractors_to_bid_other_projects = (!empty($_GET['allow_contractors_to_bid_other_projects'])) ? $_GET['allow_contractors_to_bid_other_projects'] : '';


    $database->insert("project", [
        "project_owner_user_id" => $_SESSION["user_id"],
        "project_name" => $project_name,
        "category" => $category,
        "category_specialty" => $category_specialty,
        "suggest_specialty" => $suggest_specialty,
        "description" => $description,
        "ideal_minimum_viable_product" => $ideal_minimum_viable_product,
        "ideal_minimum_viable_product_others" => $ideal_minimum_viable_product_others,
        "additional_project_files" => $additional_project_files,
        "target_markets" => $target_markets,
        "increased_social_media_awareness" => $increased_social_media_awareness,
        "lower_acquisition_costs_by" => $lower_acquisition_costs_by,
        "locate_product_faster" => $locate_product_faster,
        "compare_shopping_on" => $compare_shopping_on,
        "enable_deals_bargains_coupons_and_group_buys" => $enable_deals_bargains_coupons_and_group_buys,
        "create_market_for_products_to_achieve" => $create_market_for_products_to_achieve,
        "additional_performances_important" => $additional_performances_important,
        "performance_not_what_you_looking_for" => $performance_not_what_you_looking_for,
        "ecommerce_development_deliverables" => $ecommerce_development_deliverables,
        "ecommerce_platforms" => $ecommerce_platforms,
        "ecommerce_development_skills" => $ecommerce_development_skills,
        "ecommerce_payment_systems" => $ecommerce_payment_systems,
        "ecommerce_database" => $ecommerce_database,
        "business_size_experience" => $business_size_experience,
        "additional_features_are_important_to_you" => $additional_features_are_important_to_you,
        "features_not_what_you_looking_for" => $features_not_what_you_looking_for,
        "specific_budget" => $specific_budget,
        "specific_timeframe_to_complete_min" => $specific_timeframe_to_complete_min,
        "specific_timeframe_to_complete_max" => $specific_timeframe_to_complete_max,
        "how_long_prepared_to_work_with_min" => $how_long_prepared_to_work_with_min,
        "how_long_prepared_to_work_with_max" => $how_long_prepared_to_work_with_max,
        "how_would_you_like_to_work_with" => $how_would_you_like_to_work_with,
        "when_would_you_like_to_pay" => $when_would_you_like_to_pay,
        "contractors_email" => $contractors_email,
        "message_to_contractors" => $message_to_contractors,
        "allow_contractors_to_bid_other_projects" => $allow_contractors_to_bid_other_projects,
        "user_name" => $user_name,
        "user_email" => $user_email,
        "status" => "Proposed"
    ]);

    $database->insert("user_activity", [
        "user_id" => $_SESSION["user_id"],
        "activity_name" => "Created a Project",
        "activity_description" => "<em>".$user_name."</em> created <em>".$project_name."</em>"
    ]);

    header("Location: http://afarinsights.com/backend/user/projects.php?project_saved=true");
    //var_dump($database->log());

?>