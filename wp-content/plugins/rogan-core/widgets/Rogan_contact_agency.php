<?php
namespace RoganCore\Widgets;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Scheme_Typography;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Text_Shadow;



// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


/**
 * Text Typing Effect
 *
 * Elementor widget for text typing effect.
 *
 * @since 1.7.0
 */
class Rogan_contact_agency extends Widget_Base {

    public function get_name() {
        return 'rogan_contact_sections';
    }

    public function get_title() {
        return esc_html__( 'Contact Us', 'rogan-core' );
    }

    public function get_icon() {
        return 'eicon-mail';
    }

    public function get_categories() {
        return [ 'rogan-elements' ];
    }

    protected function _register_controls() {

        // ------------------------------ Map Settings ------------------------------
        $this->start_controls_section(
            'map_sec', [
                'label' => __( 'Map Settings', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'important_note',
            [
                'label' => esc_html__( 'Important Note', 'rogan-core' ),
                'type' => \Elementor\Controls_Manager::RAW_HTML,
                'raw' => esc_html__( 'Place this widget in a full width container of Elementor in the Elementor Full Width page template. ', 'rogan-core' ),
                'content_classes' => 'your-class',
            ]
        );

        $this->add_control(
            'map_api', [
                'label' => esc_html__( 'Map API Key', 'rogan-core' ),
                'description' => __( 'Get the Map API Key <a href="https://is.gd/qEYdtJ" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'lat', [
                'label' => esc_html__( 'Latitude', 'rogan-core' ),
                'description' => __( 'Get the Latitude <a href="https://is.gd/GDjonL" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'lng', [
                'label' => esc_html__( 'Longitude', 'rogan-core' ),
                'description' => __( 'Get the Longitude <a href="https://is.gd/GDjonL" target="_blank">here</a>', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
            ]
        );

        $this->add_control(
            'content', [
                'label' => esc_html__( 'Content', 'rogan-core' ),
                'description' => esc_html__( 'You can keep here the address with your logo.', 'rogan-core' ),
                'type' => Controls_Manager::WYSIWYG,
                'label_block' => true,
            ]
        );

        $this->end_controls_section(); // End map section



        // ***************************** Conact Form  **************************************//
        $this->start_controls_section(
            'contact_sec', [
                'label' => esc_html__( 'Contact Form Shortcode', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'cf7_shortcode',
            [
                'label' => esc_html__( 'Contact Form 7 Shortcode', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'description' => esc_html__( 'Copy the contact form 7 shortcode of which contact form you want and then paste here the shortcode. ', 'rogan-core' ),
                'label_block' => true
            ]
        );

        $this->end_controls_section();

        // ***************************** Conact Info  **************************************//
        $this->start_controls_section(
            'contact_info', [
                'label' => esc_html__( 'Contact Information', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'heading_title',
            [
                'label' => esc_html__( 'Contact Heading', 'rogan-core' ),
                'type' => Controls_Manager::TEXTAREA,
                'label_block' => true,
                'default' => 'Don’t Hesitate to contact with us for any kind of information',
            ]
        );        


        $this->add_control(
            'heading_title_color', [
                'label' => esc_html__( 'Heading Color', 'apploye-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .contact-us-section .contact-info .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_heading_title',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .contact-us-section .contact-info .title',
            ]
        );


        $this->add_control(
            'hr1',
            [
                'type' => \Elementor\Controls_Manager::DIVIDER,
            ]
        );


        $this->add_control(
            'contact_text',
            [
                'label' => esc_html__( 'Contact Info Text', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => 'Call us for imidiate support to this number',
            ]
        );

        $this->add_control(
            'contact_number',
            [
                'label' => esc_html__( 'Conact Phone Number', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '088 130 629 8615',
            ]
        );

        $this->add_control(
            'text_color', [
                'label' => esc_html__( 'Text Color', 'apploye-core' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .contact-us-section .contact-info p' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(), [
                'name' => 'typography_text',
                'scheme' => Scheme_Typography::TYPOGRAPHY_1,
                'selector' => '{{WRAPPER}} .contact-us-section .contact-info p',
            ]
        );

        $this->end_controls_section();



        // *************************************** Social Profile List *************************************//
        $this->start_controls_section(
            'social_icon_list',
            [
                'label' => esc_html__( 'Social Profile List', 'rogan-core' ),
            ]
        );

        $this->add_control(
            'f_btn_url', [
                'label' => esc_html__( 'Facebook URL', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '#',
            ]
        );

        $this->add_control(
            't_btn_url', [
                'label' => esc_html__( 'Twitter URL', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '#',
            ]
        );

        $this->add_control(
            'i_btn_url', [
                'label' => esc_html__( 'Instagram URL', 'rogan-core' ),
                'type' => Controls_Manager::TEXT,
                'label_block' => true,
                'default' => '#',
            ]
        );

        $this->end_controls_section();



        // ---------------------------------------- Section Style and Choice ------------------------------
        $this->start_controls_section(
            'contact_page_style',
            [
                'label' => __( 'Select Page Style', 'apploye-core' ),
            ]
        );

        $this->add_control(
            'page_structure', [
                'label' => esc_html__( 'Section Structure', 'apploye-core' ),
                'type' => Controls_Manager::SELECT,
                'label_block' => true,
                'options' => [
                    'style_01' => esc_html__('Contact Us Standared', 'apploye-core'),
                    'style_02' => esc_html__('Contact Us Agency', 'apploye-core'),
                ],
                'default' => 'style_01'
            ]
        );
        
        $this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings();
        $map_content = !empty($settings['content']) ? wp_kses_post($settings['content']) : '';
       
       if ( $settings['page_structure'] == 'style_01' ) { ?>
           
            <div class="contact-us-section contact-agency pt-150 mb-200">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="contact-form">
                                <?php echo !empty($settings['cf7_shortcode']) ? do_shortcode($settings['cf7_shortcode']) : ''; ?>
                            </div>
                        </div> <!-- /.col- -->

                        <div class="col-lg-6">
                            <div class="contact-info">
                                <?php if (!empty($settings['heading_title'])) : ?>
                                   <h2 class="title"><?php echo esc_html($settings['heading_title']) ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($settings['contact_text'])) : ?>
                                    <p><?php echo esc_html($settings['contact_text']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['contact_number'])) : ?>
                                    <a href="#" class="call"><?php echo esc_html($settings['contact_number']) ?></a>
                                <?php endif; ?>
                                
                                <ul>
                                    <?php if (!empty($settings['f_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['f_btn_url'] ) ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                    <?php if (!empty($settings['t_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['t_btn_url'] ) ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                    <?php if (!empty($settings['i_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['i_btn_url'] ) ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                </ul>

                            </div> <!-- /.contact-info -->
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.contact-us-section --> 

            <?php if (!empty ( $settings['map_api'] ) && !empty ( $settings['lat'] ) ) : ?>
                <div id="google-map"><div class="map-canvas"></div></div>

                <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr($settings['map_api']) ?>" type="text/javascript"></script>
                <script src="<?php echo plugins_url( 'js/sanzzy-map/dist/snazzy-info-window.min.js', __FILE__ ) ?>"></script>

                <script>
                    (function ( $ ) {
                        "use strict";

                            $(function() {
                                var mapStyle = [{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}];

                                // Create the map
                                var map = new google.maps.Map($('.map-canvas')[0], {
                                    zoom: 14,
                                    styles: mapStyle,
                                    scrollwheel: false,
                                    center: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                                });

                                // Add a marker
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                                });

                                // Add a Snazzy Info Window to the marker
                                var info = new SnazzyInfoWindow({
                                    marker: marker,
                                    content: '<?php echo $map_content ?>',
                                    closeOnMapClick: false
                                });
                                info.open();
                            });

                    }( jQuery ));

                </script>
            <?php endif; ?>
        <?php 
        } 
        elseif ($settings['page_structure'] == 'style_02') { ?>
           
            <?php if (!empty ( $settings['map_api'] ) && !empty ( $settings['lat'] ) ) : ?>
                <div id="google-map-two"><div class="map-canvas"></div></div>

                <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo esc_attr($settings['map_api']) ?>" type="text/javascript"></script>
                <script src="<?php echo plugins_url( 'js/sanzzy-map/dist/snazzy-info-window.min.js', __FILE__ ) ?>"></script>

                <script>
                    (function ( $ ) {
                        "use strict";

                            $(function() {
                                var mapStyle = [{featureType:"water",elementType:"geometry",stylers:[{color:"#e9e9e9"},{lightness:17}]},{featureType:"landscape",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:20}]},{featureType:"road.highway",elementType:"geometry.fill",stylers:[{color:"#ffffff"},{lightness:17}]},{featureType:"road.highway",elementType:"geometry.stroke",stylers:[{color:"#ffffff"},{lightness:29},{weight:.2}]},{featureType:"road.arterial",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:18}]},{featureType:"road.local",elementType:"geometry",stylers:[{color:"#ffffff"},{lightness:16}]},{featureType:"poi",elementType:"geometry",stylers:[{color:"#f5f5f5"},{lightness:21}]},{featureType:"poi.park",elementType:"geometry",stylers:[{color:"#dedede"},{lightness:21}]},{elementType:"labels.text.stroke",stylers:[{visibility:"on"},{color:"#ffffff"},{lightness:16}]},{elementType:"labels.text.fill",stylers:[{saturation:36},{color:"#333333"},{lightness:40}]},{elementType:"labels.icon",stylers:[{visibility:"off"}]},{featureType:"transit",elementType:"geometry",stylers:[{color:"#f2f2f2"},{lightness:19}]},{featureType:"administrative",elementType:"geometry.fill",stylers:[{color:"#fefefe"},{lightness:20}]},{featureType:"administrative",elementType:"geometry.stroke",stylers:[{color:"#fefefe"},{lightness:17},{weight:1.2}]}];

                                // Create the map
                                var map = new google.maps.Map($('.map-canvas')[0], {
                                    zoom: 14,
                                    styles: mapStyle,
                                    scrollwheel: false,
                                    center: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                                });

                                // Add a marker
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: new google.maps.LatLng(<?php echo esc_js($settings['lat']) ?>, <?php echo esc_js($settings['lng']) ?>)
                                });

                                // Add a Snazzy Info Window to the marker
                                var info = new SnazzyInfoWindow({
                                    marker: marker,
                                    content: '<?php echo $map_content ?>',
                                    closeOnMapClick: false
                                });
                                info.open();
                            });

                    }( jQuery ));

                </script>
            <?php endif; ?>

            <div class="contact-us-section contact-agency pt-150 mb-200">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="contact-form">
                                <?php echo !empty($settings['cf7_shortcode']) ? do_shortcode($settings['cf7_shortcode']) : ''; ?>
                            </div>
                        </div> <!-- /.col- -->

                        <div class="col-lg-6">
                            <div class="contact-info">
                                <?php if (!empty($settings['heading_title'])) : ?>
                                   <h2 class="title"><?php echo esc_html($settings['heading_title']) ?></h2>
                                <?php endif; ?>
                                <?php if (!empty($settings['contact_text'])) : ?>
                                    <p><?php echo esc_html($settings['contact_text']) ?></p>
                                <?php endif; ?>
                                <?php if (!empty($settings['contact_number'])) : ?>
                                    <a href="#" class="call"><?php echo esc_html($settings['contact_number']) ?></a>
                                <?php endif; ?>
                                
                                <ul>
                                    <?php if (!empty($settings['f_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['f_btn_url'] ) ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                    <?php if (!empty($settings['t_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['t_btn_url'] ) ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                    <?php if (!empty($settings['i_btn_url'])) : ?>
                                    <li><a href="<?php echo esc_url( $settings['i_btn_url'] ) ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <?php endif; ?>
                                </ul>

                            </div> <!-- /.contact-info -->
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.contact-us-section --> 
        <?php 
        }
    }

}