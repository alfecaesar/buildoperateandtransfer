(function ( $ ) {
    "use strict";

    $(window).on ('load', function (){ // makes sure the whole site is loaded

        // ------------------------------- AOS Animation
        if ($("[data-aos]").length) {
            AOS.init({
                duration: 1000,
                mirror: true
            });
        }

        // ------------------------------- WOW Animation
        if ($(".wow").length) {
            var wow = new WOW({
                boxClass:     'wow',      // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset:       20,          // distance to the element when triggering the animation (default is 0)
                mobile:       true,       // trigger animations on mobile devices (default is true)
                live:         true,       // act on asynchronously loaded content (default is true)
            });
            wow.init();
        }

    });  //End On Load Function

}( jQuery ));