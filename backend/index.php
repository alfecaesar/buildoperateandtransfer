<?php include 'template/header.php';

include 'dbconfig.php';

$access_type = (!empty($_GET['access_type'])) ? $_GET['access_type'] : 'user';

?>

<?php include 'template/topbar.php'; ?>


<div id="login-section" class="container">
    <div class="row">
        <div class="col-12">
        <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Login</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">
                    <?php 
                        $login_error = (!empty($_GET['login_error'])) ? $_GET['login_error'] : '';
                            if ($login_error === 'true') { ?> 
                            <div class="alert alert-danger" role="alert">
                                Wrong username/password combination!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <?php 
                        $logout = (!empty($_GET['logout'])) ? $_GET['logout'] : '';
                            if ($logout === 'true') { ?> 
                            <div class="alert alert-primary" role="alert">
                                User Successfully Logout!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <form method="post" name="login_form" id="login_form" action="login.php">
                        <div class="form-group  no-border">
                            <div class="form-group-label">
                                <label for="user_email">Email Address</label>
                            </div>
                            <div class="form-group-tag">
                                <input type="text" name="user_email" id="user_email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group no-border">
                            <div class="form-group-label">
                                <label for="user_password">Password</label>
                            </div>
                            <div class="form-group-tag">
                                <input type="password" name="user_password" id="user_password" class="form-control">
                            </div>
                        </div>

                        <div class="form-group no-border">
                            <div class="form-group-tag">
                                <input type="submit" name="frm_submit" id="frm_submit" class="form-control btn" value="LOGIN">
                            </div>
                        </div>
                    </form>
                </div>

                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Register</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                </div>

                <div class="content_col_box_container">
                    <?php 
                        $user_saved = (!empty($_GET['user_saved'])) ? $_GET['user_saved'] : '';
                            if ($user_saved === 'true') { ?> 
                            <div class="alert alert-primary" role="alert">
                                User Successfully Created! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <?php 
                        $register_error = (!empty($_GET['register_error'])) ? $_GET['register_error'] : '';
                            if ($register_error === 'true') { ?> 
                            <div class="alert alert-danger" role="alert">
                                User already exist! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <?php 
                        $register_field_error = (!empty($_GET['register_field_error'])) ? $_GET['register_field_error'] : '';
                            if ($register_field_error === 'true') { ?> 
                            <div class="alert alert-danger" role="alert">
                                Please complete all fields! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <form method="post" name="register_form" id="register_form" action="register.php">
                        <div class="form-group  no-border">
                            <div class="form-group-label">
                                <label for="user_name">Full Name</label>
                            </div>
                            <div class="form-group-tag">
                                <input type="text" name="user_name" id="user_name" class="form-control">
                            </div>
                        </div>

                        <div class="form-group no-border">
                            <div class="form-group-label">
                                <label for="user_email">Email Address</label>
                            </div>
                            <div class="form-group-tag">
                                <input type="text" name="user_email" id="user_email" class="form-control">
                            </div>
                        </div>

                        <div class="form-group no-border">
                            <div class="form-group-label">
                                <label for="user_password">Password</label>
                            </div>
                            <div class="form-group-tag">
                                <input type="password" name="user_password" id="user_password" class="form-control">
                            </div>
                        </div>

                        <div class="form-group no-border">
                            <div class="form-group-tag">
                                <input type="submit" name="frm_submit" id="frm_submit" class="form-control btn" value="REGISTER">
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo $access_type; ?>" name="access_type" id="access_type">
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<?php include 'template/footer.php'; ?>