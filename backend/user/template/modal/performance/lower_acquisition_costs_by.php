<!-- Modal -->
<div class="modal fade" id="modalbox_performance_3" tabindex="-1" role="dialog" aria-labelledby="Lower Acquisition Costs by" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Lower Acquisition Costs by</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Lower Acquisition Costs by</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="lower_acquisition_costs_by" id="lower_acquisition_costs_by" class="form-control" multiple>
                            <option id="Advertising on Social Media">Advertising on Social Media</option>
                            <option id="Affiliata Marketing">Affiliata Marketing</option>
                            <option id="Influencer Marketing">Influencer Marketing</option>
                            <option id="Social Media Management">Social Media Management</option>
                            <option id="Content Marketing">Content Marketing</option>
                            <option id="Email Marketing">Email Marketing</option>
                            <option id="Paid Media">Paid Media</option>
                            <option id="PPC Marketing ">PPC Marketing </option>
                            <option id="Search Engine Marketing (SEM)">Search Engine Marketing (SEM)</option>
                            <option id="Marketing Automation">Marketing Automation</option>
                            <option id="Fulfilment Automation">Fulfilment Automation</option>
                            <option id="Inventory Management">Inventory Management</option>
                            <option id="Eliminating Physical Stores">Eliminating Physical Stores</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>