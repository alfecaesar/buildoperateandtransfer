<?php include '../bot/template/header.php';

include '../dbconfig.php';

?>

<?php include '../bot/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../bot/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Account</h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)" onclick="document.update_account_form.submit()" class="btn btn-yellow"><span>Save Changes <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <?php 
                        $user_saved = (!empty($_GET['user_saved'])) ? $_GET['user_saved'] : '';
                            if ($user_saved === 'true') { ?> 
                            <div class="alert alert-primary" role="alert">
                                User Successfully Updated! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <?php 
                        $update_field_error = (!empty($_GET['update_field_error'])) ? $_GET['update_field_error'] : '';
                            if ($update_field_error === 'true') { ?> 
                            <div class="alert alert-danger" role="alert">
                                Please complete all fields! 
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                    <?php } ?>

                    <form name="update_account_form" action="update_account.php" method="post">
                        <div class="form-group">
                            <div class="form-group-label">
                                <label for="project_name">Full Name</label>
                            </div>
                            <div class="form-group-tag">
                                <input value="<?php echo $_SESSION["user_name"]; ?>" type="text" name="user_name" id="user_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="form-group-label">
                                <label for="project_name">Email Address</label>
                            </div>
                            <div class="form-group-tag">
                                <input value="<?php echo $_SESSION["user_email"]; ?>" type="text" name="user_email" id="user_email" class="form-control">
                            </div>
                        </div>
                        <div class="form-group no-border">
                            <div class="form-group-label">
                                <label for="project_name">Password</label>
                            </div>
                            <div class="form-group-tag">
                                <input value="<?php echo $_SESSION["user_password"]; ?>" type="password" name="user_password" id="user_password" class="form-control">
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>

<?php include '../bot/template/footer.php';?>