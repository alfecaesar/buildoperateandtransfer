<?php
Redux::setSection( 'rogan_opt', array(
    'title'            => esc_html__( 'Preloader Settings', 'rogan' ),
    'id'               => 'preloader_opt',
    'icon'             => 'dashicons dashicons-controls-repeat',
    'fields'           => array(

        array(
            'id'      => 'is_preloader',
            'type'    => 'switch',
            'title'   => esc_html__( 'Pre-loader', 'rogan' ),
            'on'      => esc_html__( 'Enable', 'rogan' ),
            'off'     => esc_html__( 'Disable', 'rogan' ),
            'default' => true,
        ),

        array(
            'required' => array( 'is_preloader', '=', '1' ),
            'id'       => 'preloader_style',
            'type'     => 'select',
            'title'    => esc_html__( 'Pre-loader Style', 'rogan' ),
            'default'   => 'text',
            'options'  => array(
                'text'  => esc_html__( 'Text Preloader', 'rogan' ),
                'image' => esc_html__( 'Image Preloader', 'rogan' )
            )
        ),

        /**
         * Text Preloader
         */
        array(
            'required' => array( 'preloader_style', '=', 'text' ),
            'id'       => 'preloader_text',
            'type'     => 'text',
            'title'    => esc_html__( 'Pre-loader Text', 'rogan' ),
            'default'  => get_bloginfo( 'name' )
        ),

        array(
            'title'     => esc_html__( 'Spinner Color', 'rogan' ),
            'id'        => 'preloader_spinner_color',
            'type'      => 'color_rgba',
            'output'    => array(
                'border-color' => '.ctn-preloader .animation-preloader .spinner',
            ),
            'required'  => array( 'preloader_style', '=', 'text' ),
        ),

        array(
            'title'     => esc_html__( 'Color', 'rogan' ),
            'id'        => 'preloader_color',
            'type'      => 'color',
            'output'    => array(
                'color' => '.ctn-preloader .animation-preloader .txt-loading .letters-loading:before',
                'border-top-color' => '.ctn-preloader .animation-preloader .spinner',
            ),
            'required'  => array( 'preloader_style', '=', 'text' ),
        ),

        array(
            'title'     => esc_html__( 'Shadow Color', 'rogan' ),
            'id'        => 'preloader_shadow_color',
            'type'      => 'color_rgba',
            'output'    => array( '.ctn-preloader .animation-preloader .txt-loading .letters-loading' ),
            'required'  => array( 'preloader_style', '=', 'text' ),
        ),
        array(
            'title'         => esc_html__( 'Typography', 'rogan' ),
            'id'            => 'preloader_typo',
            'type'          => 'typography',
            'text-align'    => false,
            'color'         => false,
            'output'        => '.ctn-preloader .animation-preloader .txt-loading',
            'required'      => array( 'preloader_style', '=', 'text' ),
        ),

        array(
            'id'       => 'loading_text',
            'type'     => 'text',
            'title'    => esc_html__( 'Loading Text', 'rogan' ),
            'default'  => esc_html__( 'Loading', 'rogan' ),
            'required' => array( 'preloader_style', '=', 'text' ),
        ),

        array(
            'title'         => esc_html__( 'Loading Text Typography', 'rogan' ),
            'id'            => 'preloader_small_typo',
            'type'          => 'typography',
            'text-align'    => false,
            'output'        => '.ctn-preloader p',
            'required' => array( 'preloader_style', '=', 'text' ),
        ),

        /**
         * Image Preloader
         */
        array(
            'required' => array( 'preloader_style', '=', 'image' ),
            'id'       => 'preloader_image',
            'type'     => 'media',
            'title'    => esc_html__( 'Pre-loader image', 'rogan' ),
            'compiler' => true,
            'default'  => array(
                'url' => ROGAN_DIR_IMG.'/status.gif'
            )
        ),
    )
));