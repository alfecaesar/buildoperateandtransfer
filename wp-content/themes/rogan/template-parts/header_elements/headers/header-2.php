<?php
$opt = get_option( 'rogan_opt' );
// Button Settings
$is_menu_btn = !empty($opt['is_menu_btn']) ? $opt['is_menu_btn'] : '';
$menu_btn_title = !empty($opt['menu_btn_label']) ? $opt['menu_btn_label'] : '';
$menu_btn_url = !empty($opt['menu_btn_url']) ? $opt['menu_btn_url'] : '';
?>

<div class="theme-main-menu theme-menu-two">
    <div class="logo">
        <?php rogan_logo() ?>
    </div>
    <nav id="mega-menu-holder" class="navbar navbar-expand-lg">
        <div class="position-relative ml-auto nav-container">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="flaticon-setup"></i>
            </button>
            <?php
            if(has_nav_menu( 'main_menu' )) {
                wp_nav_menu( array(
                    'menu' => 'main_menu',
                    'theme_location' => 'main_menu',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'navbarSupportedContent',
                    'menu_class' => 'navbar-nav',
                    'walker' => new Rogan_Nav_Navwalker(),
                    'depth' => 3
                ));
            }
            ?>
        </div> <!-- /.container -->
    </nav> <!-- /#mega-menu-holder -->

    <?php
    if(!empty($menu_btn_title) & $is_menu_btn == '1' ) :  ?>
        <a href="<?php echo esc_url($menu_btn_url); ?>" class="quote-button">
            <?php echo esc_html($menu_btn_title); ?>
        </a>
    <?php endif; ?>

</div>