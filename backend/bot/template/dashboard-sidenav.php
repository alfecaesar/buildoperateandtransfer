<?php $pathname = $_SERVER['REQUEST_URI']; ?>

<nav id="side-nav">
    <ul>
<li><a href="/backend/bot/" id="tab_side_a_1" <?php if ($pathname === '/backend/bot/') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Dashboard</span></a></li>
        <li><a href="/backend/bot/bot_opportunities.php" id="tab_side_a_2" <?php if ($pathname === '/backend/bot/bot_opportunities.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> BOT Opportunities</span></a></li>
    
        <li><a href="/backend/bot/payments.php" id="tab_side_a_3" <?php if ($pathname === '/backend/bot/payments.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Payments</span></a></li>
        <li><a href="/backend/bot/account.php" id="tab_side_a_4" <?php if ($pathname === '/backend/bot/account.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Account</span></a></li>
        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_logout" id="tab_side_a_5" <?php if ($pathname === '/backend/bot/logout.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Logout</span></a></li>
    </ul>
</nav>

