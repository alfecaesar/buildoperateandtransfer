(function ( $ ) {
    "use strict";
    $(document).on ( 'ready', function (){

        $( '.ajax_add_to_cart.added span i' ).removeClass( 'fa-shopping-cart' ).addClass( 'fa-eye' );

        $( '.app-menu .navbar-nav .login-button-two' ).removeClass( 'nav-item' );
        $( '.app-menu .navbar-nav .login-button-two a' ).attr( 'class', '' );

        $( '.row p' ).removeClass( 'form-row' );
        $( '.user-profile-data .row p select' ).addClass( 'theme-select-menu' );

        $.fn.inlineStyle = function (prop) {
            var styles = this.attr("style"),
                value;

            styles && styles.split(";").forEach(function (e) {
                var style = e.split(":");
                if ($.trim(style[0]) === prop) {
                    value = style[1];
                }
            });
            return value;
        };

        if ( $( '.wp-block-quote' ).inlineStyle( 'text-align' ) == 'right' ) {
            $( '.wp-block-quote' ).addClass( 'block-quote-right' );
        }

    }); //End Window Ready Function
    $(window).on ( 'load', function (){ // makes sure the whole site is loaded

        // -------------------- Site Preloader
        $( '#ctn-preloader' ).fadeOut(); // will first fade out the loading animation
        $( '#preloader' ).delay(350).fadeOut( 'slow' ); // will fade out the white DIV that covers the website.
        $( 'body' ).delay(350).css({'overflow':'visible'});

    });  //End On Load Function

    /*------------- preloader js --------------*/
    function loader() {
        $(window).on( 'load', function () {
            $( '#ctn-preloader' ).addClass( 'loaded' );
            // Una vez haya terminado el preloader aparezca el scroll

            if ($( '#ctn-preloader' ).hasClass( 'loaded' )) {
                // Es para que una vez que se haya ido el preloader se elimine toda la seccion preloader
                $( '#preloader' ).delay(900).queue(function () {
                    $(this).remove();
                });
            }
        });
    }
    loader();

}( jQuery ));