<div class="arch-blog row">
    <?php
    $i = 0;
    while ($blog_posts->have_posts()) : $blog_posts->the_post();
        if ( !has_post_thumbnail() ) {
            $i++;
            continue;
        }
        ?>
        <div class="col-lg-<?php echo esc_attr( $settings['column'] ) ?> col-md-6">
            <div class="blog-post">
                <?php the_post_thumbnail( 'rogan_370x225', array( 'class' => 'img' ) );
                ?>
                <div class="post">
                    <a href="<?php the_permalink() ?>" class="date font-lato">
                        <?php echo get_the_date(get_option('date_format')) ?>
                    </a>
                    <h4 class="title">
                        <a href="<?php the_permalink() ?>">
                            <?php the_title() ?>
                        </a>
                    </h4>
                    <?php if ( !empty( $settings['read_more_btn'] ) ) : ?>
                        <a href="<?php the_permalink() ?>" class="line-button-three read-more">
                            <?php echo esc_html($settings['read_more_btn']) ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php
    ++$i;
    endwhile;
    wp_reset_postdata();
    ?>
</div>