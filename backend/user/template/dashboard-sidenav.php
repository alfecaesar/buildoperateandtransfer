<?php $pathname = $_SERVER['REQUEST_URI']; ?>

<nav id="side-nav">
    <ul>
<li><a href="/backend/user/" id="tab_side_a_1" <?php if ($pathname === '/backend/user/') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Dashboard</span></a></li>
        <li><a href="/backend/user/projects.php" id="tab_side_a_2" <?php if ($pathname === '/backend/user/projects.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Projects</span></a></li>
        <li><a href="/backend/user/payments.php" id="tab_side_a_3" <?php if ($pathname === '/backend/user/payments.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Payments</span></a></li>
        <li><a href="/backend/user/account.php" id="tab_side_a_4" <?php if ($pathname === '/backend/user/account.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Account</span></a></li>
        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modalbox_logout" id="tab_side_a_5" <?php if ($pathname === '/backend/user/logout.php') { ?>class="active"<?php } ?>><span><i class="fas fa-arrow-alt-circle-right"></i> Logout</span></a></li>
    </ul>
</nav>

