<?php

session_start();

include '../dbconfig.php';

$user_id = (!empty($_POST['user_id'])) ? $_POST['user_id'] : '';
$project_id = (!empty($_POST['project_id'])) ? $_POST['project_id'] : '';
$title = (!empty($_POST['title'])) ? $_POST['title'] : '';
$description = (!empty($_POST['description'])) ? $_POST['description'] : '';
$estimated_time = (!empty($_POST['estimated_time'])) ? $_POST['estimated_time'] : '';
$due_date = (!empty($_POST['due_date'])) ? $_POST['due_date'] : '';
$status = (!empty($_POST['status'])) ? $_POST['status'] : '';
$assigned_to = (!empty($_POST['assigned_to'])) ? $_POST['assigned_to'] : '';
$milestone_id = (!empty($_POST['milestone_id'])) ? $_POST['milestone_id'] : '';



    $database->insert("task_list", [
        "user_id" => $user_id,
        "project_id" => $project_id,
        "project_owner_user_id" => $database->get("project", "project_owner_user_id", [ "project_id" => $project_id ]),
        "title" => $title,
        "description" => $description,
        "estimated_time" => $estimated_time,
        "due_date" => $due_date,
        "status" => $status,
        "assigned_to" => $assigned_to,
        "milestone_id" => $milestone_id
    ]);

    $project_name = $database->get("project", "project_name", [ "project_id" => $project_id ]);

    $database->insert("user_activity", [
        "user_id" => $_SESSION["user_id"],
        "activity_name" => "Assigned a Task",
        "activity_description" => "<em>".$_SESSION["user_name"]."</em> assigned task to <em>".$assigned_to."</em> for <em>".$project_name."</em>"
    ]);

    header("Location: http://afarinsights.com/backend/user/project_detail.php?project_id=".$project_id."&task_saved=true#taskList");
    //var_dump($database->log());

?>