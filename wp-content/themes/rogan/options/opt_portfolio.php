<?php

Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Portfolio Settings', 'rogan' ),
	'id'        => 'portfolio_settings',
	'icon'      => 'dashicons dashicons-images-alt2',
	'fields'    => array(
		array(
			'title'     => esc_html__( 'Default Layout', 'rogan' ),
			'subtitle'  => esc_html__( 'Select the default portfolio layout for portfolio details page', 'rogan' ),
			'id'        => 'portfolio_layout',
			'type'      => 'select',
			'default'   => 'agency',
            'options'   => array(
                'standard' => esc_html__( 'Standard', 'rogan' ),
                'agency' => esc_html__( 'Agency', 'rogan' ),
                'business' => esc_html__( 'Business', 'rogan' ),
                'creative' => esc_html__( 'Creative', 'rogan' ),
            )
		),
    )
));


// Portfolio Share Options
Redux::setSection( 'rogan_opt', array(
    'title'     => esc_html__( 'Portfolio Single', 'rogan' ),
    'id'        => 'portfolio_share_opt',
    'icon'      => '',
    'subsection'=> true,
    'fields'    => array(

        array(
            'id'        => 'is_related_portfolio',
            'title'     => esc_html__( 'Related Portfolio', 'rogan' ),
            'type'      => 'switch',
            'default'   => true,
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
        ),

        array(
            'id'        => 'related_portfolio_title',
            'type'      => 'text',
            'title'     => esc_html__( 'Section Title', 'rogan' ),
            'subtitle'  => esc_html__( 'Related Portfolio Section Title', 'rogan' ),
            'default'   => esc_html__( 'Related Project', 'rogan' ),
            'required'  => array( 'is_related_portfolio', '=', '1' )
        ),

        array(
            'id'        => 'portfolio_related_posts_count',
            'type'      => 'slider',
            'title'     => esc_html__( 'Post Show Count', 'rogan' ),
            "default"   => 4,
            "min"       => 1,
            "step"      => 1,
            "max"       => 20,
            'display_value' => 'text',
            'required'      => array( 'is_related_portfolio', '=', '1' )
        ),

        array(
            'id'        => 'portfolio_title_word_limit',
            'type'      => 'slider',
            'title'     => esc_html__( 'Portfolio Title Word Limit', 'rogan' ),
            "default"   => 2,
            "min"       => 1,
            "step"      => 1,
            "max"       => 20,
            'display_value' => 'text',
            'required'      => array( 'is_related_portfolio', '=', '1' )
        ),

        array(
            'id' => 'portfolio_share_start',
            'type' => 'section',
            'title' => esc_html__( 'Share Options', 'rogan' ),
            'subtitle' => esc_html__( 'Enable/Disable social media share options as you want.', 'rogan' ),
            'indent' => true
        ),

        array(
            'id'       => 'share_options',
            'title'    => esc_html__( 'Share Options', 'rogan' ),
            'type'     => 'switch',
            'default'  => true,
            'on'       => esc_html__( 'Enable', 'rogan' ),
            'off'      => esc_html__( 'Disable', 'rogan' ),
        ),

        array(
            'id'       => 'share_title',
            'type'     => 'text',
            'title'    => esc_html__( 'Title', 'rogan' ),
            'default'  => esc_html__( 'Share on', 'rogan' ),
            'required' => array( 'share_options','=','1' ),
        ),
        array(
            'id'       => 'is_portfolio_fb',
            'type'     => 'switch',
            'title'    => esc_html__( 'Facebook', 'rogan' ),
            'default'  => true,
            'required' => array( 'share_options','=','1' ),
            'on'       => esc_html__( 'Show', 'rogan' ),
            'off'      => esc_html__( 'Hide', 'rogan' ),
        ),
        array(
            'id'       => 'is_portfolio_twitter',
            'type'     => 'switch',
            'title'    => esc_html__( 'Twitter', 'rogan' ),
            'default'  => true,
            'required' => array( 'share_options','=','1' ),
            'on'       => esc_html__( 'Show', 'rogan' ),
            'off'      => esc_html__( 'Hide', 'rogan' ),
        ),
        array(
            'id'       => 'is_portfolio_googleplus',
            'type'     => 'switch',
            'title'    => esc_html__( 'Google Plus', 'rogan' ),
            'default'  => true,
            'required' => array( 'share_options','=','1' ),
            'on'       => esc_html__( 'Show', 'rogan' ),
            'off'      => esc_html__( 'Hide', 'rogan' ),
        ),
        array(
            'id'       => 'is_portfolio_linkedin',
            'type'     => 'switch',
            'title'    => esc_html__( 'Linkedin', 'rogan' ),
            'required' => array( 'share_options','=','1' ),
            'on'       => esc_html__( 'Show', 'rogan' ),
            'off'      => esc_html__( 'Hide', 'rogan' ),
        ),
        array(
            'id'       => 'is_portfolio_pinterest',
            'type'     => 'switch',
            'title'    => esc_html__( 'Pinterest', 'rogan' ),
            'required' => array( 'share_options','=','1' ),
            'on'       => esc_html__( 'Show', 'rogan' ),
            'off'      => esc_html__( 'Hide', 'rogan' ),
        ),

        array(
            'id'     => 'portfolio_share_end',
            'type'   => 'section',
            'indent' => false,
        ),
    )
));

