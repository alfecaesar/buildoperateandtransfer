<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package rogan
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>
<?php if ( have_comments() ) : ?>
    <div class="user-comment-area pt-50">
            <h3 class="inner-block-title"> <?php rogan_comment_count(get_the_ID(), '' ) ?> </h3>
            <div class="comment-wrapper">
                <?php
                wp_list_comments(
                    array(
                        'style'      => 'div',
                        'short_ping' => true,
                        'callback'	 => 'rogan_comments'
                    ),
                    get_comments(array(
                        'post_id' => get_the_ID(),
                    ))
                );
                the_comments_navigation();
                ?>
            </div> <!-- /.comment-wrapper -->
    </div>
<?php endif; ?>

    <div class="comment-form-area pt-150">
        <?php
        $is_comment_row_start = (is_user_logged_in()) ? '<div class="row">' : '';
        $is_comment_row_end = (is_user_logged_in()) ? '</div>' : '';
        $commenter      = wp_get_current_commenter();
        $req            = get_option( 'require_name_email' );
        $aria_req       = ( $req ? " aria-required='true'" : '' );
        $fields =  array(
            'author' => '<div class="col-md-6"> <input type="text" name="author" id="name" value="'.esc_attr($commenter['comment_author']).'" placeholder="'.esc_attr__( 'Name *', 'rogan' ).'" '.$aria_req.'> </div>',
            'email'	=> '<div class="col-md-6"> <input type="email" name="email" id="email" value="'.esc_attr($commenter['comment_author_email']).'" placeholder="'.esc_attr__( 'Email *', 'rogan' ).'" '.$aria_req.'> </div>',
        );
        $comments_args = array(
            'fields'                => apply_filters( 'comment_form_default_fields', $fields ),
            'class_submit'          => 'theme-button-two',
            'title_reply_before'    => '<h3 class="inner-block-title">',
            'title_reply'           => esc_html__( 'Leave a Comment', 'rogan' ),
            'title_reply_after'     => '</h3>',
            'comment_notes_before'  => '<div class="row">',
            'comment_field'         => $is_comment_row_start.'<div class="col-12"><textarea name="comment" id="comment" placeholder="'.esc_attr__( 'Your Comment', 'rogan' ).'"></textarea></div>'.$is_comment_row_end,
            'comment_notes_after'   => '</div>',
        );
        comment_form($comments_args);
        ?>

<?php echo ( is_user_logged_in() ) ? '' : '</div>'; ?>