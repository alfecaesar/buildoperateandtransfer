<!-- Modal -->
<div class="modal fade" id="modalbox_features_4" tabindex="-1" role="dialog" aria-labelledby="eCommerce Payment Systems" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">eCommerce Payment Systems</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add eCommerce Payment Systems</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="ecommerce_payment_systems" id="ecommerce_payment_systems" class="form-control" multiple>
                            <option id="PayPal">PayPal</option>
                            <option id="Stripe">Stripe</option>
                            <option id="Braintree">Braintree</option>
                            <option id="WePay">WePay</option>
                            <option id="Wordlpay">Wordlpay</option>
                            <option id="Authorize.Net">Authorize.Net</option>
                            <option id="2Checkout">2Checkout</option>
                            <option id="Sage Pay">Sage Pay</option>
                            <option id="Payoneer">Payoneer</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>