<?php
/**
 * Plugin Name: Rogan Core
 * Plugin URI: https://themeforest.net/user/creativegigs/portfolio
 * Description: This plugin adds the core features to the Rogan WordPress theme. You must have to install this plugin to get all the features included with the theme.
 * Version: 1.5.7
 * Author: CreativeGigs
 * Author URI: https://themeforest.net/user/creativegigs/portfolio
 * Text domain: rogan-core
 */

if ( !defined('ABSPATH') )
    die('-1' );


// Make sure the same class is not loaded twice in free/premium versions.
if ( !class_exists( 'Rogan_core' ) ) {
	/**
	 * Main Rogan Core Class
	 *
	 * The main class that initiates and runs the Rogan Core plugin.
	 *
	 * @since 1.7.0
	 */
	final class Rogan_core {
		/**
		 * Rogan Core Version
		 *
		 * Holds the version of the plugin.
		 *
		 * @since 1.7.0
		 * @since 1.7.1 Moved from property with that name to a constant.
		 *
		 * @var string The plugin version.
		 */
		const VERSION = '1.0' ;
		/**
		 * Minimum Elementor Version
		 *
		 * Holds the minimum Elementor version required to run the plugin.
		 *
		 * @since 1.7.0
		 * @since 1.7.1 Moved from property with that name to a constant.
		 *
		 * @var string Minimum Elementor version required to run the plugin.
		 */
		const MINIMUM_ELEMENTOR_VERSION = '1.7.0';
		/**
		 * Minimum PHP Version
		 *
		 * Holds the minimum PHP version required to run the plugin.
		 *
		 * @since 1.7.0
		 * @since 1.7.1 Moved from property with that name to a constant.
		 *
		 * @var string Minimum PHP version required to run the plugin.
		 */
		const  MINIMUM_PHP_VERSION = '5.4' ;
        /**
         * Plugin's directory paths
         * @since 1.0
         */
        const CSS = null;
        const JS = null;
        const IMG = null;
        const VEND = null;

		/**
		 * Instance
		 *
		 * Holds a single instance of the `Rogan_Core` class.
		 *
		 * @since 1.7.0
		 *
		 * @access private
		 * @static
		 *
		 * @var Rogan_Core A single instance of the class.
		 */
		private static  $_instance = null ;
		/**
		 * Instance
		 *
		 * Ensures only one instance of the class is loaded or can be loaded.
		 *
		 * @since 1.7.0
		 *
		 * @access public
		 * @static
		 *
		 * @return Rogan_Core An instance of the class.
		 */
		public static function instance() {
			if ( is_null( self::$_instance ) ) {
				self::$_instance = new self( );
			}
			return self::$_instance;
		}

		/**
		 * Clone
		 *
		 * Disable class cloning.
		 *
		 * @since 1.7.0
		 *
		 * @access protected
		 *
		 * @return void
		 */
		public function __clone() {
			// Cloning instances of the class is forbidden
			_doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'rogan-core' ), '1.7.0'  );
		}

		/**
		 * Wakeup
		 *
		 * Disable unserializing the class.
		 *
		 * @since 1.7.0
		 *
		 * @access protected
		 *
		 * @return void
		 */
		public function __wakeup() {
			// Unserializing instances of the class is forbidden.
			_doing_it_wrong( __FUNCTION__, esc_html__( 'Cheatin&#8217; huh?', 'rogan-core' ), '1.7.0'  );
		}

		/**
		 * Constructor
		 *
		 * Initialize the Rogan Core plugins.
		 *
		 * @since 1.7.0
		 *
		 * @access public
		 */
		public function __construct() {
            add_action('init', [$this, 'mega_menu_include'] );
			$this->core_includes( );
			$this->init_hooks( );
			do_action( 'rogan_core_loaded'  );
		}

		/**
		 * Include Files
		 *
		 * Load core files required to run the plugin.
		 *
		 * @since 1.7.0
		 *
		 * @access public
		 */
		public function core_includes() {
			// Extra functions
			require_once __DIR__ . '/inc/extra.php';

			// Custom post types
			require_once __DIR__ . '/post-type/service.pt.php';
			require_once __DIR__ . '/post-type/portfolio.pt.php';
			require_once __DIR__ . '/post-type/faq.pt.php';
			require_once __DIR__ . '/post-type/none.pt.php';
            require_once __DIR__ . '/post-type/Rogan_mega_menu.pt.php';

            /**
             * Register widget area.
             *
             * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
             */
			require_once plugin_dir_path(__FILE__) . '/wp-widgets/widgets.php';

			// Elementor custom field icons
			require_once plugin_dir_path(__FILE__) . '/fields/icons.php';

            // RGBA color picker
            require plugin_dir_path(__FILE__) . '/acf-rgba-color-picker/acf-rgba-color-picker.php';

            // ACF Metaboxes
            require plugin_dir_path(__FILE__) . '/inc/metaboxes.php';
		}

        function mega_menu_include() {
		    $opt = get_option('rogan_opt' );
            // Mega Menu
            $mega_menus = new WP_Query(array(
                'post_type' => 'megamenu',
                'posts_per_page' => -1,
            ) );
            $header_style = !empty($opt['header_style']) ? $opt['header_style'] : '';
            $mega_menu_count = $mega_menus->post_count;
            if ( ($mega_menu_count > 0) && ($header_style != '6') ) {
                require plugin_dir_path(__FILE__) . '/inc/mega_menu.php';
            }
        }

		/**
		 * Init Hooks
		 *
		 * Hook into actions and filters.
		 *
		 * @since 1.7.0
		 *
		 * @access private
		 */
		private function init_hooks() {
			add_action( 'init', [ $this, 'i18n' ]  );
			add_action( 'plugins_loaded', [ $this, 'init' ]  );
		}

		/**
		 * Load Textdomain
		 *
		 * Load plugin localization files.
		 *
		 * @since 1.7.0
		 *
		 * @access public
		 */
		public function i18n() {
			load_plugin_textdomain( 'rogan-core', false, plugin_basename( dirname( __FILE__ ) ) . '/languages'  );
		}


		/**
		 * Init Rogan Core
		 *
		 * Load the plugin after Elementor (and other plugins) are loaded.
		 *
		 * @since 1.0.0
		 * @since 1.7.0 The logic moved from a standalone function to this class method.
		 *
		 * @access public
		 */
		public function init() {

			if ( !did_action( 'elementor/loaded' ) ) {
				add_action( 'admin_notices', [ $this, 'admin_notice_missing_main_plugin' ]  );
				return;
			}

			// Check for required Elementor version

			if ( !version_compare( ELEMENTOR_VERSION, self::MINIMUM_ELEMENTOR_VERSION, '>=' ) ) {
				add_action( 'admin_notices', [ $this, 'admin_notice_minimum_elementor_version' ]  );
				return;
			}

			// Check for required PHP version

			if ( version_compare( PHP_VERSION, self::MINIMUM_PHP_VERSION, '<' ) ) {
				add_action( 'admin_notices', [ $this, 'admin_notice_minimum_php_version' ]  );
				return;
			}

			// Add new Elementor Categories
			add_action( 'elementor/init', [ $this, 'add_elementor_category' ]  );

			// Register Widget Scripts
			add_action( 'elementor/frontend/after_register_scripts', [ $this, 'register_widget_scripts' ]  );
			add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'register_widget_scripts' ]  );

			// Register Widget Style
			add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'enqueue_widget_styles' ]  );
			add_action( 'elementor/editor/before_enqueue_scripts', [ $this, 'enqueue_widget_styles' ]  );
			add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_widget_styles' ]  );

			// Register New Widgets
			add_action( 'elementor/widgets/widgets_registered', [ $this, 'on_widgets_registered' ]  );
		}

		/**
		 * Admin notice
		 *
		 * Warning when the site doesn't have Elementor installed or activated.
		 *
		 * @since 1.1.0
		 * @since 1.7.0 Moved from a standalone function to a class method.
		 *
		 * @access public
		 */
		public function admin_notice_missing_main_plugin() {
			$message = sprintf(
			/* translators: 1: Rogan Core 2: Elementor */
				esc_html__( '"%1$s" requires "%2$s" to be installed and activated.', 'rogan-core' ),
				'<strong>' . esc_html__( 'Rogan core', 'rogan-core' ) . '</strong>',
				'<strong>' . esc_html__( 'Elementor', 'rogan-core' ) . '</strong>'
			 );
			printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message  );
		}

		/**
		 * Admin notice
		 *
		 * Warning when the site doesn't have a minimum required Elementor version.
		 *
		 * @since 1.1.0
		 * @since 1.7.0 Moved from a standalone function to a class method.
		 *
		 * @access public
		 */
		public function admin_notice_minimum_elementor_version() {
			$message = sprintf(
			/* translators: 1: Rogan Core 2: Elementor 3: Required Elementor version */
				esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'rogan-core' ),
				'<strong>' . esc_html__( 'Rogan Core', 'rogan-core' ) . '</strong>',
				'<strong>' . esc_html__( 'Elementor', 'rogan-core' ) . '</strong>',
				self::MINIMUM_ELEMENTOR_VERSION
			 );
			printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message  );
		}

		/**
		 * Admin notice
		 *
		 * Warning when the site doesn't have a minimum required PHP version.
		 *
		 * @since 1.7.0
		 *
		 * @access public
		 */
		public function admin_notice_minimum_php_version() {
			$message = sprintf(
			/* translators: 1: Rogan Core 2: PHP 3: Required PHP version */
				esc_html__( '"%1$s" requires "%2$s" version %3$s or greater.', 'rogan-core' ),
				'<strong>' . esc_html__( 'Rogan Core', 'rogan-core' ) . '</strong>',
				'<strong>' . esc_html__( 'PHP', 'rogan-core' ) . '</strong>',
				self::MINIMUM_PHP_VERSION
			 );
			printf( '<div class="notice notice-warning is-dismissible"><p>%1$s</p></div>', $message  );
		}

		/**
		 * Add new Elementor Categories
		 *
		 * Register new widget categories for Rogan Core widgets.
		 *
		 * @since 1.0.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access public
		 */
		public function add_elementor_category() {
			\Elementor\Plugin::instance()->elements_manager->add_category( 'rogan-elements', [
				'title' => __( 'Rogan Elements', 'rogan-core' ),
			], 1  );


		}

		/**
		 * Register Widget Scripts
		 *
		 * Register custom scripts required to run Rogan Core.
		 *
		 * @since 1.6.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access public
		 */
		public function register_widget_scripts() {
            $opt = get_option( 'rogan_opt' );
            $is_anim = isset($opt['is_anim']) ? $opt['is_anim'] : '1';
            /**
             * Registering scripts
             */
            wp_register_script( 'magnific-popup', plugins_url( 'assets/vendors/magnific/jquery.magnific-popup.min.js', __FILE__ ), 'jquery', '1.1.0', true );
            wp_register_script( 'owl-carousel', plugins_url( 'assets/vendors/owl-carousel/owl.carousel.min.js', __FILE__ ), 'jquery', '3.5', true );
            wp_register_script( 'iziModal', plugins_url( 'assets/vendors/iziModal-master/iziModal.min.js', __FILE__ ), 'jquery', '1.6.0', true );
            wp_register_script( 'wow', plugins_url( 'assets/vendors/WOW-master/dist/wow.min.js', __FILE__ ), 'jquery', '1.1.3', true );
            wp_register_script( 'rogan-define-anim', plugins_url( 'assets/js/define-anim.js', __FILE__ ), 'jquery', '1.0', true );

            if ( $is_anim == '1' ) {
                wp_enqueue_script( 'wow' );
                wp_enqueue_script( 'rogan-define-anim' );
            }

            wp_register_script( 'isotope', plugins_url( 'assets/vendors/isotope.pkgd.min.js', __FILE__ ), 'jquery', '2.2.2', true );

            wp_enqueue_script( 'appear', plugins_url( 'assets/vendors/jquery.appear.js', __FILE__ ), 'jquery', '1.0', true );

            wp_register_script( 'countto', plugins_url( 'assets/vendors/jquery.countTo.js', __FILE__ ), 'jquery', '1.0', true );

            wp_register_script('animated-headline', plugins_url( 'assets/vendors/animated-headline-master/main.js', __FILE__ ), 'jquery', '1.0', true );

            wp_register_script('fancybox', plugins_url( 'assets/vendors/fancybox/dist/jquery.fancybox.min.js', __FILE__ ), 'jquery', '3.3.5', true );

            wp_enqueue_script('selectize', plugins_url( 'assets/vendors/selectize.js/selectize.min.js', __FILE__ ), array('jquery', 'bootstrap'), '0.12.6', true );

            wp_register_script('tilt-jquery', plugins_url( 'assets/vendors/tilt.jquery.js', __FILE__ ), 'jquery', '3.3.5', true );

            wp_enqueue_script('rogan-theme', plugins_url( 'assets/js/theme.js', __FILE__ ), array('jquery', 'popper', 'bootstrap', 'rogan-mega-menu'), '1.0', true );
		}

		/**
		 * Register Widget Styles
		 *
		 * Register custom styles required to run Rogan Core.
		 *
		 * @since 1.7.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access public
		 */

		public function enqueue_widget_styles() {

            wp_register_style( 'magnific-popup', plugins_url( 'assets/vendors/magnific/magnific-popup.css', __FILE__ )  );

            wp_register_style( 'simple-line-icon', plugins_url( 'assets/vendors/simple-line-icon/simple-line-icons.css', __FILE__ )  );

            wp_register_style( 'themify-icons', plugins_url( 'assets/vendors/themify-icon/themify-icons.css', __FILE__ )  );

            wp_register_style( 'elegent-icons', plugins_url( 'assets/vendors/elegent/style.css', __FILE__ )  );

            if ( defined('ELEMENTOR_VERSION') ) {
                if ( \Elementor\Plugin::$instance->preview->is_preview_mode() ) {
                    wp_enqueue_style( 'simple-line-icon'  );
                    wp_enqueue_style( 'themify-icons'  );
                    wp_enqueue_style( 'elegent-icons'  );
                }
            }

            wp_enqueue_style('rogan-flaticon', plugins_url( 'assets/font/flaticon.css', __FILE__ ) );

            wp_register_style('fancybox', plugins_url( 'assets/vendors/fancybox/dist/jquery.fancybox.min.css', __FILE__ ) );

            wp_enqueue_style('sanzzy-map', plugins_url( 'assets/vendors/sanzzy-map/dist/snazzy-info-window.min.css', __FILE__ ) );

            wp_enqueue_style('rogan-custom-animation', plugins_url( 'assets/css/custom-animation.css', __FILE__ ) );

            wp_enqueue_style('animated-headline', plugins_url( 'assets/vendors/animated-headline-master/style.css', __FILE__ ) );

            wp_enqueue_style('iziModal', plugins_url( 'assets/vendors/iziModal-master/iziModal.css', __FILE__ ) );

            wp_enqueue_style('jquery-ui', plugins_url( 'assets/vendors/jquery-ui/jquery-ui.min.css', __FILE__ ) );

            wp_enqueue_style('selectize', plugins_url( 'assets/vendors/selectize.js/selectize.css', __FILE__ ) );

            wp_enqueue_style('owl-carousel', plugins_url( 'assets/vendors/owl-carousel/owl.carousel.css', __FILE__ ) );

            wp_enqueue_style('owl-theme', plugins_url( 'assets/vendors/owl-carousel/owl.theme.css', __FILE__ ) );

            wp_enqueue_style('animate', plugins_url( 'assets/vendors/owl-carousel/animate.css', __FILE__ ) );
		}


		/*public function register_admin_styles() {
            wp_enqueue_style( 'rogan_core_admin', plugins_url( 'assets/css/rogan-core-admin.css', __FILE__ )  );
        }*/

		/**
		 * Register New Widgets
		 *
		 * Include Rogan Core widgets files and register them in Elementor.
		 *
		 * @since 1.0.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access public
		 */
		public function on_widgets_registered() {
			$this->include_widgets( );
			$this->register_widgets( );
		}

		/**
		 * Include Widgets Files
		 *
		 * Load Rogan Core widgets files.
		 *
		 * @since 1.0.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access private
		 */
		private function include_widgets() {
			require_once __DIR__ . '/widgets/Rogan_hero_agency.php';

			require_once __DIR__ . '/widgets/Rogan_hero_saas.php';

			require_once __DIR__ . '/widgets/Rogan_hero_app.php';

			require_once __DIR__ . '/widgets/Rogan_hero_seo.php';

			require_once __DIR__ . '/widgets/Rogan_hero_portfolio.php';

			require_once __DIR__ . '/widgets/Rogan_service_agency.php';

			require_once __DIR__ . '/widgets/Rogan_services_saas.php';

			require_once __DIR__ . '/widgets/Rogan_features_app.php';

            require_once __DIR__ . '/widgets/Rogan_logo_slider.php';

            require_once __DIR__ . '/widgets/Rogan_two_column_features.php';

            require_once __DIR__ . '/widgets/Rogan_pricing_table.php';

            require_once __DIR__ . '/widgets/Rogan_pricing_table_seo.php';

            require_once __DIR__ . '/widgets/Rogan_pricing_table_features.php';

			require_once __DIR__ . '/widgets/Rogan_testimonials_agency.php';

			require_once __DIR__ . '/widgets/Rogan_testimonials_saas.php';

			require_once __DIR__ . '/widgets/Rogan_testimonials_app.php';

			require_once __DIR__ . '/widgets/Rogan_testimonials_portfolio.php';

			require_once __DIR__ . '/widgets/Rogan_faq.php';

            require_once __DIR__ . '/widgets/Rogan_call_to_action.php';

            require_once __DIR__ . '/widgets/Rogan_about_us.php';

			require_once __DIR__ . '/widgets/Rogan_counter.php';

			require_once __DIR__ . '/widgets/Rogan_agency_gallery.php';

			require_once __DIR__ . '/widgets/Rogan_blog_posts.php';

			require_once __DIR__ . '/widgets/Rogan_icon_boxes.php';

			require_once __DIR__ . '/widgets/Rogan_our_goal.php';

			require_once __DIR__ . '/widgets/Rogan_our_goal_app.php';

			require_once __DIR__ . '/widgets/Rogan_processes.php';

			require_once __DIR__ . '/widgets/Rogan_single_video.php';

			require_once __DIR__ . '/widgets/Rogan_tabs_horizontal.php';

			require_once __DIR__ . '/widgets/Rogan_skills_dark.php';

			require_once __DIR__ . '/widgets/Rogan_service_posts_SEO.php';

			require_once __DIR__ . '/widgets/Rogan_service_posts_dark.php';

			require_once __DIR__ . '/widgets/Rogan_portfolio_dark.php';

			require_once __DIR__ . '/widgets/Rogan_portfolio.php';

			require_once __DIR__ . '/widgets/Rogan_portfolio_carousel.php';

			require_once __DIR__ . '/widgets/Rogan_portfolio_with_title.php';

			require_once __DIR__ . '/widgets/Rogan_contact_form.php';

			require_once __DIR__ . '/widgets/Rogan_service_creative.php';

			require_once __DIR__ . '/widgets/Rogan_map.php';

			require_once __DIR__ . '/widgets/Rogan_team.php';

			require_once __DIR__ . '/widgets/Rogan_three_column_features.php';

			require_once __DIR__ . '/widgets/Rogan_alerts_box.php';

			require_once __DIR__ . '/widgets/Rogan_contact_agency.php';

			require_once __DIR__ . '/widgets/Rogan_blog_full_grid.php';

			require_once __DIR__ . '/widgets/Rogan_blog_filter.php';

			require_once __DIR__ . '/widgets/Rogan_blog_masonry_grid.php';
			
			require_once __DIR__ . '/widgets/Rogan_op_image_slider.php';
			
			require_once __DIR__ . '/widgets/Rogan_op_quoteform.php';

			require_once __DIR__ . '/widgets/Rogan_op_about.php';

			require_once __DIR__ . '/widgets/Rogan_op_partner.php';
			
			require_once __DIR__ . '/widgets/Rogan_op_projectwork.php';

			require_once __DIR__ . '/widgets/Rogan_testimonials_op.php';

			require_once __DIR__ . '/widgets/Rogan_heading.php';

            // Add New Arch
            require_once __DIR__ . '/widgets/Rogan_hero_arch.php';

            require_once __DIR__ . '/widgets/Rogan_title_text.php';

            require_once __DIR__ . '/widgets/Rogan_testimonials_arch.php';

            require_once __DIR__ . '/widgets/Rogan_quoteform_with_imgText.php';

			if ( class_exists('WooCommerce') ) {
			    require_once  __DIR__ . '/widgets/Rogan_product_slider.php';
            }

        }

		/**
		 * Register Widgets
		 *
		 * Register Rogan Core widgets.
		 *
		 * @since 1.0.0
		 * @since 1.7.1 The method moved to this class.
		 *
		 * @access private
		 */
		private function register_widgets() {
			// Site Elements
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_agency()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_saas()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_app()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_seo()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_portfolio()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_service_agency()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_services_saas()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_service_posts_SEO()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_features_app()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_logo_slider()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_two_column_features()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_pricing_table()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_pricing_table_seo()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_pricing_table_features()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_agency()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_saas()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_app()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_portfolio()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_faq()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_call_to_action()  );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_about_us()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_counter()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_agency_gallery()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_blog_posts()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_icon_boxes()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_our_goal()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_our_goal_app()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_processes()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_single_video()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_tabs_horizontal()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_skills_dark()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_service_posts_SEO()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_service_posts_dark()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_portfolio_dark()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_portfolio()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_portfolio_carousel()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_portfolio_with_title()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_contact_form()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_service_creative()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_map()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_team()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_three_column_features()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_alerts_box()  );
			
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_contact_agency()  );
			
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_blog_full_grid()  );
			
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_blog_filter()  );
			
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_blog_masonry_grid()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_op_image_slider()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_op_quoteform()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_op_about()  );
			
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_op_partner()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_op_projectwork()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_op()  );

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_heading()  );

            //Add New Arch
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_hero_arch() );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_title_text() );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_testimonials_arch() );

            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \RoganCore\Widgets\Rogan_quoteform_with_imgText() );

            if ( class_exists('WooCommerce') ) {
                \Elementor\Plugin::instance()->widgets_manager->register_widget_type(new \RoganCore\Widgets\Rogan_product_slider());
            }
		}
	}
}
// Make sure the same function is not loaded twice in free/premium versions.

if ( !function_exists( 'rogan_core_load' ) ) {
	/**
	 * Load Rogan Core
	 *
	 * Main instance of Rogan_Core.
	 *
	 * @since 1.0.0
	 * @since 1.7.0 The logic moved from this function to a class method.
	 */
	function rogan_core_load() {
		return Rogan_core::instance( );
	}

	// Run Rogan Core
    rogan_core_load( );
}


function rogan_admin_cpt_script( $hook ) {

    global $post;

    if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
        if ( 'service' === $post->post_type ) {
            wp_enqueue_style('themify-icons', plugins_url( 'assets/vendors/themify-icon/themify-icons.css', __FILE__ ) );
        }
    }
}
add_action( 'admin_enqueue_scripts', 'rogan_admin_cpt_script', 10, 1  );