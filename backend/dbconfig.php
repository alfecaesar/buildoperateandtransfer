<?php

// https://medoo.in/doc

require  'Medoo.php';


use Medoo\Medoo;
 
$database = new Medoo([
	'database_type' => 'mysql',
	'database_name' => 'buildoperatetransfer_wp',
	'server' => 'localhost',
	'username' => 'buildoperatetransfer',
	'password' => 'CslFoTSXvXoXPzA0'
]);




// create project table
$database->create("project", [
	"project_id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"project_owner_user_id" => [
		"INT",
		"NOT NULL"
    ],
    "project_name" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "category" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "category_specialty" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"suggest_specialty" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "description" => [
		"TEXT(15000)",
		"NOT NULL"
    ],
    "ideal_minimum_viable_product" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "ideal_minimum_viable_product_others" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "additional_project_files" => [
		"LONGBLOB", 
		"NOT NULL"
    ],
    "target_markets" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "increased_social_media_awareness" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "lower_acquisition_costs_by" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "locate_product_faster" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "compare_shopping_on" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "enable_deals_bargains_coupons_and_group_buys" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "create_market_for_products_to_achieve" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "additional_performances_important" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "performance_not_what_you_looking_for" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "ecommerce_development_deliverables" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],    
    "ecommerce_platforms" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "ecommerce_development_skills" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "ecommerce_payment_systems" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "ecommerce_database" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "business_size_experience" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "additional_features_are_important_to_you" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "features_not_what_you_looking_for" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],  
    "specific_budget" => [
		"VARCHAR(255)",
		"NOT NULL"
    ], 
    "specific_timeframe_to_complete_min" => [
		"VARCHAR(255)",
		"NOT NULL"
    ], 
    "specific_timeframe_to_complete_max" => [
		"VARCHAR(255)",
		"NOT NULL"
    ], 
    "how_long_prepared_to_work_with_min" => [
		"VARCHAR(255)",
		"NOT NULL"
    ], 
    "how_long_prepared_to_work_with_max" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "how_would_you_like_to_work_with" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "when_would_you_like_to_pay" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"contractors_email" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"message_to_contractors" => [
		"TEXT(15000)",
		"NOT NULL"
	],
	"allow_contractors_to_bid_other_projects" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"user_name" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "user_email" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "status" => [
		"VARCHAR(255)",
		"NOT NULL"
	]

]);

//echo 'LOG: ' . var_dump($database->log());
//echo 'ERROR: ' .var_dump( $database->error() );


// create user table
$database->create("user", [
	"user_id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"user_name" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
    "user_email" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"user_password" => [
		"VARCHAR(24)",
		"NOT NULL"
	],
	"access_type" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"register_timestamp" => [
		"TIMESTAMP",
		"NOT NULL",
		"DEFAULT CURRENT_TIMESTAMP"
    ]
	
]);

//echo 'LOG: ' . var_dump($database->log());
//echo 'ERROR: ' .var_dump( $database->error() );

$database->create("bot_project_request", [
	"id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"user_id" => [
		"INT",
		"NOT NULL"
	],
	"project_id" => [
		"INT",
		"NOT NULL"
	],
	"project_owner_user_id" => [
		"INT",
		"NOT NULL"
    ],
    "request_status" => [
		"VARCHAR(255)",
		"NOT NULL"
	],

]);


$database->create("task_list", [
	"id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"user_id" => [
		"INT",
		"NOT NULL"
	],
	"project_id" => [
		"INT",
		"NOT NULL"
    ],
	"project_owner_user_id" => [
		"INT",
		"NOT NULL"
    ],
    "title" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "description" => [
		"TEXT(15000)",
		"NOT NULL"
	],
    "estimated_time" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "due_date" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "status" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "assigned_to" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
	"milestone_id" => [
		"VARCHAR(255)",
		"NOT NULL"
	]

]);


$database->create("user_activity", [
	"id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"user_id" => [
		"INT",
		"NOT NULL"
	],
	"activity_name" => [
		"VARCHAR(255)",
		"NOT NULL"
    ],
	"activity_description" => [
		"TEXT",
		"NOT NULL"
    ],
	"activity_timestamp" => [
		"TIMESTAMP",
		"NOT NULL",
		"DEFAULT CURRENT_TIMESTAMP"
    ]

]);

$database->create("milestone", [
	"id" => [
		"INT",
		"NOT NULL",
		"AUTO_INCREMENT",
		"PRIMARY KEY"
	],
	"user_id" => [
		"INT",
		"NOT NULL"
	],
	"project_id" => [
		"INT",
		"NOT NULL"
    ],
	"project_owner_user_id" => [
		"INT",
		"NOT NULL"
    ],
    "title" => [
		"VARCHAR(255)",
		"NOT NULL"
	],
    "due_date" => [
		"VARCHAR(255)",
		"NOT NULL"
	]
]);

// drop project table
// $database->drop("project");



?>