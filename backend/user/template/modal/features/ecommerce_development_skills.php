<!-- Modal -->
<div class="modal fade" id="modalbox_features_3" tabindex="-1" role="dialog" aria-labelledby="eCommerce Development Skills" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">eCommerce Development Skills</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add eCommerce Development Skills</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="ecommerce_development_skills" id="ecommerce_development_skills" class="form-control" multiple>
                            <option id="Java">Java</option>
                            <option id="JavaScript">JavaScript</option>
                            <option id="Angular">Angular</option>
                            <option id="AngularJS">AngularJS</option>
                            <option id="ASP.NET">ASP.NET</option>
                            <option id="Bootstrap">Bootstrap</option>
                            <option id="jQuery">jQuery</option>
                            <option id="PHP">PHP</option>
                            <option id="React">React</option>
                            <option id="Ruby">Ruby</option>
                            <option id="ue.js">ue.js</option>
                            <option id="Python">Python</option>
                            <option id="CSS">CSS</option>
                            <option id="CSS3">CSS3</option>
                            <option id="HTML">HTML</option>
                            <option id="HTML5">HTML5</option>
                            <option id="Node.js">Node.js</option>
                            <option id="Joomla">Joomla</option>
                            <option id="jQuery Mobile">jQuery Mobile</option>
                            <option id="Laravel ">Laravel </option>
                            <option id="Liquid">Liquid</option>
                            <option id="Ruby on Rails">Ruby on Rails</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>