<!-- Modal -->
<div class="modal fade" id="modalbox_logout" tabindex="-1" role="dialog" aria-labelledby="Confirm Logout" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Confirm Logout</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-text">
                        <p>Do you wish to logout your account?</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> No</a>
                <a href="http://afarinsights.com/backend/user/logout.php" class="btn btn-yellow"><span>Yes <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>