<?php

Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Blog settings', 'rogan' ),
	'id'        => 'blog_page',
	'icon'      => 'dashicons dashicons-admin-post',
));


Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Title-bar', 'rogan' ),
	'id'        => 'blog_titlebar_settings',
	'icon'      => '',
    'subsection' => true,
	'fields'    => array(
		array(
			'title'     => esc_html__( 'Blog page title', 'rogan' ),
			'subtitle'  => esc_html__( 'Give here the blog page title', 'rogan' ),
			'desc'      => esc_html__( 'This text will be show on blog page banner', 'rogan' ),
			'id'        => 'blog_title',
			'type'      => 'text',
			'default'   => 'Blog List'
		),
		array(
			'title'     => esc_html__( 'Blog page subtitle', 'rogan' ),
			'id'        => 'blog_subtitle',
			'type'      => 'text',
			'default'   =>  esc_html__( 'With right sidebar', 'rogan' ),
            'required'  => array( 'blog_banner_style', '=', '1' )
		),
	)
));


Redux::setSection( 'rogan_opt', array(
    'title'     => esc_html__( 'Nav-bar', 'rogan' ),
    'id'        => 'blog_navbar',
    'icon'      => '',
    'subsection' => true,
    'fields'    => array(
        array(
            'title'     => esc_html__( 'Menu Item Color', 'rogan' ),
            'subtitle'  => esc_html__( 'Menu item font color', 'rogan' ),
            'id'        => 'blog_menu_font_color',
            'output'    => '.blog .header_area .navbar .navbar-nav .menu-item a',
            'type'      => 'color',
        ),
    )
));


Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Blog archive', 'rogan' ),
	'id'        => 'blog_meta_opt',
	'icon'      => '',
	'subsection' => true,
	'fields'    => array(
        array(
            'title'     => esc_html__( 'Blog excerpt', 'rogan' ),
            'subtitle'  => esc_html__( 'How much words you want to show in blog page.', 'rogan' ),
            'id'        => 'blog_excerpt',
            'type'      => 'slider',
            'default'   => 40,
            "min"       => 15,
            "step"      => 1,
            "max"       => 100,
            'display_value' => 'label'
        ),
		array(
			'title'     => esc_html__( 'Post meta', 'rogan' ),
			'subtitle'  => esc_html__( 'Show/hide post meta on blog archive page', 'rogan' ),
			'id'        => 'is_post_meta',
			'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '1',
		),
		array(
			'title'     => esc_html__( 'Post author name', 'rogan' ),
			'id'        => 'is_post_author',
			'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '1',
            'required' => array( 'is_post_meta', '=', 1 )
		),
		array(
			'title'     => esc_html__( 'Post date', 'rogan' ),
			'id'        => 'is_post_date',
			'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '1',
            'required' => array( 'is_post_meta', '=', 1 )
		),
		array(
			'title'     => esc_html__( 'Post category', 'rogan' ),
			'id'        => 'is_post_cat',
			'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '1',
            'required' => array( 'is_post_meta', '=', 1 )
		),
	)
));


Redux::setSection( 'rogan_opt', array(
	'title'     => esc_html__( 'Blog single', 'rogan' ),
	'id'        => 'blog_single_opt',
	'icon'      => '',
	'subsection' => true,
	'fields'    => array(
		array(
			'title'     => esc_html__( 'Social share', 'rogan' ),
			'id'        => 'is_social_share',
			'type'      => 'switch',
            'on'        => esc_html__( 'Enabled', 'rogan' ),
            'off'       => esc_html__( 'Disabled', 'rogan' ),
            'default'   => '1'
		),
		array(
			'title'     => esc_html__( 'Post tag', 'rogan' ),
			'id'        => 'is_post_tag',
			'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
            'default'   => '1'
		),
        array(
            'title'     => esc_html__( 'Related posts ', 'rogan' ),
            'id'        => 'is_related_posts',
            'type'      => 'switch',
            'on'        => esc_html__( 'Show', 'rogan' ),
            'off'       => esc_html__( 'Hide', 'rogan' ),
        ),
        array(
            'title'     => esc_html__( 'Posts section title', 'rogan' ),
            'id'        => 'related_posts_title',
            'type'      => 'text',
            'default'   => esc_html__( 'Related Post', 'rogan' ),
            'required'  => array( 'is_related_posts', '=', '1' )
        ),
        array(
            'title'     => esc_html__( 'Related posts count', 'rogan' ),
            'id'        => 'related_posts_count',
            'type'      => 'slider',
            'default'       => 3,
            'min'           => 3,
            'step'          => 1,
            'max'           => 50,
            'display_value' => 'label',
            'required'  => array( 'is_related_posts', '=', '1' )
        ),
	)
));
