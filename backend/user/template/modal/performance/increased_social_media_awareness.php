<!-- Modal -->
<div class="modal fade" id="modalbox_performance_2" tabindex="-1" role="dialog" aria-labelledby="Increased Social Media Awareness" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Increased Social Media Awareness</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add Increased Social Media Awareness</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="increased_social_media_awareness" id="increased_social_media_awareness" class="form-control" multiple>
                            <option id="Amazon">Amazon</option>
                            <option id="Bing">Bing</option>
                            <option id="Facebook ">Facebook </option>
                            <option id="Google">Google</option>
                            <option id="Instagram">Instagram</option>
                            <option id="Yahoo">Yahoo</option>
                            <option id="Linkedin">Linkedin</option>
                            <option id="Pinterest">Pinterest</option>
                            <option id="Snapchat">Snapchat</option>
                            <option id="Twitter">Twitter</option>
                            <option id="YouTube">YouTube</option>
                            <option id="Website">Website</option>
                            <option id="Blog">Blog</option>
                            <option id="eBay">eBay</option>
                            <option id="Reddit">Reddit</option>
                            <option id="TikTok">TikTok</option>
                            <option id="Tumblr">Tumblr</option>
                            <option id="Twitch">Twitch</option>
                            <option id="WeChat">WeChat</option>
                            <option id="WhatsApp">WhatsApp</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>