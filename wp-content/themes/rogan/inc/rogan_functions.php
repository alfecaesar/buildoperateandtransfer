<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package rogan
 */


// Image sizes
add_image_size( 'rogan_70x80', 70, 80, true); // Cart product thumbnail
add_image_size( 'rogan_100x100', 100, 100, true);
add_image_size( 'rogan_370x510', 370, 510, true); // Related Portfolio thumbnail
add_image_size( 'rogan_370x360', 370, 360, true); // Posts carousel thumbnail
add_image_size( 'rogan_770x480', 770, 480, true); // Blog list thumbnail
add_image_size( 'rogan_430x575', 430, 575, true); // Portfolio archive thumbnail


// Comment list
function rogan_comments($comment, $args, $depth){
     if ( 'div' === $args['style'] ) {
        $tag       = 'div';
        $add_below = 'comment';
    } else {
        $tag       = 'li';
        $add_below = 'div-comment';
    }
    $GLOBALS['comment'] = $comment;
    extract($args, EXTR_SKIP);
    $is_reply = ($depth > 1) ? 'comment-reply' : '';
    $has_children = ( !empty( $args['has_children'] ) ) ? ' has_comment_reply' : '';
    ?>
    <div <?php comment_class( "single-comment align-items-top $has_children $is_reply") ?> id="comment-<?php comment_ID() ?>">
        <?php
        if(get_avatar($comment)) {
            echo get_avatar($comment, 60, null, null, array( 'class'=> 'user-img' ));
        }
        ?>
        <div class="user-comment-data">
            <h6 class="name"> <?php echo ucwords(get_comment_author()) ?> </h6>
            <div class="date"> <?php comment_date(get_option( 'date_format' )); ?> </div>
            <?php comment_text() ?>
            <?php
            comment_reply_link(
                array_merge( $args,
                    array( 'reply_text' => esc_html__( 'Reply', 'rogan' ),
                    'depth' => $depth,
                    'max_depth' => $args['max_depth'])
                )
            );
            ?>
        </div> <!-- /.user-comment-data -->

    <?php
}


// Social Links
function rogan_social_links() {
    $opt = get_option( 'rogan_opt' );
    ?>
    <?php if(!empty($opt['facebook'])) { ?>
        <li> <a href="<?php echo esc_url($opt['facebook']); ?>"> <i class="fa fa-facebook" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['twitter'])) { ?>
        <li> <a href="<?php echo esc_url($opt['twitter']); ?>"> <i class="fa fa-twitter" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['googleplus'])) { ?>
        <li> <a href="<?php echo esc_url($opt['googleplus']); ?>"> <i class="fa fa-google" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['instagram'])) { ?>
        <li> <a href="<?php echo esc_url($opt['instagram']); ?>"> <i class="fa fa-instagram" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['linkedin'])) { ?>
        <li> <a href="<?php echo esc_url($opt['linkedin']); ?>"> <i class="fa fa-linkedin" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['youtube'])) { ?>
        <li> <a href="<?php echo esc_url($opt['youtube']); ?>"> <i class="fa fa-youtube" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php if(!empty($opt['dribble'])) { ?>
        <li> <a href="<?php echo esc_url($opt['dribble']); ?>"> <i class="fa fa-dribbble" aria-hidden="true"></i> </a> </li>
    <?php } ?>
    <?php
}


// Social Share Options
function rogan_portfolio_social_share() {
    $opt = get_option( 'rogan_opt' );
    echo !empty($opt['share_title']) ? '<li>'.esc_html($opt['share_title']) . '</li>' : esc_html__( 'Share on ', 'rogan' ) ?>
    <?php if ($opt['is_portfolio_fb'] == '1' ) : ?>
        <li><a href="//facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
    <?php endif; ?>
    <?php if ($opt['is_portfolio_twitter'] == '1' ) : ?>
        <li><a href="//twitter.com/intent/tweet?text=<?php the_permalink(); ?>"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
    <?php endif; ?>
    <?php if ($opt['is_portfolio_googleplus'] == '1' ) : ?>
        <li> <a href="//plus.google.com/share?url=<?php the_permalink() ?>"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
    <?php endif; ?>
    <?php if ($opt['is_portfolio_linkedin'] == '1' ) : ?>
        <li><a href="//www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink() ?>"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
    <?php endif; ?>
    <?php if ($opt['is_portfolio_pinterest'] == '1' ) : ?>
        <li><a href="//www.pinterest.com/pin/create/button/?url=<?php the_permalink() ?>"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
    <?php endif; ?>
    <?php
}


// Search form
function rogan_search_form($is_button=true) {
    ?>
    <div class="rogan-search">
        <form class="form-wrapper" action="<?php echo esc_url(home_url( '/' )); ?>" _lpchecked="1">
            <input type="text" id="search" placeholder="<?php esc_attr_e( 'Search ...', 'rogan' ); ?>" name="s">
            <button type="submit" class="btn"><i class="fa fa-search"></i></button>
        </form>
        <?php if($is_button==true) { ?>
        <a href="<?php echo esc_url(home_url( '/' )); ?>" class="home_btn"> <?php esc_html_e( 'Back to home Page', 'rogan' ); ?> </a>
        <?php } ?>
    </div>
    <?php
}


// Day link to archive page
function rogan_day_link() {
    $archive_year   = get_the_time( 'Y' );
    $archive_month  = get_the_time( 'm' );
    $archive_day    = get_the_time( 'd' );
    echo get_day_link( $archive_year, $archive_month, $archive_day);
}


// Limit latter
function rogan_limit_latter($string, $limit_length, $suffix = '...' ) {
    if (strlen($string) > $limit_length) {
        echo strip_shortcodes(substr($string, 0, $limit_length) . $suffix);
    }
    else {
        echo strip_shortcodes(esc_html($string));
    }
}


// Get comment count text
function rogan_comment_count($post_id, $no_comments = 'No Comments' ) {
    $comments_number = get_comments_number($post_id);
    if($comments_number == 0) {
        $comment_text = $no_comments;
    }elseif($comments_number == 1) {
        $comment_text = esc_html__( '1 Comment', 'rogan' );
    }elseif($comments_number > 1) {
        $comment_text = $comments_number.esc_html__( ' Comments', 'rogan' );
    }
    echo esc_html($comment_text);
}


// Post's excerpt text
function rogan_excerpt($redux_opt, $settings_key, $echo = true) {
    $opt = get_option($redux_opt);
    $excerpt_limit = !empty($opt[$settings_key]) ? $opt[$settings_key] : 40;
    $excerpt = wp_trim_words(get_the_content(), $excerpt_limit, '' );
    if($echo == true) {
        echo esc_html($excerpt);
    }else {
        return esc_html($excerpt);
    }
}


// Get author role
function rogan_get_author_role() {
    global $authordata;
    $author_roles = $authordata->roles;
    $author_role = array_shift($author_roles);
    return esc_html($author_role);
}


// Banner Title
function rogan_banner_title() {
    $opt = get_option( 'rogan_opt' );
    if ( is_home() ) {
        $blog_title = !empty($opt['blog_title']) ? $opt['blog_title'] : esc_html__( 'Blog', 'rogan' );
        echo esc_html($blog_title);
    }
    elseif ( is_page() || is_single() ) {
        the_title();
    }
    elseif ( is_category() ) {
        single_cat_title();
    }
    elseif ( is_search() ) {
        echo esc_html__( "Search result for ", "rogan" );
        echo '<strong>"';
        echo get_search_query();
        echo '"<strong>';
    }
    elseif ( class_exists( 'WooCommerce' ) ) {
        if ( is_shop() )  {
            echo !empty($opt['shop_title']) ? $opt['shop_title'] : '';
        }
    }
    elseif ( is_archive() ) {
        the_archive_title();
    }
    else {
        the_title();
    }
}

function rogan_banner_subtitle() {
    $opt = get_option( 'rogan_opt' );
    if(is_home()) {
        $blog_subtitle = !empty($opt['blog_subtitle']) ? $opt['blog_subtitle'] : get_bloginfo( 'description' );
        echo esc_html($blog_subtitle);
    }
    elseif(is_page() || is_single()) {
        echo nl2br(get_the_excerpt());
    }
}

function rogan_banner_subtitle2() {
    $opt = get_option( 'rogan_opt' );
    if(is_home()) {
        $blog_title = !empty($opt['blog_title']) ? $opt['blog_title'] : esc_html__( 'Blog', 'rogan' );
        echo esc_html($blog_title);
    }
    elseif(is_page() || is_single()) {
        the_title();
    }
}


/**
 * Get a specific html tag from content
 * @return a specific HTML tag from the loaded content
 */
function rogan_get_html_tag( $tag = 'blockquote', $content = '' ) {
    $dom = new DOMDocument();
    $dom->loadHTML($content);
    $divs = $dom->getElementsByTagName( $tag );
    $i = 0;
    foreach ($divs as $div) {
        if($i == 1) {
            break;
        }
        echo "<{$div->tagName}> {$div->nodeValue} </{$div->tagName}>";
        ++$i;
    }
}


/**
 * Website Logo
 */
function rogan_logo() {
    $opt = get_option( 'rogan_opt' );
    $page_logo = function_exists( 'get_field' ) ? get_field( 'logo' ) : '';
    if( $page_logo ) {
        $main_logo = $page_logo;
    } else {
        $main_logo = isset($opt['main_logo'] ['url']) ? $opt['main_logo'] ['url'] : '';
        $sticky_logo = isset($opt['sticky_logo']['url']) ? $opt['sticky_logo']['url'] : '';
    }
    ?>
    <a href="<?php echo esc_url(home_url( '/' )) ?>">
        <?php
        if( !empty($main_logo) ) : ?>
                <img src="<?php echo esc_url($main_logo) ?>" alt="<?php bloginfo( 'name' ); ?>">
                <?php if ( !empty($sticky_logo) ) : ?>
                    <img src="<?php echo esc_url($sticky_logo) ?>" alt="<?php bloginfo( 'name' ); ?>">
                <?php endif; ?>
            <?php
        else: ?>
            <h3> <?php echo get_bloginfo( 'name' ) ?> </h3>
            <?php
        endif;
    echo '</a>';
}
