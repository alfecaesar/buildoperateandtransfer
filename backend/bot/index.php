<?php include '../bot/template/header.php';

include '../dbconfig.php';

?>

<?php include '../bot/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../bot/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Dashboard</h2>
                        </div>
                        <div class="col-6 text-right">
                            
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <div class="row">
                        
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Requested Opportunities</h5>
                                    <p class="card-text">
                                        <?php
                                            $count = $database->count("bot_project_request", [
                                                "user_id" => $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]),
                                                "request_status" => "Pending"
                                            ]);

                                            echo $count;
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Approved Opportunities</h5>
                                    <p class="card-text">
                                        <?php
                                            $count = $database->count("bot_project_request", [
                                                "user_id" => $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]),
                                                "request_status" => "Approved"
                                            ]);

                                            echo $count;
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <div class="card">
                                <div class="card-body text-center">
                                    <h5 class="card-title">Completed Opportunities</h5>
                                    <p class="card-text">
                                    <?php
                                            $count = $database->count("bot_project_request", [
                                                "user_id" => $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]),
                                                "request_status" => "Completed"
                                            ]);

                                            echo $count;
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    

                    <!--
                    <div class="alert alert-danger" role="alert">
                        This is a test sample notification!
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    -->
                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>

<?php include '../bot/template/footer.php';?>