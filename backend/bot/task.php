<?php include '../bot/template/header.php';

include '../dbconfig.php';

?>

<?php include '../bot/template/topbar.php'; ?>


<div class="container" id="dashboard-section">
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12 nav_col">
            <?php include '../bot/template/dashboard-sidenav.php'; ?>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12 content_col">


            <!-- START OF STEP 1 -->
            <div class="content_col_box active_tab">
                <div class="content_col_box_heading">
                    <div class="row">
                        <div class="col-6 text-left">
                            <h2>Task</h2>
                        </div>
                        <div class="col-6 text-right">
                            <a href="javascript:void(0)"  data-toggle="modal" data-target="#modalbox_create_task" class="btn btn-yellow"><span>Create Task <i class="fas fa-angle-double-right"></i></a>
                        </div>
                    </div>
                    
                </div>
                <div class="content_col_box_container">

                    <?php 
                    $task_saved = (!empty($_GET['task_saved'])) ? $_GET['task_saved'] : '';
                        if ($task_saved === 'true') { ?>
                        <div class="alert alert-primary" role="alert">
                            Task Successfully Created! 
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php } ?>

                    <div class="task-box row task-box-head">
                        <div class="task-box-1 col col-4 text-left">Title</div>
                        <div class="task-box-3 col col-2 text-left">Estimated Time</div>
                        <div class="task-box-4 col col-2 text-left">Due Date</div>
                        <div class="task-box-5 col col-2 text-left">Status</div>
                        <div class="task-box-6 col col-2 text-left">Assigned To</div>
                    </div>

                    <?php

                    $project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

                    $datas = $database->select("task_list", [
                        "title",
                        "description",
                        "estimated_time",
                        "due_date",
                        "status",
                        "assigned_to"
                    ], [
                        "user_id" => $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]),
                        "project_id" => $project_id
                    ]);

                    foreach($datas as $data){
                        echo '<div class="task-box row">'.
                               '<div class="task-box-1 col col-4 text-left"><div><strong>'.$data['title'].'</strong></div><div>'.$data['description'].'</div></div>'.
                               '<div class="task-box-3 col col-2 text-left">'.$data['estimated_time'].'</div>'.
                               '<div class="task-box-4 col col-2 text-left">'.$data['due_date'].'</div>'.
                               '<div class="task-box-5 col col-2 text-left">'.$data['status'].'</div>'.
                               '<div class="task-box-6 col col-2 text-left">'.$data['assigned_to'].'</div>'.
                             '</div>';
                    }
                    ?>

                </div>
            </div>
            <!-- END OF STEP 1 -->



        </div>
    </div>
</div>
<?php include '../bot/template/modal/create_task.php';?>
<?php include '../bot/template/footer.php';?>