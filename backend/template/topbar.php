<!--Navbar -->
<nav class="mb-1 navbar navbar-expand-lg navbar-dark default-color">
  <a class="navbar-brand" href="/backend/"><img src="http://afarinsights.com/wp-content/uploads/2020/02/bot_logo-1.png" alt="Dashboard"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
    aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="http://afarinsights.com/">
            <i class="fas fa-columns"></i> Back to Website</a>
      </li>
    </ul>
  </div>
</nav>
<!--/.Navbar -->