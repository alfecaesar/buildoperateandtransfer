<!-- Modal -->
<div class="modal fade" id="modalbox_features_1" tabindex="-1" role="dialog" aria-labelledby="eCommerce development deliverables" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">eCommerce development deliverables</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group no-border">
                    <div class="form-group-label">
                        <label for="mod_perf_fld_1">Add eCommerce development deliverables</label>
                    </div>
                    <div class="form-group-tag performance_select">
                        <select type="text" name="ecommerce_development_deliverables" id="ecommerce_development_deliverables" class="form-control" multiple>
                            <option id="Landing Page">Landing Page</option>
                            <option id="Website Development">Website Development</option>
                            <option id="API Integration">API Integration</option>
                            <option id="App Development ">App Development </option>
                            <option id="Bug Fixes">Bug Fixes</option>
                            <option id="Design Enhancement">Design Enhancement</option>
                            <option id="Maintenance">Maintenance</option>
                            <option id="Platform Plugin">Platform Plugin</option>
                            <option id="Responsive Design">Responsive Design</option>
                            <option id="Template">Template</option>
                            <option id="Theme Customization">Theme Customization</option>
                            <option id="Theme Development">Theme Development</option>
                            <option id="We Design">We Design</option>
                            <option id="Website Customization">Website Customization</option>
                            <option id="Website Migration">Website Migration</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="saveSpecialty($('#select_specialty'))" class="btn btn-yellow" data-dismiss="modal"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>