<?php

$project_id = (!empty($_GET['project_id'])) ? $_GET['project_id'] : '';

?>

<!-- Modal -->
<div class="modal fade" id="modalbox_create_milestone" tabindex="-1" role="dialog" aria-labelledby="Create Milestone" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Create Milestone</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form name="create_milestone_form" method="POST" action="save_milestone.php">
                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="title">Title</label>
                        </div>
                        <div class="form-group-tag">
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="form-group-label">
                            <label for="due_date">Due Date</label>
                        </div>
                        <div class="form-group-tag  date input-group">
                            <input type="text" name="due_date" id="due_date" class="form-control"> <span class="input-group-addon"><i class="far fa-calendar-alt"></i>
                        </div>
                    </div>


                    
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $database->get("user", "user_id", [ "user_email" => $_SESSION["user_email"] ]); ?>">
                    <input type="hidden" name="project_id" id="project_id" value="<?php echo $project_id; ?>">
                </form>
            </div>
            <div class="modal-footer">
                <a href="#" class="btn btn-grey" data-dismiss="modal"><span><i class="far fa-times-circle"></i> Cancel</a>
                <a href="javascript:void(0)" onclick="document.create_milestone_form.submit()" class="btn btn-yellow"><span>Save <i class="far fa-check-circle"></i></a>
            </div>
        </div>
    </div>
</div>