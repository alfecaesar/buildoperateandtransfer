<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header();

$opt = get_option( 'rogan_opt' );
$error_heading = !empty($opt['error_heading']) ? $opt['error_heading'] : esc_html__( '404', 'rogan' );
$error_title = !empty($opt['error_title']) ? $opt['error_title'] : esc_html__( 'Page not found', 'rogan' );
$error_subtitle = !empty($opt['error_subtitle']) ? $opt['error_subtitle'] : esc_html__("We can't seem to find the page you're looking for", "rogan");
$error_home_btn_label  =!empty($opt['error_home_btn_label']) ?  $opt['error_home_btn_label'] : esc_html__( 'Go Back', 'rogan' );

?>
<?php if ( $opt['error_img_select'] == '1' ) : ?>
    <div class="error-creative-content">
        <div class="inner-wrapper">
            <h2> <?php echo esc_html($error_title) ?> </h2>
            <p> <?php echo esc_html($error_subtitle) ?> </p>
            <a href="<?php echo esc_url(home_url( '/' )) ?>" class="back-button solid-button-one">
                <?php echo esc_html($error_home_btn_label) ?>
            </a>
        </div>
    </div> <!-- /.error-creative-content -->
<?php endif; ?>

<?php if ( $opt['error_img_select'] == '2' ) : ?>
	<div class="error-content">
		<div class="inner-wrapper">
			<h2> <?php echo esc_html($error_heading) ?> </h2>
            <p> <?php echo wp_kses_post($error_subtitle) ?> </p>
			<a href="<?php echo esc_url(home_url( '/' )) ?>" class="back-button line-button-one">
				<?php echo esc_html($error_home_btn_label) ?>
			</a>
		</div>
	</div> <!-- /.error-content -->
<?php endif; ?>

<?php
get_footer();